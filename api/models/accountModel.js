var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var accountSchema = new Schema({
    _id: { type: String, required: true },
    full_name: { type: String, required: true }, /** ชื่อบริษัท , ชื่อแอดมิน */
    index: { type: Number, default: 0 }, /** ลำดับ */
    email: { type: String, required: true }, /** อีเมล์ Login */
    password: { type: String, required: true }, /** Password hash */
    profile_url: { type: String, default: "default.png" }, /** ภาพเริ่มต้น */
    role: { type: String, default: "USER" }, /** Role : USER , ADMIN */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
    isActive: { type: Boolean, required: true }, /** เปิดหรือปิด ใช้งานเข้าสู่ระบบ */
    isLogOn: { type: Boolean, default: false }, /** online to Server */
    package_default_id: { type: String, default: "" }, /** package เริ่มต้นหลังจาก แบบซื้อหมดอายุ */
    package_id: { type: String, default: "" }, /** บอกบริการที่กำลังใช้งานว่าใช้แบบกี่ GB , TB อื่น ๆ  */
    total_used_storage: { type: Number, default: 0 }, /** หน่วยเป็น Bytes */
});

module.exports = mongoose.model('account', accountSchema, 'account');