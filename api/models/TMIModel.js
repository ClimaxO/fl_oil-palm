var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Teachable Machine Image Model
 */

var tmiSchema = new Schema({
    _id: { type: String, required: true },/** id เทรน และ ชื่อโฟลเดอร์ */
    version: { type: String, required: true }, /** version model : 00 = default */
    path: {
        zip_file_train: { type: String, default: "" },/** ที่อยู่ไฟล์ zip สำหรับเทรน */
        zip_file_build: { type: String, default: "" },/** ที่อยู่ไฟล์ zip สำหรับเทรนเสร็จแล้ว */
    },
    chart: {
        img_acc: { type: String, default: "" }, /** ที่อยู่ไฟล์กราฟ acc image */
        img_lost: { type: String, default: "" }, /** ที่อยู่ไฟล์กราฟ lost image */
    },
    class: {
        rmse: { type: Number, default: 0 }, /** RMSE */
        mae: { type: Number, default: 0 }, /** MAE */
    },
    progress: { type: String, default: "01" },/** สถานะ */
    public: { type: Boolean, default: false }, /** เปิดให้ใช้งานสำหรับประเมินภาพ */
    isActive: { type: Boolean, required: true }, /** เปิด/ปิด */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
});

module.exports = mongoose.model('tmiModel', tmiSchema, 'tmiModel');