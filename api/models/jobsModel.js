var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var jobsSchema = new Schema({
    _id: {
        type: String, set: (uuid = "yyy-xxxx") => {
            const input_length = String(uuid.split("-")[1]).length;
            const add_zero = 5 - input_length;
            let input = uuid.split("-")[1];
            for (let i = 0; i < add_zero; i++) {
                input = '0' + input;
            }
            return `${uuid.split("-")[0]}-${input}`;
        }
    },
    account_id: { type: String, required: true }, /** id ผู้ใช้ */
    index: { type: Number, default: 0 }, /** ลำดับ */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
    progress_date: { type: Date, default: "" }, /** เวลาเริ่มประมวลผลภาพ */
    success_date: { type: Date, default: "" }, /** เวลาสิ้นสุดประมวลผลภาพ */
    progress: { type: String, required: true }, /** สถานะ */
    total_size_bytes: { type: Number, default: 0 }, /** ขนาดไฟล์คำนวณเป็นไปต์ */
    tmiModeUse: { /** model ที่ใช้ประเมินผลภาพ */
        _id: { type: String, default: "" }, /** id model */
        version: { type: String, default: "" },/** id model */
    },
    form_data: {
        seller_id: { type: String, default: "" }, /** รหัสผู้ขาย */
        seller_name: { type: String, default: "" }, /** ชื่อผู้ขาย */
        plot_id: { type: String, default: "" }, /** แปลงที่ */
        driver_name: { type: String, default: "" }, /** ชื่อผู้ขับ */
        car_no_id: { type: String, default: "" }, /** ทะเบียนรถ */
        weigher_name: { type: String, default: "" }, /** ผู้ชั่งชื่อ */
        evaluation_officer: { type: String, default: "" }, /** เจ้าหน้าที่ประเมิน */
        default: {
            seller_id: "", /** รหัสผู้ขาย */
            seller_name: "", /** ชื่อผู้ขาย */
            plot_id: "", /** แปลงที่ */
            driver_name: "", /** ชื่อผู้ขับ */
            car_no_id: "", /** ทะเบียนรถ */
            weigher_name: "", /** ผู้ชั่งชื่อ */
            evaluation_officer: "", /** เจ้าหน้าที่ประเมิน */
        }
    },
    group_rank: {
        top_best: {
            _id: { type: String, default: "" }, /** id หมวดหมู่ */
            name_th: { type: String, default: "" }, /** ชื่อภาษาไทย */
            name_en: { type: String, default: "" }, /** ชื่อภาษาอังกฤษ */
            score: { type: Number, default: 0 }, /** คะแนน */
            weight: { type: Number, default: 0 }, /** น้ำหนัก */
            price: { type: Number, default: 0 }, /** ราคา */
        },
        group_list: {
            type: Object,
            _id: { type: String, default: "" }, /** id หมวดหมู่ */
            name_th: { type: String, default: "" }, /** ชื่อภาษาไทย */
            name_en: { type: String, default: "" }, /** ชื่อภาษาอังกฤษ */
            score: { type: Number, default: 0 }, /** คะแนน */
            default: []
        }
    },
    isActive: { type: Boolean, required: true }, /** เปิดหรือปิด ใช้งาน */
});

module.exports = mongoose.model('jobs', jobsSchema, 'jobs');