var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var imagesSchema = new Schema({
    _id: { type: String, required: true },
    account_id: { type: String, required: true }, /** id ผู้ใช้ */
    jobs_id: { type: String, required: true }, /** id jobs */
    file_name: { type: String, required: true }, /** ชื่อไฟล์ */
    file_size_bytes: { type: Number, default: 0 }, /** ขนาดไฟล์คำนวณเป็นไปต์ */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
    tmiModeUse: { /** model ที่ใช้ประเมินผลภาพ */
        _id: { type: String, default: "" }, /** id model */
        version: { type: String, default: "" },/** id model */
    },
    tmiMode: { /** เชื่อมกับระบบเทรน */
        _id: { type: String, default: "" }, /** id model */
        version: { type: String, default: "" },/** id model */
        confirm_group: {
            _id: { type: String, default: "" }, /** id หมวดหมู่ */
            name_th: { type: String, default: "" }, /** ชื่อภาษาไทย */
            name_en: { type: String, default: "" }, /** ชื่อภาษาอังกฤษ */
            score: { type: Number, default: 0 }, /** คะแนน */
        }
    },
    group_rank: {
        top_best: {
            _id: { type: String, default: "" }, /** id หมวดหมู่ */
            name_th: { type: String, default: "" }, /** ชื่อภาษาไทย */
            name_en: { type: String, default: "" }, /** ชื่อภาษาอังกฤษ */
            score: { type: Number, default: 0 }, /** คะแนน */
        },
        group_list: {
            type: Object,
            _id: { type: String, default: "" }, /** id หมวดหมู่ */
            name_th: { type: String, default: "" }, /** ชื่อภาษาไทย */
            name_en: { type: String, default: "" }, /** ชื่อภาษาอังกฤษ */
            score: { type: Number, default: 0 }, /** คะแนน */
            default: []
        }
    },
});

module.exports = mongoose.model('images', imagesSchema, 'images');