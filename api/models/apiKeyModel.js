var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var apiKeySchema = new Schema({
    _id: { type: String, required: true },
    name: { type: String, required: true }, /** แสดงผล */
    platform: { type: String, required: true, default: "bot" }, /** bot */
    isActive: { type: Boolean, required: true }, /** เปิดหรือปิด ใช้งานเข้าสู่ระบบ */
    isLogOn: { type: Boolean, default: false }, /** online to Server */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
});

module.exports = mongoose.model('apikey_service', apiKeySchema, 'apikey');