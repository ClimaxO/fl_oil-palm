var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tmiProgressSchema = new Schema({
    _id: { type: String, required: true },
    name_th: { type: String, required: true }, /** สถานะ */
    name_en: { type: String, required: true }, /** สถานะ */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
});

module.exports = mongoose.model('tmiProgressModel', tmiProgressSchema, 'tmiProgressModel');