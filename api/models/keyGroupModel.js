var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var keyGroupSchema = new Schema({
    _id: { type: String, required: true },
    name_th: { type: String, required: true }, /** แสดงผล */
    name_en: { type: String, required: true }, /** แสดงผล */
    keyMatch: { type: String, required: true }, /** จะตรงกับโปรแกรมเทรน */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
});

module.exports = mongoose.model('keyGroup', keyGroupSchema, 'keyGroup');