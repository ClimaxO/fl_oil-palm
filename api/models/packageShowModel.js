var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const crud = require('../functions/crud');

var packageSchema = new Schema({
    _id: { type: String, required: true },
    name: { type: String, required: true }, /** ชื่อ Package */
    detail: { type: String, required: true }, /** รายละเอียด Package */
    price: { type: Number, required: true }, /** ราคาซื้อ Package */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
    isActive: { type: Boolean, required: true }, /** เปิดหรือปิด ใช้งาน */
    can_used_storage: { type: Number, default: "" }, /** กำหนดหน่วยเป็น Bytes */
    expiry_type: { type: String, default: "" }, /** รู้แบบการใช้งาน กำหนดหมดอายุ หรือ ใช้ได้ตลอดชีพ , SET_TIME = กำหนดเวลาหมดอายุ , LIFE_TIME = ตลอดชีพ */
    expiry: { /** กำหนดวันหมดอายุหลังจากเปิดใช้งาน */
        d: { type: Number, default: 0 },
        m: { type: Number, default: 0 },
        y: { type: Number, default: 0 },
        h: { type: Number, default: 0 },
        m: { type: Number, default: 0 },
    }, /** กำหนดหมดอายุ */
});

module.exports = mongoose.model('package_show', packageSchema, 'package_show');