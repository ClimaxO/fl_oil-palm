var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var palmPriceSchema = new Schema({
    _id: { type: String, required: true },/** id เทรน และ ชื่อโฟลเดอร์ */
    title: { type: String, required: true },/** อัพเดทวันที่ดึงจากเว็บที่ไปเอาข้อมูลมาแสดง */
    prices: {
        type: Object,
        title: { type: String, default: "" },
        price: { type: Number, default: 0 },
        unit: { type: String, default: "" },
        default: []
    },/** อัพเดทราคาที่ดึงจากเว็บที่ไปเอาข้อมูลมาแสดง */
    price_avg: {
        title: { type: String, default: "" },
        price: { type: Number, default: 0 },
        unit: { type: String, default: "" },
    },
    isActive: { type: Boolean, required: true }, /** เปิด/ปิด */
    create_date: { type: Date, required: true },
    update_date: { type: Date, required: true },
});

module.exports = mongoose.model('palmPrice', palmPriceSchema, 'palmPrice');