module.exports = function (io) {
    /**
     * functions
     */
    const crud = require('../functions/crud');

    /**
     * models
     */
    const accountModel = require('../models/accountModel');
    const packageShowModel = require('../models/packageShowModel');

    return {
        main: async function (req, res) {
            res.json({
                status: true,
                msg: "OK",
                data: await crud.get_index(accountModel)
            })
        },
        detail: function (req, res) {
            res.json({
                status: true,
                msg: "OK",
                data: req.session
            })
        },
        auth_login: function (req, res) {
            var _get = req.params;
            var _post = req.body;

            const _txt_email = String(_post.email).toLowerCase();
            const _txt_pass = String(_post.password);

            if (
                _txt_email === "" ||
                _txt_email === undefined ||
                _txt_pass === "" ||
                _txt_pass === undefined
            ) {
                return res.json({
                    status: false,
                    msg: "email หรือ password ไม่ถูกต้องโปรดลองใหม่"
                })
            }

            accountModel.findOne({ email: _txt_email, isActive: true })
                .exec(async function (err, userInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด"
                        })
                    }

                    if (userInfo) {
                        const isMatch = await crud.comparePassword(_txt_pass, userInfo.password)
                        console.log(isMatch);
                        if (isMatch) {

                            var accessToken = crud.setToken({
                                _id: userInfo._id,
                                role: userInfo.role,
                                expiresIn: "365d" //7d = week , 300 = 5m  
                            });

                            var refreshToken = crud.setToken({
                                _id: userInfo._id,
                                role: userInfo.role,
                                expiresIn: "365d" //7d = week , 300 = 5m  
                            }, "refreshToken");

                            return res.json({
                                status: true,
                                data: {
                                    token: accessToken,
                                    refreshToken: refreshToken,
                                    full_name: userInfo.full_name,
                                    role: userInfo.role,
                                    profile_url: userInfo.profile_url
                                },
                                msg: "เข้าสู่ระบบสำเร็จ"
                            })


                        } else {
                            return res.json({
                                status: false,
                                data: {},
                                msg: "เข้าสู่ระบบไม่สำเร็จโปรดตรวจสอบความถูกต้องใหม่"
                            })
                        }
                    }
                    if (!userInfo) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "เข้าสู่ระบบไม่สำเร็จโปรดตรวจสอบความถูกต้องใหม่ [1]"
                        })
                    }

                })


        },
        package_info: function (req, res) {
            var _get = req.params;
            packageShowModel.aggregate([
                {
                    '$project': {
                        '_id': 1,
                        'name': 1,
                        'detail': 1,
                        'price': 1,
                        'can_used_storage': 1
                    }
                }
            ])
                .exec(async function (err, packageInfo) {

                    if (err || !packageInfo.length) {
                        return res.json({
                            status: false,
                            msg: `ไม่พบข้อมูลโปรดลองใหม่ภายหลัง`,
                            data: []
                        })
                    }

                    res.json({
                        status: true,
                        msg: '',
                        data: packageInfo
                    })
                })

        }
    }
};