module.exports = function (io) {
    /**
     * System
     */
    const fs = require('fs');
    const path = require('path');
    const multer = require('multer');

    /**
     * functions
     */
    const crud = require('../functions/crud');
    const rabbitmq = require('../functions/rabbitmq')(io);

    /**
     * models
     */
    const accountModel = require('../models/accountModel');
    const imagesModel = require('../models/imagesModel');
    const jobsModel = require('../models/jobsModel');
    const palmPriceModel = require('../models/palmPriceModel');

    require('dotenv').config()
    const env = process.env;
    const path_image_user = path.join(__dirname, `${env.PATH_STORAGE}/user`)

    return {
        me_info: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER", "ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }


            accountModel.aggregate([
                {
                    '$match': {
                        '_id': token._id
                    }
                }, {
                    '$project': {
                        'full_name': 1,
                        'role': 1,
                        'profile_url': 1,
                        'create_date': 1,
                        'total_used_storage': 1,
                        'package_default_id': 1
                    }
                }, {
                    '$lookup': {
                        'from': 'package_show',
                        'localField': 'package_default_id',
                        'foreignField': '_id',
                        'as': 'packageInfo'
                    }
                }, {
                    '$unwind': {
                        'path': '$packageInfo',
                        'preserveNullAndEmptyArrays': true
                    }
                }, {
                    '$project': {
                        'full_name': 1,
                        'role': 1,
                        'profile_url': 1,
                        'create_date': 1,
                        'total_used_storage': 1,
                        'package_default_id': 1,
                        'packageInfo': {
                            'name': '$packageInfo.name',
                            'detail': '$packageInfo.detail',
                            'can_used_storage': '$packageInfo.can_used_storage'
                        }
                    }
                }
            ])
                .exec(async function (err, userInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด"
                        })
                    }

                    res.json({
                        status: (userInfo.length ? true : false),
                        data: (userInfo.length ? userInfo[0] : {}),
                        msg: undefined
                    })

                })

        },
        reset_password: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER", "ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _passsword = (_post.txt_pass ? _post.txt_pass : undefined);
            const _passsword_new = (_post.txt_pass_new ? _post.txt_pass_new : undefined);

            if (!_passsword || !_passsword_new) {
                return res.json({
                    status: false,
                    data: {},
                    msg: "รหัสผ่านไม่ถูกต้อง หรือส่งไม่ครบ [1]"
                })
            }


            accountModel.findOne({
                _id: token._id
            })
                .exec(async function (err, userInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด"
                        })
                    }

                    if (!userInfo) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ไม่พบผู้ใช้งาน"
                        })
                    }

                    const isMatch = await crud.comparePassword(_passsword, userInfo.password)
                    // console.log(isMatch);
                    if (!isMatch) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "รหัสผ่านเก่าไม่ถูกต้อง"
                        })
                    }

                    const password = await crud.hashPassword(_passsword_new);

                    crud.update(accountModel, { password: password, update_date: crud.getTimeZone() }, token._id, {}).then((res_update) => {
                        res.json(res_update);
                    })


                })

        },
        me_update: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER", "ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _full_name = (_post.txt_full_name ? _post.txt_full_name : undefined);

            if (!_full_name) {
                return res.json({
                    status: false,
                    data: {},
                    msg: "ชื่อเต็มผู้ใช้ไม่สามารถว่างได้ [1]"
                })
            }

            crud.update(accountModel, { full_name: _full_name, update_date: crud.getTimeZone() }, token._id, {}).then((res_update) => {
                res.json(res_update);
            })

        },
        me_update_profile: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER", "ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _path_dir_user = `${path_image_user}/${token._id}`;

            await crud.create_dir(_path_dir_user)

            const storage = multer.diskStorage({
                destination: (req, file, cb) => {
                    cb(null, _path_dir_user)
                },
                filename: (req, file, cb) => {
                    let fileName = `${crud.getTimeZone().getTime()}-${crud.uuid(4)}-${crud.uuid(6)}`;
                    switch (file.mimetype) {
                        case 'image/png':
                            fileName += ".png"
                            break;
                        case 'image/jpg':
                        case 'image/jpeg':
                            fileName += ".jpg"
                            break;

                        default:
                            break;
                    }
                    cb(null, fileName)
                },
            })

            var upload = multer({
                storage: storage,
                // limits: {
                //   fileSize: 1024 * 1024 * 5
                // },
                fileFilter: (req, file, cb) => {
                    if (
                        file.mimetype == 'image/png' ||
                        file.mimetype == 'image/jpg' ||
                        file.mimetype == 'image/jpeg'
                    ) {
                        cb(null, true)
                    } else {
                        cb(null, false)
                        return cb('Only .png, .jpg and .jpeg format allowed!')
                    }
                },
            }).single('profile');

            upload(req, res, async function (err) {

                if (err) {
                    return res.json({
                        status: false,
                        msg: err,
                    })
                }

                crud.update(accountModel, { profile_url: req.file.filename, update_date: crud.getTimeZone() }, token._id, { file: req.file }).then((res_upload) => {
                    res.json({ ...res_upload, filename: req.file.filename })
                })

            })

        },
        create: function (req, res) {
            var token = req.user;

            // if (token) {
            //     console.log("token", token);
            //     return
            // }

            const _remove_file_for_error = ((files = []) => {
                for (var i = 0; i < files.length; i++) {
                    const _files = files[i];
                    const _full_path = _files.path;
                    fs.unlink(_full_path, (err) => {
                        if (err) {
                            console.error('Error deleting file:', err);
                            return;
                        }

                        console.log('File deleted successfully ', _full_path);
                    });
                }
            })

            const storage = multer.diskStorage({
                destination: (req, file, cb) => {
                    cb(null, path_image_user)
                },
                filename: (req, file, cb) => {
                    let fileName = `${token._id}-${crud.uuid(3)}-${crud.uuid(6)}`;
                    switch (file.mimetype) {
                        case 'image/png':
                            fileName += ".png"
                            break;
                        case 'image/jpg':
                        case 'image/jpeg':
                            fileName += ".jpg"
                            break;

                        default:
                            break;
                    }
                    cb(null, fileName)
                },
            })

            var upload = multer({
                storage: storage,
                // limits: {
                //   fileSize: 1024 * 1024 * 5
                // },
                fileFilter: (req, file, cb) => {
                    if (
                        file.mimetype == 'image/png' ||
                        file.mimetype == 'image/jpg' ||
                        file.mimetype == 'image/jpeg'
                    ) {
                        cb(null, true)
                    } else {
                        cb(null, false)
                        return cb('Only .png, .jpg and .jpeg format allowed!')
                    }
                },
            }).array('images');

            upload(req, res, async function (err) {

                if (err) {
                    return res.json({
                        status: false,
                        msg: err,
                        images: [],

                        // path: path.join(__dirname, env.PATH_STORAGE)
                    })
                }

                const _post = req.body; /** ดึง POST พร้อมการอัพโหลดไฟล์ */
                // return res.json(_post)

                const _index_jobs = await crud.get_index(jobsModel, { account_id: token._id });
                const _set_data = {
                    _id: (`${crud.uuid(3, 0)}-${String(_index_jobs.index)}`),
                    account_id: token._id, /** id ผู้ใช้ */
                    index: _index_jobs.index, /** ลำดับ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                    progress: "01", /** สถานะ */
                    total_size_bytes: 0,
                    tmiModeUse: { /** model ที่ใช้ประเมินผลภาพ */
                        _id: "", /** id model */
                        version: "",/** id model */
                    },
                    group_rank: {
                        top_best: {
                            _id: "", /** id หมวดหมู่ */
                            name_th: "", /** ชื่อภาษาไทย */
                            name_th: "", /** ชื่อภาษาอังกฤษ */
                            score: 0, /** คะแนน */
                            weight: 0, /** น้ำหนัก */
                            price: 0, /** ราคา */
                        },
                        group_list: []
                    },
                    form_data: {
                        seller_id: (_post.seller_id ? _post.seller_id : ""), /** รหัสผู้ขาย */
                        seller_name: (_post.seller_name ? _post.seller_name : ""), /** ชื่อผู้ขาย */
                        plot_id: (_post.plot_id ? _post.plot_id : ""), /** แปลงที่ */
                        driver_name: (_post.driver_name ? _post.driver_name : ""), /** ชื่อผู้ขับ */
                        car_no_id: (_post.car_no_id ? _post.car_no_id : ""), /** ทะเบียนรถ */
                        weigher_name: (_post.weigher_name ? _post.weigher_name : ""), /** ผู้ชั่งชื่อ */
                        evaluation_officer: (_post.evaluation_officer ? _post.evaluation_officer : ""), /** เจ้าหน้าที่ประเมิน */
                    },
                    isActive: true, /** เปิดหรือปิด ใช้งาน */
                }

                crud.create(jobsModel, _set_data, _set_data).then((res_create_jobs) => {
                    if (res_create_jobs.status) {

                        let _total_size = 0;
                        const _images_create = []
                        for (var i = 0; i < req.files.length; i++) {

                            const _file_name = req.files[i].filename;
                            _total_size += req.files[i].size;

                            const _set_data_image = {
                                _id: `${crud.uuid(12, 0)}-${crud.uuid(13, 0)}`,
                                account_id: token._id, /** id ผู้ใช้ */
                                jobs_id: res_create_jobs.data._id, /** id jobs */
                                file_name: _file_name, /** ชื่อไฟล์ */
                                file_size_bytes: req.files[i].size, /** ขนาดไฟล์คำนวณเป็นไปต์ */
                                create_date: crud.getTimeZone(),
                                update_date: crud.getTimeZone(),
                                tmiModeUse: { /** model ที่ใช้ประเมินผลภาพ */
                                    _id: "", /** id model */
                                    version: "",/** id model */
                                },
                                tmiMode: { /** เชื่อมกับระบบเทรน */
                                    _id: "", /** id model */
                                    version: "",/** id model */
                                    confirm_group: {
                                        _id: "", /** id หมวดหมู่ */
                                        name_th: "", /** ชื่อภาษาไทย */
                                        name_en: "", /** ชื่อภาษาอังกฤษ */
                                    }
                                },
                                group_rank: {
                                    top_best: {
                                        _id: "0", /** id หมวดหมู่ */
                                        name_th: "", /** ชื่อภาษาไทย */
                                        name_th: "", /** ชื่อภาษาอังกฤษ */
                                        score: 0, /** คะแนน */
                                    },
                                    group_list: []
                                },
                            }
                            _images_create.push(_set_data_image)
                        }
                        crud.update(jobsModel, { total_size_bytes: _total_size }, res_create_jobs.data._id);
                        crud.account_update_size_bytes(token._id, _total_size);

                        crud.create(imagesModel, _images_create, _images_create).then((res_create) => {
                            if (res_create.status) {

                                /**
                                 * Add to Queue Service
                                 */
                                rabbitmq.main(res_create_jobs.data._id);

                                res.json({
                                    status: true,
                                    msg: "สร้างรายการสำเร็จ โปรดรอระบบคำนวณสักครู่ ตามคิวในระบบ",
                                    jobs: res_create_jobs.data._id,
                                    log: {
                                        res_create_jobs: res_create_jobs,
                                        res_create: res_create
                                    }
                                })
                            } else {
                                res.json(res_create)
                            }
                        }).catch((err) => {
                            res.json(err)
                            _remove_file_for_error(req.files);
                        })

                    } else {
                        /**
                         * รอเขียนลบไฟล์ที่อัพโหลดมาแล้ว
                         */
                        res.json(res_create_jobs)
                    }
                }).catch((err) => {
                    res.json(err);
                    _remove_file_for_error(req.files);
                })


            })


        },
        imageView: function (req, res) {
            var _get = req.params;
            var _post = req.body;

            if (!_get.fileName) {
                return res.status(404).send('Not found');
            }

            const filePath = `${path_image_user}/${_get.fileName}`;

            fs.access(filePath, fs.constants.F_OK, (err) => {
                if (err) {
                    console.error('File does not exist:', filePath);
                    return res.status(404).send('Not found');
                }
                let extension = filePath.split('.').pop();
                let contentType = "";

                switch (extension) {
                    case "jpge":
                        contentType = 'image/jpge';
                        break;
                    case "jpg":
                        contentType = 'image/jpg';
                        break;
                    case "png":
                        contentType = 'image/png';
                        break;
                }

                fileToLoad = fs.readFileSync(filePath);
                res.writeHead(200, { 'Content-Type': contentType });
                res.end(fileToLoad, 'binary');
                console.log('File exists:', filePath);
            });

        },
        imageProfileView: function (req, res) {
            var _get = req.params;
            var _post = req.body;

            const _fileName = (_get.fileName ? _get.fileName : undefined);
            if (!_fileName) {
                return res.status(404).send('Not found');
            }


            accountModel.findOne({ profile_url: _fileName })
                .exec(async function (err, userInfo) {
                    if (err) {
                        return res.status(404).send('Not found [0]');
                    }

                    if (!userInfo) {
                        return res.status(404).send('Not found [1]');
                    }

                    const filePath = `${path_image_user}/${userInfo._id}/${_fileName}`;

                    fs.access(filePath, fs.constants.F_OK, (err) => {
                        if (err) {
                            console.error('File does not exist:', filePath);
                            return res.status(404).send('Not found');
                        }
                        let extension = filePath.split('.').pop();
                        let contentType = "";

                        switch (extension) {
                            case "jpge":
                                contentType = 'image/jpge';
                                break;
                            case "jpg":
                                contentType = 'image/jpg';
                                break;
                            case "png":
                                contentType = 'image/png';
                                break;
                        }

                        fileToLoad = fs.readFileSync(filePath);
                        res.writeHead(200, { 'Content-Type': contentType });
                        res.end(fileToLoad, 'binary');
                        console.log('File exists:', filePath);
                    });


                })

        },
        get_palm_info: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            res.json(await crud._get_palm_price());

        },
        history_now: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const jobsId = _get.jobsId;
            let _time_start = crud.getTimeZone();
            let _time_end = crud.getTimeZone();
            _time_start.setHours(0, 0, 0);
            _time_end.setHours(23, 59, 59);

            jobsModel.aggregate([
                {
                    '$match': {
                        'account_id': token._id,
                        'create_date': {
                            $gte: _time_start,
                            $lte: _time_end
                        },
                        'isActive': true
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'group_top_bast': '$group_rank.top_best',
                        'create_date': 1,
                        'date': {
                            '$dateToString': {
                                'date': '$create_date',
                                'format': '%d/%m/%Y',
                                'timezone': '+07:00'
                            }
                        },
                        'form_data': 1
                    }
                }, {
                    '$lookup': {
                        'from': 'images',
                        'localField': '_id',
                        'foreignField': 'jobs_id',
                        'as': 'imagesInfo'
                    }
                }, {
                    '$group': {
                        '_id': 'null',
                        'data': {
                            '$push': '$$ROOT'
                        },
                        'total_image': {
                            '$sum': {
                                '$size': '$imagesInfo'
                            }
                        }
                    }
                }, {
                    '$unwind': {
                        'path': '$data',
                        'preserveNullAndEmptyArrays': true
                    }
                }, {
                    '$project': {
                        '_id': '$data._id',
                        'create_date': '$data.create_date',
                        'group_top_bast': '$data.group_top_bast',
                        'date': '$data.date',
                        'imagesInfo': '$data.imagesInfo',
                        'count_image': {
                            '$sum': {
                                '$size': '$data.imagesInfo'
                            }
                        },
                        'total_image': 1,
                        'form_data': '$data.form_data'
                    }
                }, {
                    '$facet': {
                        'data': [
                            {
                                '$sort': {
                                    'create_date': -1
                                }
                            }, {
                                '$project': {
                                    '_id': 1,
                                    'group_top_bast': 1,
                                    'date': 1,
                                    'count_image': 1,
                                    'total_image': 1,
                                    'form_data': 1
                                }
                            }
                        ],
                        'chart': [
                            {
                                '$project': {
                                    '_id': 1,
                                    'group_top_bast': 1,
                                    'date': 1,
                                    'total_image': 1,
                                    'count_image': 1,
                                    'imagesInfo': 1
                                }
                            }, {
                                '$unwind': {
                                    'path': '$imagesInfo',
                                    'preserveNullAndEmptyArrays': true
                                }
                            }, {
                                '$group': {
                                    '_id': '$imagesInfo.group_rank.top_best._id',
                                    'name_th': {
                                        '$first': '$imagesInfo.group_rank.top_best.name_th'
                                    },
                                    'name_en': {
                                        '$first': '$imagesInfo.group_rank.top_best.name_en'
                                    },
                                    'images': {
                                        '$push': '$imagesInfo'
                                    },
                                    'total_image': {
                                        '$first': '$total_image'
                                    },
                                    'count_image': {
                                        '$first': '$count_image'
                                    },
                                    'score': {
                                        '$sum': 1
                                    }
                                }
                            }, {
                                '$project': {
                                    '_id': 1,
                                    'images': 1,
                                    'name_th': 1,
                                    'name_en': 1,
                                    'total_image': 1,
                                    'count_image': 1,
                                    'percen': {
                                        '$multiply': [
                                            {
                                                '$divide': [
                                                    '$score', '$total_image'
                                                ]
                                            }, 100
                                        ]
                                    },
                                    'score': 1
                                }
                            }, {
                                '$sort': {
                                    'percen': -1
                                }
                            }
                        ],
                        'title_chart': [
                            {
                                '$group': {
                                    '_id': null,
                                    'date': {
                                        '$first': {
                                            '$dateToString': {
                                                'date': '$create_date',
                                                'format': '%d/%m/%Y',
                                                'timezone': '+07:00'
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    }
                }, {
                    '$project': {
                        '_id': null,
                        'data': 1,
                        'chart': 1,
                        'title': {
                            '$arrayElemAt': [
                                '$title_chart', 0
                            ]
                        }
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'data': 1,
                        'chart': 1,
                        'title': '$title.date'
                    }
                }
            ])
                .exec(async function (err, jobsInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด"
                        })
                    }

                    res.json({
                        status: true,
                        data: (jobsInfo.length ? jobsInfo[0]["data"] : []),
                        chart: (jobsInfo.length ? jobsInfo[0]["chart"] : []),
                        title: (jobsInfo.length ? jobsInfo[0]["title"] : []),
                        msg: undefined
                    })

                })

        },
        history_group: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const jobsId = _get.jobsId;
            let _time_start = crud.getTimeZone();
            let _time_end = crud.getTimeZone();
            _time_start.setHours(0, 0, 0);
            _time_end.setHours(23, 59, 59);

            jobsModel.aggregate([
                {
                    '$match': {
                        'account_id': token._id,
                        'isActive': true
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'index': 1,
                        'group_top_bast': '$group_rank.top_best',
                        'create_date': 1,
                        'date': {
                            '$dateToString': {
                                'date': '$create_date',
                                'format': '%d/%m/%Y',
                                'timezone': '+07:00'
                            }
                        },
                        'form_data': 1
                    }
                }, {
                    '$lookup': {
                        'from': 'images',
                        'localField': '_id',
                        'foreignField': 'jobs_id',
                        'as': 'imagesInfo'
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'index': 1,
                        'group_top_bast': 1,
                        'date': 1,
                        'create_date': 1,
                        'total_image': {
                            '$size': '$imagesInfo'
                        },
                        'form_data': 1
                    }
                }, {
                    '$sort': {
                        'create_date': -1
                    }
                }, {
                    '$group': {
                        '_id': '$date',
                        'create_date': { '$first': '$create_date' },
                        'jobs': {
                            '$push': '$$ROOT'
                        }
                    }
                },
                {
                    '$sort': {
                        'create_date': -1
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'jobs': 1
                    }
                }
            ])
                .exec(async function (err, jobsInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด"
                        })
                    }

                    res.json({
                        status: true,
                        data: jobsInfo,
                        msg: undefined
                    })

                })

        },
        historyByJobsId: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const jobsId = _get.jobsId;

            jobsModel.aggregate([
                {
                    '$match': {
                        '_id': jobsId,
                        'account_id': token._id,
                        'isActive': true
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'progress': 1,
                        'group_rank': 1,
                        'progress_date': 1,
                        'success_date': 1,
                        'tmiModeUse': 1,
                        'form_data': 1
                    }
                }
            ])
                .exec(async function (err, jobsInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด"
                        })
                    }
                    if (!jobsInfo.length) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: `ไม่พบรายงานที่คุณต้องการ [${jobsId}]`
                        })
                    }

                    const _report = (() => {
                        return new Promise((resolve) => {
                            imagesModel.aggregate([
                                {
                                    '$match': {
                                        'jobs_id': jobsId
                                    }
                                }, {
                                    '$group': {
                                        '_id': '$group_rank.top_best._id',
                                        'name_th': {
                                            '$first': '$group_rank.top_best.name_th'
                                        },
                                        'name_en': {
                                            '$first': '$group_rank.top_best.name_en'
                                        },
                                        'score': {
                                            '$first': 0
                                        },
                                        'images': {
                                            '$push': {
                                                '_id': '$group_rank.top_best._id',
                                                'name_th': '$group_rank.top_best.name_th',
                                                'name_en': '$group_rank.top_best.name_en',
                                                'tmiModeUse': '$tmiModeUse',
                                                'score': {
                                                    '$trunc': [
                                                        {
                                                            '$multiply': [
                                                                '$group_rank.top_best.score', 100
                                                            ]
                                                        }, 2
                                                    ]
                                                },
                                                'file_name': '$file_name'
                                            }
                                        }
                                    }
                                }
                            ])
                                .exec(async function (err, imagesInfo) {
                                    resolve(imagesInfo.length ? imagesInfo : [])
                                })
                        })
                    })

                    const _get_image = (() => {
                        return new Promise((resolve) => {
                            imagesModel.find({ jobs_id: jobsId })
                                .exec(async function (err, imagesInfo) {
                                    resolve(imagesInfo.length ? imagesInfo : [])
                                })
                        })
                    })

                    const _jobs_info = jobsInfo[0];
                    let _set_res = { ..._jobs_info, images: (_jobs_info.progress !== "03" ? await _get_image() : []), images_group: (_jobs_info.progress === "03" ? await _report() : []) }

                    if (_jobs_info.progress === "03") {
                        for (let index = 0; index < _set_res.group_rank.group_list.length; index++) {
                            const _group = _set_res.group_rank.group_list[index];
                            for (let i = 0; i < _set_res.images_group.length; i++) {
                                const _group_img = _set_res.images_group[i];
                                if (_group._id === _group_img._id) {
                                    _set_res.images_group[i].score = _group.score
                                }
                            }
                        }
                    }

                    _set_res.images_group = _set_res.images_group.sort((a, b) => b.score - a.score);

                    res.json({
                        status: true,
                        data: _set_res,
                        msg: undefined
                    })

                })

        },
        history_jobsid_update_weigth: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["USER"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const jobsId = (_get.jobsId ? _get.jobsId : undefined);
            const _weigth = (_post.weigth ? _post.weigth : undefined);

            if (!jobsId || !_weigth) {
                return res.json({
                    status: false,
                    data: {},
                    msg: `โปรดใส่ข้อมูลให้ครบ [${!jobsId ? '1' : (!_weigth ? '2' : '0')}]`
                })
            }

            const _get_jobs_info = (() => {
                return new Promise((resolve) => {
                    jobsModel.findOne({
                        _id: jobsId,
                        isActive: true
                    })
                        .exec(async function (err, jobsInfo) {
                            const _status = (jobsInfo ? (jobsInfo.group_rank.top_best.weight === 0 || jobsInfo.group_rank.top_best.price === 0 ? true : false) : false);
                            resolve({
                                status: _status,
                                msg: (!_status ? (jobsInfo ? `รายงานที่คุณมีการอัพเดทน้ำหนักแล้ว น้ำหนัก ${jobsInfo.group_rank.top_best.weight} กิโลกรัม , ราคาประมาณ ${(jobsInfo.group_rank.top_best.price).toFixed(2)} ฿` : "ไม่พบรายงานที่ต้องการอัพเดท") : ""),
                                data: {}
                            })
                        })
                })
            })

            const _jobsInfo = await _get_jobs_info();

            /**
             * เช็คว่าเคยอัพเดทน้ำหนักแล้วหรือยัง
             */
            if (!_jobsInfo.status) {
                return res.json(_jobsInfo);
            }

            imagesModel.aggregate([
                {
                    '$match': {
                        'jobs_id': jobsId
                    }
                }, {
                    '$group': {
                        '_id': '$group_rank.top_best._id',
                        'name_th': {
                            '$first': '$group_rank.top_best.name_th'
                        },
                        'score': {
                            '$push': '$group_rank.top_best.score'
                        }
                    }
                }, {
                    '$sort': {
                        '_id': -1
                    }
                }, {
                    '$match': {
                        '_id': {
                            '$in': [
                                '04', '03'
                            ]
                        }
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'name_th': 1,
                        'score': 1,
                        'score_avg': {
                            '$avg': '$score'
                        },
                        'percen': {
                            '$trunc': [
                                {
                                    '$multiply': [
                                        {
                                            '$avg': '$score'
                                        }, 100
                                    ]
                                }, 2
                            ]
                        }
                    }
                }
            ])
                .exec(async function (err, imagesInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: "ระบบเกิดข้อผิดพลาด [1]"
                        })
                    }

                    if (!imagesInfo) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: `ไม่พบรายงานที่คุณต้องการอัพเดท [${jobsId}] [2]`
                        })
                    }

                    const _price_palm = await crud._get_palm_price();

                    /**
                     * เช็คราคาล่าสุด
                     */
                    if (!_price_palm.status) {
                        return res.json({
                            status: false,
                            data: {},
                            msg: `เกิดข้อผิดพลาดไม่สามารถดึงข้อมูลราคาปัจจุบันได้`
                        })
                    }

                    if (!imagesInfo.length) {
                        /**
                         * กรณีไม่มีข้อมูลของ สุกมาก และ สุกเลย จะอัพเดทเป็นไม่ต้องใส่ราคา
                         */
                        return crud.update(jobsModel, { "group_rank.top_best.weight": -1, "group_rank.top_best.price": -1, update_date: crud.getTimeZone() }, jobsId, { jobsId: jobsId, images: imagesInfo.length }).then((res_update) => {
                            res.json({
                                status: false,
                                data: {},
                                msg: `รายงานของคุณไม่สามารถประเมินราคาได้เนื่องจากไม่มีผลประเมินค่าความ สุกมาก , สุก`
                            })
                        })
                    }

                    let x = _weigth;
                    let y = (imagesInfo.length ? imagesInfo[0].percen : 0);
                    let t = _price_palm.data.price;

                    let z = x * (y / 100);
                    let i = z * t;

                    console.log("z =", z);
                    console.log("i =", i);
                    crud.update(jobsModel, { "group_rank.top_best.weight": _weigth, "group_rank.top_best.price": i, update_date: crud.getTimeZone() }, jobsId, { post: _post, get: _get, jobsId: jobsId, imagesInfo: imagesInfo, cale: { x: x, y: y, t: t, z: z, i: i } }).then((res_update) => {
                        res.json({ ...res_update, tmp: imagesInfo });
                    })

                })

        },
    }
};