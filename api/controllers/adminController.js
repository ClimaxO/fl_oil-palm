module.exports = function (io) {
    /**
     * System
     */
    const fs = require('fs');
    const path = require('path');
    const multer = require('multer');

    /**
     * functions
     */
    const crud = require('../functions/crud');
    const rabbitmq = require('../functions/rabbitmq')(io);
    const socketCtl = require('../functions/socketCtl')(io);
    const pricePalm = require('../functions/pricePalm')();

    /**
     * models
     */
    const accountModel = require('../models/accountModel');
    const packageShowModel = require('../models/packageShowModel');
    const progressModel = require('../models/progressModel');
    const imagesModel = require('../models/imagesModel');
    const keyGroupModel = require('../models/keyGroupModel');
    const TMIModel = require('../models/TMIModel');
    const TMIProgressModel = require('../models/TMIProgressModel');
    const apiKeyModel = require('../models/apiKeyModel');
    const palmPriceModel = require('../models/palmPriceModel');

    require('dotenv').config()
    const env = process.env;
    const path_model = path.join(__dirname, `${env.PATH_STORAGE}/model`);
    const _room_admin = `admin:log`;

    return {
        create: async function (req, res) {
            var _get = req.params;
            var _post = req.body;

            if (_post.key !== "kitaro") {
                return res.status(404).json({
                    status: false,
                    msg: "KEY NULL"
                })
            }

            const _index = await crud.get_index(accountModel)

            const _set_data = {
                _id: (`${crud.uuid(10)}-${_index.index}`),
                full_name: _post.full_name, /** ชื่อบริษัท , ชื่อแอดมิน */
                index: _index.index, /** ลำดับ */
                email: _post.email, /** อีเมล์ Login */
                password: await crud.hashPassword(_post.password), /** อีเมล์ Login */
                profile_url: "default.png", /** ภาพเริ่มต้น */
                role: _post.role, /** Role : USER , ADMIN */
                create_date: crud.getTimeZone(),
                update_date: crud.getTimeZone(),
                isActive: true, /** เปิดหรือปิด ใช้งานเข้าสู่ระบบ */
                package_default_id: "YVslwduDjXOp_Zk", /** package เริ่มต้นหลังจาก แบบซื้อหมดอายุ */
                package_id: "", /** บอกบริการที่กำลังใช้งานว่าใช้แบบกี่ GB , TB อื่น ๆ  */
                total_used_storage: 0, /** หน่วยเป็น Bytes */
            }

            crud.create(accountModel, _set_data, { _post: _post, _get: _get, _set_data: _set_data, _index: _index }).then((res_create) => {
                res.json(res_create);
            }).catch(err => {
                res.json(err);
            })



        },
        create_apikey: async function (req, res) {
            var _get = req.params;
            var _post = req.body;

            if (_post.key !== "kitaro") {
                return res.status(404).json({
                    status: false,
                    msg: "KEY NULL"
                })
            }

            const _set_data = {
                _id: `${crud.uuid(12, 2)}`,
                name: `BOT Train Model`, /** แสดงผล */
                platform: `bot`, /** bot */
                isActive: true, /** เปิดหรือปิด ใช้งานเข้าสู่ระบบ */
                isLogOn: false, /** online to Server */
                create_date: crud.getTimeZone(),
                update_date: crud.getTimeZone(),
            }

            crud.create(apiKeyModel, _set_data, { _post: _post, _get: _get, _set_data: _set_data }).then((res_create) => {
                res.json(res_create);
            }).catch(err => {
                res.json(err);
            })



        },
        create_package: async function (req, res) {
            var _get = req.params;
            var _post = req.body;

            if (_post.key !== "kitaro") {
                return res.status(404).json({
                    status: false,
                    msg: "KEY NULL"
                })
            }

            const _set_data = {
                _id: crud.uuid(15),
                name: "Package เริ่มต้น", /** ชื่อ Package */
                detail: "สำหรับเริ่มต้นใช้งาน 15 GB", /** รายละเอียด Package */
                price: 0, /** ราคาซื้อ Package */
                create_date: crud.getTimeZone(),
                update_date: crud.getTimeZone(),
                isActive: false, /** ปิดใช้งานให้ทำการเพิ่มอัตโนมัติเท่านั้น */
                can_used_storage: 16106127360, /** กำหนดหน่วยเป็น 15 GB = 16106127360 Bytes */
                expiry_type: "LIFE_TIME", /** รู้แบบการใช้งาน กำหนดหมดอายุ หรือ ใช้ได้ตลอดชีพ , SET_TIME = กำหนดเวลาหมดอายุ , LIFE_TIME = ตลอดชีพ */
                expiry: { /** กำหนดวันหมดอายุหลังจากเปิดใช้งาน */
                    d: 0,
                    m: 0,
                    y: 0,
                    h: 0,
                    m: 0,
                }, /** กำหนดหมดอายุ */
            }


            crud.create(packageShowModel, _set_data, { _post: _post, _get: _get, _set_data: _set_data }).then((rea_create) => {
                res.json(rea_create);
            })

        },
        create_progress: async function (req, res) {

            const _create_list = [
                {
                    _id: "01",
                    name_th: "รอดำเนินการ", /** สถานะ */
                    name_en: "Pending", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "02",
                    name_th: "เกิดข้อผิดพลาด", /** สถานะ */
                    name_en: "Error", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "03",
                    name_th: "ดำเนินการสำเร็จ", /** สถานะ */
                    name_en: "Success", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                }
            ]


            crud.create(progressModel, _create_list, { _create_list: _create_list }).then((rea_create) => {
                res.json(rea_create);
            })

        },
        create_progress_tmi: async function (req, res) {

            const _create_list = [
                {
                    _id: "01",
                    name_th: "รอทำการ Training Model", /** สถานะ */
                    name_en: "Waiting for model training", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "02",
                    name_th: "เริ่ม Training Model", /** สถานะ */
                    name_en: "Start Training Model", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "03",
                    name_th: "เกิดข้อผิดพลาด", /** สถานะ */
                    name_en: "Error", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "04",
                    name_th: "ดำเนินการสำเร็จ", /** สถานะ */
                    name_en: "Training Success", /** สถานะ */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                }
            ]


            crud.create(TMIProgressModel, _create_list, { _create_list: _create_list }).then((rea_create) => {
                res.json(rea_create);
            })

        },
        create_key_group: async function (req, res) {

            const _create_list = [
                {
                    _id: "01",
                    name_th: "ดิบ", /** แสดงผล */
                    name_en: "Raw", /** แสดงผล */
                    keyMatch: "Raw", /** จะตรงกับโปรแกรมเทรน */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "02",
                    name_th: "ครึ่งสุก", /** แสดงผล */
                    name_en: "Half-Ripe", /** แสดงผล */
                    keyMatch: "Half-Ripe", /** จะตรงกับโปรแกรมเทรน */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "03",
                    name_th: "สุก", /** แสดงผล */
                    name_en: "Ripe", /** แสดงผล */
                    keyMatch: "Ripe", /** จะตรงกับโปรแกรมเทรน */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                },
                {
                    _id: "04",
                    name_th: "สุกมาก", /** แสดงผล */
                    name_en: "Very Ripe", /** แสดงผล */
                    keyMatch: "Very Ripe", /** จะตรงกับโปรแกรมเทรน */
                    create_date: crud.getTimeZone(),
                    update_date: crud.getTimeZone(),
                }
            ]


            crud.create(keyGroupModel, _create_list, { _create_list: _create_list }).then((rea_create) => {
                res.json(rea_create);
            })

        },
        queue_mq: function (req, res) {
            var _get = req.params;
            var _post = req.body;
            rabbitmq.main(_post.jobsId);
            res.json({
                status: true,
                jobsId: _post.jobsId
            })
        },
        queue_train_mq: function (req, res) {
            var _get = req.params;
            var _post = req.body;
            rabbitmq.main_train(_post.model_id);
            res.json({
                status: true,
                model_id: _post.model_id
            })
        },
        empty_train: function (req, res) {
            var _get = req.params;
            var _post = req.body;

            imagesModel.aggregate([
                {
                    '$match': {
                        'tmiMode._id': _post.model_id
                    }
                }, {
                    '$group': {
                        '_id': null,
                        'id': {
                            '$push': '$_id'
                        }
                    }
                }
            ])
                .exec(async function (err, imagesInfo) {

                    if (!imagesInfo.length) {
                        return res.json({
                            status: false,
                            msg: { imagesInfo: imagesInfo, err: err }
                        })
                    }

                    crud.update(imagesModel, { 'tmiMode._id': '', 'tmiMode.version': '' }, { $in: imagesInfo[0]["id"] }).then((res_update) => {
                        res.json(res_update);
                    })

                });

        },
        list_model_select: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json({ ..._check_permission, data: [], seelct: "" });
            }

            TMIModel.aggregate([
                {
                    '$match': {
                        'progress': '04'
                    }
                },
                {
                    '$project': {
                        '_id': 1,
                        'version': 1,
                        'public': 1
                    }
                }, {
                    '$facet': {
                        'data': [],
                        'selected': [
                            {
                                '$match': {
                                    'public': true
                                }
                            }
                        ]
                    }
                }, {
                    '$project': {
                        '_id': null,
                        'data': 1,
                        'count': {
                            '$size': '$selected'
                        },
                        'selected': 1
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'data': 1,
                        'selected': {
                            '$cond': [
                                '$count', {
                                    '$arrayElemAt': [
                                        '$selected._id', 0
                                    ]
                                }, 'default'
                            ]
                        }
                    }
                }
            ])
                .exec(async function (err, modelInfo) {

                    if (err) {

                        return res.json({
                            status: true,
                            msg: 'เกิดข้อผิดพลาด [1]',
                            data: [],
                            select: ""
                        })
                    }

                    let _set_data = {
                        status: true,
                        msg: '',
                        data: (modelInfo.length ? modelInfo[0]["data"] : []),
                        select: (modelInfo.length ? modelInfo[0]["selected"] : "")
                    }

                    _set_data.data.unshift({
                        _id: 'default',
                        version: 'Default',
                        public: false
                    })

                    return res.json(_set_data)
                });

        },
        dl_file_train_model: function (req, res) {
            var _get = req.params;
            var _post = req.body;

            const _file_name = (_get.file_name ? _get.file_name : undefined);

            if (!_file_name) {
                return res.status(404).send(`ไม่พบไฟล์ : ${_file_name}`)
            }

            TMIModel.findOne({ 'path.zip_file_train': _file_name })
                .exec(async function (err, fileInfo) {

                    if (!fileInfo) {
                        return res.status(404).send(`ไม่พบไฟล์[1] : ${_file_name}`)
                    }

                    const _file_zip_path_main = path.resolve(`../storage/files-zip`);
                    res.download(`${_file_zip_path_main}/${fileInfo.path.zip_file_train}`, fileInfo.path.zip_file_train, (error) => {
                        if (error) {
                            console.error('Error downloading file:', error);
                            res.status(404).send('File not found');
                        }
                    });

                })



        },
        view_image_model_chart: function (req, res) {
            var _get = req.params;
            var _post = req.body;

            const _file_name = (_get.file_name ? _get.file_name : undefined);
            const _modelId = (_get.modelId ? _get.modelId : undefined);

            if (!_file_name || !_modelId) {
                return res.status(404).send(`ไม่พบไฟล์ : ${_file_name} หรือ Model Id`)
            }

            TMIModel.findOne({ _id: _modelId })
                .exec(async function (err, fileInfo) {

                    if (!fileInfo) {
                        return res.status(404).send(`ไม่พบไฟล์[1] : ${_file_name}`)
                    }

                    const filePath = path.resolve(`../storage/model/${_modelId}/${_file_name}`);

                    fs.access(filePath, fs.constants.F_OK, (err) => {
                        if (err) {
                            console.error('File does not exist:', filePath);
                            return res.status(404).send('Not found');
                        }
                        let extension = filePath.split('.').pop();
                        let contentType = "";

                        switch (extension) {
                            case "jpge":
                                contentType = 'image/jpge';
                                break;
                            case "jpg":
                                contentType = 'image/jpg';
                                break;
                            case "png":
                                contentType = 'image/png';
                                break;
                        }

                        fileToLoad = fs.readFileSync(filePath);
                        res.writeHead(200, { 'Content-Type': contentType });
                        res.end(fileToLoad, 'binary');
                        console.log('File exists:', filePath);
                    });


                })



        },
        crud_palm_list: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            palmPriceModel.aggregate([
                {
                    '$project': {
                        '_id': 1,
                        'title': 1,
                        'prices': 1,
                        'price_avg': 1,
                        'create_date': 1,
                        'isActive': 1
                    }
                },
                {
                    '$sort': {
                        'create_date': -1
                    }
                }
            ])
                .exec(async function (err, priceInfo) {

                    const _set_data = {
                        status: priceInfo.length ? true : false,
                        msg: "",
                        data: priceInfo
                    }

                    res.json(_set_data);

                })

        },
        crud_palm_is_active: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _palm_id = (_post._id ? _post._id : undefined);

            if (!_palm_id) {
                return res.json({
                    status: false,
                    msg: 'ข้อมูลไม่ครบโปรดลองใหม่',
                    data: {}
                })
            }

            palmPriceModel.findOne({ _id: _palm_id })
                .exec(async function (err, palmInfo) {
                    if (err) {
                        return res.json({
                            status: false,
                            msg: 'เกิดข้อผิดพลาด [0]',
                            data: {}
                        })
                    }

                    if (!palmInfo) {
                        return res.json({
                            status: false,
                            msg: 'ไม่พบข้อมูล [1]',
                            data: {}
                        })
                    }

                    const _check_is_active = await (() => {
                        return new Promise((resolve) => {
                            palmPriceModel.aggregate([
                                {
                                    '$match': {
                                        '_id': {
                                            '$nin': [_palm_id]
                                        },
                                        'isActive': true
                                    }
                                }
                            ])
                                .exec(async function (err, priceInfo) {
                                    resolve(priceInfo.length ? true : false);
                                })
                        })
                    })()

                    if ((_check_is_active && palmInfo.isActive === true) || !palmInfo.isActive) {

                        crud.update(palmPriceModel, { isActive: palmInfo.isActive ? false : true, update_date: crud.getTimeZone() }, palmInfo._id, { _post: _post, _palm_id: _palm_id }).then((res_update) => {
                            res.json(res_update)
                        });

                    } else {
                        res.json({
                            status: false,
                            msg: `ไม่สามารถยกเลิกใช้งาน ${palmInfo.title} ได้เนื่องจากรายการราคาไม่มีตัวเริ่มต้นใช้งานจำเป็นต้องเหลือไว้อย่างน้อย 1 รายการ`,
                            data: {}
                        })
                    }

                })

        },
        crud_palm_update: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _res_data = await pricePalm.init()

            res.json(_res_data);

        },
        crud_palm_create: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _title = (_post.title ? _post.title : undefined);
            const _prices = (_post.prices.length ? _post.prices : []);
            const _price_avg = (_post.price_avg ? _post.price_avg : { title: undefined, price: 0, unit: undefined });

            if (!_title || !_prices.length || !_price_avg.title) {
                return res.json({
                    status: false,
                    msg: 'ข้อมูลไม่ครบโปรดลองใหม่',
                    data: {}
                })
            }

            let _set_create = {
                _id: await crud.uuid(6, 2),/** id เทรน และ ชื่อโฟลเดอร์ */
                title: _title,/** อัพเดทวันที่ดึงจากเว็บที่ไปเอาข้อมูลมาแสดง */
                prices: [],/** อัพเดทราคาที่ดึงจากเว็บที่ไปเอาข้อมูลมาแสดง */
                price_avg: {
                    title: _price_avg.title,
                    price: _price_avg.price,
                    unit: _price_avg.unit,
                },
                isActive: true, /** เปิด/ปิด */
                create_date: crud.getTimeZone(),
                update_date: crud.getTimeZone(),
            }

            for (let index = 0; index < _prices.length; index++) {
                const _price = _prices[index];

                if (
                    !_price.title ||
                    !_price.price ||
                    !_price.unit
                ) {
                    return res.json({
                        status: false,
                        msg: `มีบางรายการดังนี้เป็นค่าว่าง Title : ${_price.title} Price : ${_price.price} Unit : ${_price.unit}`,
                        data: {}
                    })
                }

                const _set_data = {
                    title: _price.title,
                    price: _price.price,
                    unit: _price.unit,
                }

                _set_create.prices.push(_set_data);

            }

            crud.create(palmPriceModel, _set_create, { _post: _post, _set_create: _set_create }).then((res_create) => {
                res.json(res_create);
            })


        },
        view_account: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            accountModel.aggregate([
                {
                    '$project': {
                        '_id': 1,
                        'full_name': 1,
                        'index': 1,
                        'email': 1,
                        'role': 1,
                        'create_date': 1,
                        'update_date': 1,
                        'isActive': 1,
                        'total_used_storage': 1
                    }
                }
            ])
                .exec(async function (err, accountInfo) {

                    const _set_data = {
                        status: accountInfo.length ? true : false,
                        data: accountInfo
                    }

                    res.json(_set_data);

                })

        },
        create_account: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _set_data = {
                full_name: (_post.txt_full_name ? _post.txt_full_name : undefined),
                email: (_post.txt_email ? String(_post.txt_email).toLowerCase() : undefined),
                password: (_post.txt_password ? _post.txt_password : undefined),
                role: (_post.txt_role ? _post.txt_role : undefined),
                isActive: (_post.isActive !== "" || _post.isActive !== undefined ? _post.isActive : undefined),
            }

            if (
                !_set_data.full_name ||
                !_set_data.email ||
                !_set_data.role ||
                _set_data.isActive === undefined
            ) {
                return res.json({
                    status: false,
                    msg: `มีข้อมูลว่างโปรดลองใหม่`,
                    data: _set_data
                })
            }

            const _check_email = await (() => {
                return new Promise((resolve) => {
                    accountModel.count({ email: _set_data.email })
                        .exec(async function (err, accountInfo) {
                            resolve(accountInfo ? false : true);
                        })
                })
            })()

            if (!_check_email) {
                return res.json({
                    status: false,
                    msg: `email: ${_set_data.email} มีผู้ใช้แล้ว !`,
                    data: _set_data
                })
            }

            const _index = await crud.get_index(accountModel)

            const _create_data = {
                _id: (`${crud.uuid(10)}-${_index.index}`),
                full_name: _set_data.full_name, /** ชื่อบริษัท , ชื่อแอดมิน */
                index: _index.index, /** ลำดับ */
                email: _set_data.email, /** อีเมล์ Login */
                password: await crud.hashPassword(_set_data.password), /** อีเมล์ Login */
                profile_url: "default.png", /** ภาพเริ่มต้น */
                role: _set_data.role, /** Role : USER , ADMIN */
                create_date: crud.getTimeZone(),
                update_date: crud.getTimeZone(),
                isActive: true, /** เปิดหรือปิด ใช้งานเข้าสู่ระบบ */
                package_default_id: "YVslwduDjXOp_Zk", /** package เริ่มต้นหลังจาก แบบซื้อหมดอายุ */
                package_id: "", /** บอกบริการที่กำลังใช้งานว่าใช้แบบกี่ GB , TB อื่น ๆ  */
                total_used_storage: 0, /** หน่วยเป็น Bytes */
            }

            crud.create(accountModel, _create_data, { _post: _post, _get: _get, _set_data: _set_data, _index: _index }).then((res_create) => {
                res.json(res_create);
            }).catch(err => {
                res.json(err);
            })

        },
        update_account: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _set_data = {
                _id: (_post._id ? _post._id : undefined),
                full_name: (_post.txt_full_name ? _post.txt_full_name : undefined),
                role: (_post.txt_role ? _post.txt_role : undefined),
                isActive: (_post.isActive !== "" || _post.isActive !== undefined ? _post.isActive : undefined),
            }

            if (
                !_set_data._id ||
                !_set_data.full_name ||
                !_set_data.role ||
                _set_data.isActive === undefined
            ) {
                return res.json({
                    status: false,
                    msg: `มีข้อมูลว่างโปรดลองใหม่`,
                    data: _set_data
                })
            }

            accountModel.findOne({
                _id: _set_data._id
            })
                .exec(async function (err, accountInfo) {
                    if (!accountInfo || err) {
                        return res.json({
                            status: false,
                            msg: `ไม่พบข้อมูลผู้ใช้งาน หรือ เกิดข้อผิดพลาด ${_set_data._id}[ID]`,
                            data: {}
                        })
                    }

                    crud.update(accountModel, {
                        full_name: _set_data.full_name,
                        role: _set_data.role,
                        isActive: _set_data.isActive,
                        update_date: crud.getTimeZone()
                    }, _set_data._id, { _set_data: _set_data, _post: _post }).then((res_update) => {

                        res.json(res_update);

                    })

                })

        },
        reset_password_account: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _set_data = {
                _id: (_post._id ? _post._id : undefined),
                password: (_post.txt_pass_new ? _post.txt_pass_new : undefined),
            }

            if (
                !_set_data._id ||
                !_set_data.password
            ) {
                return res.json({
                    status: false,
                    msg: `มีข้อมูลว่างโปรดลองใหม่`,
                    data: _set_data
                })
            }

            accountModel.findOne({
                _id: _set_data._id
            })
                .exec(async function (err, accountInfo) {
                    if (!accountInfo || err) {
                        return res.json({
                            status: false,
                            msg: `ไม่พบข้อมูลผู้ใช้งาน หรือ เกิดข้อผิดพลาด ${_set_data._id}[ID]`,
                            data: {}
                        })
                    }

                    crud.update(accountModel, {
                        password: await crud.hashPassword(_set_data.password),
                        update_date: crud.getTimeZone()
                    }, _set_data._id, { _set_data: _set_data, _post: _post }).then((res_update) => {

                        res.json(res_update);

                    })

                })

        },
        view_history: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            TMIModel.aggregate([
                {
                    '$lookup': {
                        'from': 'images',
                        'localField': '_id',
                        'foreignField': 'tmiMode._id',
                        'as': 'imagesInfo'
                    }
                }, {
                    '$lookup': {
                        'from': 'tmiProgressModel',
                        'localField': 'progress',
                        'foreignField': '_id',
                        'as': 'progressInfo'
                    }
                }, {
                    '$unwind': {
                        'path': '$progressInfo',
                        'preserveNullAndEmptyArrays': true
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'version': {
                            '$concat': [
                                'Model', ' ', '$version'
                            ]
                        },
                        'chart': 1,
                        'create_date': 1,
                        'count_images': {
                            '$size': '$imagesInfo'
                        },
                        'progressInfo': {
                            '_id': 1,
                            'name_th': 1,
                            'name_en': 1
                        }
                    }
                }, {
                    '$sort': {
                        'create_date': -1
                    }
                }, {
                    '$group': {
                        '_id': {
                            '$dateToString': {
                                'date': '$create_date',
                                'format': '%d/%m/%Y',
                                'timezone': '+07:00'
                            }
                        },
                        'data': {
                            '$push': '$$ROOT'
                        }
                    }
                }
            ])
                .exec(async function (err, modelTrainInfo) {

                    const _set_data = {
                        status: modelTrainInfo.length ? true : false,
                        data: modelTrainInfo.length ? modelTrainInfo : [],
                        msg: ''
                    }

                    res.json(_set_data)
                })

        },
        view_history_by_id: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _modelId = (_get.modelId ? _get.modelId : undefined);

            if (!_modelId) {
                return res.json({
                    status: false,
                    data: {},
                    msg: `ไม่พบข้อมูล id ของคุณ`
                })
            }

            imagesModel.aggregate([
                {
                    '$match': {
                        'tmiMode._id': _modelId
                    }
                }, {
                    '$lookup': {
                        'from': 'account',
                        'localField': 'account_id',
                        'foreignField': '_id',
                        'as': 'accountInfo'
                    }
                }, {
                    '$unwind': {
                        'path': '$accountInfo',
                        'preserveNullAndEmptyArrays': true
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'account_id': 1,
                        'accountInfo': {
                            '_id': '$accountInfo._id',
                            'full_name': '$accountInfo.full_name'
                        },
                        'file_name': 1,
                        'create_date': 1,
                        'tmiModeUse': 1,
                        'tmiMode': 1,
                        'group_rank': 1
                    }
                }, {
                    '$group': {
                        '_id': '$tmiMode.confirm_group._id',
                        'name_th': {
                            '$first': '$tmiMode.confirm_group.name_th'
                        },
                        'name_en': {
                            '$first': '$tmiMode.confirm_group.name_en'
                        },
                        'modelId': {
                            '$first': '$tmiMode._id'
                        },
                        'image_count': {
                            '$sum': 1
                        },
                        'images': {
                            '$push': '$$ROOT'
                        }
                    }
                }, {
                    '$match': {
                        '_id': {
                            '$nin': [
                                null
                            ]
                        }
                    }
                }, {
                    '$sort': {
                        '_id': 1
                    }
                }, {
                    '$facet': {
                        'data': [],
                        'modelInfo': [
                            {
                                '$group': {
                                    '_id': null,
                                    'modelId': { '$first': '$modelId' }
                                }
                            },
                            {
                                '$lookup': {
                                    'from': 'tmiModel',
                                    'localField': 'modelId',
                                    'foreignField': '_id',
                                    'as': 'modelInfo'
                                }
                            },
                            {
                                '$unwind': {
                                    'path': '$modelInfo',
                                    'preserveNullAndEmptyArrays': true
                                }
                            }
                        ]
                    }
                }, {
                    '$project': {
                        '_id': null,
                        'data': 1,
                        'modelInfo': {
                            '$arrayElemAt': [
                                '$modelInfo', 0
                            ]
                        }
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'data': 1,
                        'modelInfo': {
                            '_id': '$modelInfo.modelInfo._id',
                            'class': '$modelInfo.modelInfo.class',
                            'chart': '$modelInfo.modelInfo.chart',
                            'version': '$modelInfo.modelInfo.version',
                        }
                    }
                }
            ])
                .exec(async function (err, imagesInfo) {

                    const _set_data = {
                        status: imagesInfo.length ? true : false,
                        data: imagesInfo.length ? imagesInfo[0]["data"] : [],
                        modelInfo: imagesInfo.length ? imagesInfo[0]["modelInfo"] : [],
                    }

                    res.json(_set_data)

                })
        },
        view_training_await: function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            imagesModel.aggregate([
                {
                    '$match': {
                        'tmiMode._id': ''
                    }
                }, {
                    '$lookup': {
                        'from': 'account',
                        'localField': 'account_id',
                        'foreignField': '_id',
                        'as': 'accountInfo'
                    }
                }, {
                    '$unwind': {
                        'path': '$accountInfo',
                        'preserveNullAndEmptyArrays': true
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'account_id': 1,
                        'accountInfo': {
                            '_id': '$accountInfo._id',
                            'full_name': '$accountInfo.full_name'
                        },
                        'file_name': 1,
                        'create_date': 1,
                        'tmiModeUse': 1,
                        'tmiMode': 1,
                        'group_rank': 1
                    }
                }, {
                    '$group': {
                        '_id': '$tmiMode.confirm_group._id',
                        'name_th': {
                            '$first': '$tmiMode.confirm_group.name_th'
                        },
                        'name_en': {
                            '$first': '$tmiMode.confirm_group.name_en'
                        },
                        'image_count': {
                            '$sum': 1
                        },
                        'images': {
                            '$push': '$$ROOT'
                        }
                    }
                }, {
                    '$match': {
                        '_id': {
                            '$nin': [
                                null
                            ]
                        }
                    }
                }, {
                    '$sort': {
                        '_id': 1
                    }
                }, {
                    '$facet': {
                        'data': [],
                        'total': [
                            {
                                '$group': {
                                    '_id': null,
                                    'total': {
                                        '$sum': '$image_count'
                                    }
                                }
                            }
                        ]
                    }
                }, {
                    '$project': {
                        '_id': null,
                        'data': 1,
                        'total': {
                            '$arrayElemAt': [
                                '$total', 0
                            ]
                        }
                    }
                }, {
                    '$project': {
                        '_id': 1,
                        'data': 1,
                        'total': '$total.total'
                    }
                }
            ])
                .exec(async function (err, imagesInfo) {

                    const _set_data = {
                        status: imagesInfo.length ? true : false,
                        data: imagesInfo.length ? imagesInfo[0]["data"] : [],
                        total: imagesInfo.length ? imagesInfo[0]["total"] : []
                    }

                    res.json(_set_data)

                })

        },
        move_group_by_id: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _imgId = _post.imgId;
            const _groupId = _post.groupId;

            if (!_groupId || !_imgId) {
                return res.json({
                    status: false,
                    msg: 'เกิดข้อผิดพลาด ข้อมูลได้รับไม่ครบ'
                });
            }

            const _keyGroup = (() => {
                return new Promise((resolve) => {
                    keyGroupModel.findOne({ _id: _groupId })
                        .exec(async function (err, keyGroupInfo) {
                            resolve({ status: (keyGroupInfo ? true : false), data: keyGroupInfo })
                        })
                })
            })()

            const _group_res = await _keyGroup;

            if (!_group_res.status) {
                return res.json({
                    status: false,
                    msg: 'เกิดข้อผิดพลาด ไม่พบกลุ่มที่คุณเลือก',
                    tmp: {
                        _keyGroup: _group_res,
                        _post: _post
                    }
                });
            }

            crud.update(imagesModel, {
                'tmiMode.confirm_group': {
                    _id: _group_res.data._id, /** id หมวดหมู่ */
                    name_th: _group_res.data.name_th, /** ชื่อภาษาไทย */
                    name_en: _group_res.data.name_en, /** ชื่อภาษาอังกฤษ */
                    score: 1, /** คะแนน */
                }
            }, _imgId).then((res_update) => {
                res.json({
                    ...res_update,
                    tmp: {
                        _post: _post
                    }
                });
            })




        },
        create_tranin: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _images = (_post.images ? (_post.images.length ? _post.images : []) : []);

            if (!_images.length) {
                return res.json({
                    status: false,
                    msg: 'ไม่พบ image id ทำหรับเทรน'
                })
            }

            const _get_ver = await (() => {
                return new Promise((resolve) => {
                    TMIModel.findOne()
                        .sort({ version: -1 })
                        .exec(function (err, resInfo) {
                            if (err) {
                                return resolve({
                                    status: false,
                                    msg: 'Error getting.',
                                    index: undefined
                                });
                            }

                            const _res = {
                                version: resInfo ? resInfo.version : 0
                            }

                            let index = Number(_res.version) + 1;
                            let version = Number(_res.version) + 1;

                            const input_length = String(version).length;
                            const add_zero = 4 - input_length;
                            let input = version;
                            for (let i = 0; i < add_zero; i++) {
                                input = '0' + input;
                            }

                            const _set_index = {
                                status: true,
                                msg: '',
                                index: index,
                                version: input
                            }

                            resolve(_set_index);
                        })
                })
            })()

            if (!_get_ver.status) {
                return res.json(_get_ver);
            }

            const _model_id = `${crud.uuid(4, 2)}-${crud.uuid(2, 2)}-${_get_ver.index}`;
            const _set_data = {
                _id: _model_id,/** id เทรน และ ชื่อโฟลเดอร์ */
                version: _get_ver.version, /** version model : 00 = default */
                path: {
                    zip_file_train: `${crud.uuid(7, 2)}-${_get_ver.version}.zip`,/** ที่อยู่ไฟล์ zip สำหรับเทรน */
                    zip_file_build: `${_model_id}-${_get_ver.version}.zip`,/** ที่อยู่ไฟล์ zip สำหรับเทรนเสร็จแล้ว */
                },
                chart: {
                    img_acc: "accuracyPerEpoch.png", /** ที่อยู่ไฟล์กราฟ acc image */
                    img_lost: "lossPerEpoch.png", /** ที่อยู่ไฟล์กราฟ lost image */
                },
                progress: "01",/** สถานะ */
                public: false, /** เปิดให้ใช้งานสำหรับประเมินภาพ */
                isActive: true, /** เปิด/ปิด */
                create_date: crud.getTimeZone(),
                update_date: crud.getTimeZone(),
            }

            crud.create(TMIModel, _set_data, { _set_data: _set_data }).then((res_create) => {
                if (!res_create.status) {
                    return res.json(res_create);
                }

                crud.update(imagesModel, { 'tmiMode._id': _model_id, 'tmiMode.version': _set_data.version }, { $in: _images }, { _images: _images, _set_data: _set_data }).then((res_update) => {

                    if (res_update.status) {

                        /**
                         * Add to Queue Service
                         */
                        rabbitmq.main_train(_set_data._id);
                    }

                    res.json({ ...res_update, tmp: { _images: _images, _set_data: _set_data } });
                })

            })

        },
        update_model_tranin: async function (req, res) {
            var _get = req.params;
            var _post = req.body;

            /**
             * tmp ไฟล์ zip ที่ได้รับมาเพื่อรออัพโหลดเก็บจนเสร็จแล้วเช็คว่าไฟล์นี้เป็นของ model ตัวไหนถ้าหากไม่เจอให้ลบทันที
             */
            const _path_dir_tmp = `${path_model}/tmp`;

            await crud.create_dir(_path_dir_tmp)

            const _remove_file_for_error = ((files = []) => {
                for (var i = 0; i < files.length; i++) {
                    const _files = files[i];
                    const _full_path = _files.path;
                    fs.unlink(_full_path, (err) => {
                        if (err) {
                            console.error('Error deleting file:', err);
                            return;
                        }

                        console.log('File deleted successfully ', _full_path);
                    });
                }
            })

            const storage = multer.diskStorage({
                destination: (req, file, cb) => {
                    cb(null, _path_dir_tmp)
                },
                filename: (req, file, cb) => {
                    cb(null, file.originalname)
                },
            })

            var upload = multer({
                storage: storage,
                // limits: {
                //   fileSize: 1024 * 1024 * 5
                // },
                fileFilter: (req, file, cb) => {
                    if (
                        file.mimetype == 'application/zip'
                    ) {
                        cb(null, true)
                    } else {
                        cb(null, false)
                        return cb('Only .zip format allowed!')
                    }
                },
            }).single('model_zip');

            upload(req, res, async function (err) {

                if (err) {
                    return res.json({
                        status: false,
                        msg: err,
                    })
                }
                const _c_fileName = String(req.file.filename);
                TMIModel.findOne({ "path.zip_file_build": _c_fileName })
                    .exec(async function (err, modeleInfo) {
                        if (err || !modeleInfo) {
                            _remove_file_for_error([req.file]);
                            socketCtl.room(_room_admin, 'process', {
                                admin: {
                                    log: {
                                        time: crud.getTimeZone(),
                                        // msg: `Copy File Group : ${_config.name} , images ${_config.files.length} ok!`
                                        msg: `ทำการลบไฟล์ ${req.file.filename} เนื่องจากไม่พบข้อมูล หรือ เกิดข้อผิดพลาด ${err}`
                                    }
                                }
                            })
                            return res.json({
                                status: false,
                                msg: 'Not found [0]',
                            })
                        }

                        crud.update(TMIModel, { progress: "04", update_date: crud.getTimeZone() }, modeleInfo._id, { file: req.file, modeleInfo: modeleInfo }).then(async (res_upload) => {


                            /**
                             * ลบไฟล์ออกเมื่ออัพเดทไม่สำเร็จ
                             */
                            if (!res_upload.status) {
                                _remove_file_for_error([req.file]);
                                socketCtl.room(_room_admin, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            // msg: `Copy File Group : ${_config.name} , images ${_config.files.length} ok!`
                                            msg: `ทำการลบไฟล์ ${req.file.filename} เนื่องจากอัพเดทไม่สำเร็จ :: ${res_upload.msg}`
                                        }
                                    }
                                })
                            }


                            /**
                             * ย้ายไฟล์ไปเก็บ
                             */
                            await crud.create_dir(`${path_model}/${modeleInfo._id}`);
                            await crud.copyFiles([req.file.filename], _path_dir_tmp, `${path_model}/${modeleInfo._id}`)

                            if (res_upload.status) {
                                socketCtl.room(_room_admin, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            // msg: `Copy File Group : ${_config.name} , images ${_config.files.length} ok!`
                                            msg: `Copy File ${req.file.filename} สำเร็จ , jobsId [${modeleInfo._id}]`
                                        }
                                    }
                                });

                                _remove_file_for_error([req.file]);
                                crud.upzip_file(`${path_model}/${modeleInfo._id}/${req.file.filename}`, `${path_model}/${modeleInfo._id}`).then((res_unzip) => {
                                    console.log(res_unzip);
                                })
                            }

                            res.json({ ...res_upload, filename: req.file.filename })
                        })

                    })


            })

        },
        update_used_model: async function (req, res) {
            var token = req.user;
            var _get = req.params;
            var _post = req.body;

            const _check_permission = crud.permission_role(["ADMIN"], token)

            if (!_check_permission.status) {
                return res.json(_check_permission);
            }

            const _modelId = (_post.modelId ? _post.modelId : undefined);

            if (!_modelId) {
                return res.json({
                    status: false,
                    msg: 'ไม่พบ โมเดลที่คุณเลือก'
                })
            }


            const _get_mode_public = await (() => {
                return new Promise((resolve) => {
                    TMIModel.aggregate([
                        {
                            '$match': {
                                'public': true
                            }
                        }, {
                            '$group': {
                                '_id': null,
                                'model_id': {
                                    '$push': '$_id'
                                }
                            }
                        }
                    ])
                        .exec(async function (err, packageInfo) {

                            const _set_data = {
                                status: (err ? false : true),
                                data: packageInfo,
                                msg: (err ? 'พบข้อผิดพลาด [1]' : '')
                            }

                            resolve(_set_data)

                        })
                })
            })()

            if (!_get_mode_public.status) {
                return res.json(_get_mode_public)
            }

            if (_get_mode_public.data.length) {
                await crud.update(TMIModel, { public: false, update_date: crud.getTimeZone() }, { $in: _get_mode_public.data[0]["model_id"] }, { _get_mode_public: _get_mode_public, _post: _post, _modelId: _modelId }).then((res_update_main) => {
                    if (!res_update_main.status) {
                        return res.json(res_update_main)
                    }
                })
            }

            if (_modelId === "default") {
                return res.json({
                    status: true,
                    msg: 'ปรับเป็นโมเดลเริ่มต้้นเรียบร้อยแล้ว'
                })
            }

            crud.update(TMIModel, { public: true, update_date: crud.getTimeZone() }, _modelId, { _get_mode_public: _get_mode_public, _post: _post, _modelId: _modelId }).then((res_update) => {
                res.json(res_update);
            })


        },
    }
};