module.exports = function (io) {
    /**
     * Function
     */
    const crud = require('../functions/crud');

    /**
     * Model
     */
    const accountModel = require("../models/accountModel");
    const apiKeyModel = require('../models/apiKeyModel');

    require('dotenv').config()
    const env = process.env;
    return {
        login: function (data_login = undefined, socket) {
            return new Promise((resolve) => {
                if (!data_login) {
                    console.log("Data Login : null.");
                    resolve(false)
                    return
                }

                const jwt = require('jsonwebtoken');
                const _token = (data_login["Authorization"] ? data_login["Authorization"].split(" ")[1] : "");
                /**
                 * Check Token
                 */
                if (!_token) {
                    console.log("Token : null.");
                    resolve(false)
                    return
                }
                jwt.verify(_token, env.ACCESS_TOKEN_SECRET, (error, playload) => {

                    if (error) {
                        console.log("Token : Error.", error);
                        resolve(false)
                        return
                    }

                    if (!playload) {
                        console.log("playload token : null.");
                        resolve(false)
                        return
                    }

                    accountModel.findOne({
                        _id: playload._id,
                        role: playload.role,
                        isActive: true
                    })
                        .exec(async function (err, userInfo) {
                            if (err) {
                                console.log("userInfo : ระบบเกิดข้อผิดพลาด.");
                                resolve(false)
                                return
                            }

                            if (!userInfo) {
                                console.log("userInfo : ไม่พบผู้ใช้งาน.");
                                resolve(false)
                                return
                            }

                            const _set_data = {
                                _id: userInfo._id,
                                full_name: userInfo.full_name,
                                role: userInfo.role,
                                socket_id: socket.id,
                                platform: data_login.platform,
                                version: data_login.version,
                                user_agent: socket.handshake.headers["user-agent"]
                            }

                            crud.account_status_online(_set_data._id, 'user', true)
                            // console.log("_set_data", _set_data);
                            resolve(_set_data);

                        })


                    // console.log(error, playload);
                })

            })
        },
        login_bot: function (data_login = undefined, socket) {
            return new Promise((resolve) => {
                if (!data_login) {
                    console.log("Data Login : null.");
                    resolve(false)
                    return
                }

                const jwt = require('jsonwebtoken');
                const _token = (data_login["Authorization"] ? data_login["Authorization"] : "");
                /**
                 * Check Token
                 */
                if (!_token) {
                    console.log("Token : null.");
                    resolve(false)
                    return
                }

                apiKeyModel.findOne({
                    _id: _token,
                    platform: data_login.platform,
                    isActive: true
                })
                    .exec(async function (err, userInfo) {
                        if (err) {
                            console.log("userInfo : ระบบเกิดข้อผิดพลาด.");
                            resolve(false)
                            return
                        }

                        if (!userInfo) {
                            console.log("userInfo : ไม่พบ API KEY.");
                            resolve(false)
                            return
                        }

                        const _set_data = {
                            _id: userInfo._id,
                            full_name: userInfo.name,
                            role: 'SERVICE',
                            socket_id: socket.id,
                            platform: data_login.platform,
                            version: data_login.version,
                            user_agent: socket.handshake.headers["user-agent"]
                        }

                        crud.account_status_online(_set_data._id, 'bot', true)
                        // console.log("_set_data", _set_data);
                        resolve(_set_data);

                    })


                // console.log(error, playload); 

            })
        },
        room: function (room_name = undefined, target = undefined, data = undefined) {
            // console.log(room_name, target, data);
            if (room_name && target && data) {
                io.sockets.in(room_name).emit(target, data)
            }
        }
    }
};