const accountModel = require('../models/accountModel');

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

require('dotenv').config()
const env = process.env;

module.exports = function (passport) {
    var opts = {}
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("JWT");
    opts.secretOrKey = env.ACCESS_TOKEN_SECRET;

    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        // console.log("session", passport);
        // console.log("----------------------- #jwt_payload ------------------")
        // console.log(jwt_payload)
        // console.log("C->", jwt_payload.type, jwt_payload._id)
        // console.log("------------------- end#jwt_payload -------------------")
        if (!jwt_payload) {
            return done(null, false);
        }

        accountModel.findOne({ _id: jwt_payload._id, isActive: true })
            .exec(async function (err, userInfo) {
                if (err || !userInfo) {
                    return done(null, false);
                }

                return done(null, {
                    _id: userInfo._id,
                    full_name: userInfo.full_name,
                    total_used_storage: userInfo.total_used_storage,
                    package_default_id: userInfo.package_default_id,
                    role: userInfo.role
                });

            })


    }))

}