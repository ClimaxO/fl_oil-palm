module.exports = function () {
    /**
     * System
     */
    const axios = require('axios');
    const cheerio = require('cheerio');

    require('dotenv').config()
    const env = process.env;

    return {
        init: function () {
            return new Promise((resolve) => {
                const url = env.API_PALM;

                axios.get(url, { timeout: 10000 })
                    .then(response => {
                        const html = response.data;
                        const $ = cheerio.load(html);

                        const _date_price = $('#ctl00_ContentDetail_div_data > div.col-md-12:nth-child(1) > .col-md-12').text();
                        const _price_list = $('#ctl00_ContentDetail_div_data > div.col-md-12:nth-child(1) > div.custom-price').map((_, el) => {
                            return {
                                title: String($('p > a', el).text()),
                                price: Number(String($('.price', el).text()).split(" ")[0]),
                                unit: String($('.price', el).text()).split(" ")[1],
                            }
                        }).get();

                        const _set_data = {
                            title: _date_price,
                            prices: _price_list,
                            price_avg: {
                                title: (_price_list.length ? _price_list[0].title : ""),
                                price: (_price_list.length ? _price_list[0].price : 0),
                                unit: (_price_list.length ? _price_list[0].unit : ""),
                            }
                        }
                        resolve({
                            status: true,
                            msg: '',
                            data: _set_data
                        })
                    })
                    .catch(error => {
                        console.error('Error:', error);
                        resolve({
                            status: false,
                            msg: ["ETIMEDOUT", "ECONNABORTED"].includes(error.code) ? `ไม่สามารถเชื่อมต่อฐานข้อมูลราคากลางผลปาล์มได้ โปรดตรวจสอบเว็บต้นทางและลองใหม่อีกครั้ง` : `เกิดข้อผิดพลาด รหัส [${error.code}]`,
                            data: {}
                        })
                    });
            })
        }
    }
}