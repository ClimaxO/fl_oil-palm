module.exports = function (io) {
    /**
     * functions
     */
    const crud = require('../functions/crud');
    const TeachableMachine = require("../functions/tf-model");
    const _console = require('../functions/console');
    const socketCtl = require('../functions/socketCtl')(io);

    /**
     * System
     */
    const amqplib = require('amqplib');
    const path = require('path');
    const fs = require('fs');

    /**
     * models
     */
    const imagesModel = require('../models/imagesModel');
    const keyGroupModel = require('../models/keyGroupModel');
    const jobsModel = require('../models/jobsModel');
    const TMIModel = require('../models/TMIModel');

    require('dotenv').config()
    const env = process.env;
    let connection = null;
    let channel = null;
    const queue_name = "Image eval."
    const queue_name_train = "Train Model."

    return {
        connect_to_server: async function () {
            connection = await amqplib.connect(env.API_RABBITMQ);
            channel = await connection.createChannel();
        },
        receive: function () {
            try {
                channel.assertQueue(queue_name, {
                    durable: false
                });

                console.log("Rabbitmq Jobs : ", _console.color().FgGreen(queue_name));
                channel.consume(queue_name, async function (msg) {
                    // console.log(" [x] Received %s", msg.content.toString());
                    const _res = JSON.parse(msg.content.toString());
                    console.log("Rabbitmq New Job :: ", _console.color().FgGreen(_res.jobs_id));

                    let _list_group = {};

                    const _keyGroup = (() => {
                        return new Promise((resolve) => {
                            keyGroupModel.find({ jobs_id: _res.jobs_id })
                                .exec(async function (err, keyGroupInfo) {
                                    for (let index = 0; index < keyGroupInfo.length; index++) {
                                        const _keyGroupInfo = keyGroupInfo[index];
                                        _list_group[_keyGroupInfo.keyMatch] = _keyGroupInfo;
                                    }
                                    resolve(keyGroupInfo)
                                })
                        })
                    })()

                    const _get_tmiModel = await (() => {
                        return new Promise((resolve) => {
                            TMIModel.findOne({ isActive: true, public: true }, ["_id", "version"])
                                .sort({ create_date: -1 })
                                .exec(async function (err, tmiInfo) {
                                    const _set_data = {
                                        _id: (tmiInfo ? tmiInfo._id : '-1'),
                                        version: (tmiInfo ? tmiInfo.version : 'default'),
                                        path: (tmiInfo ? `${env.PATH_MODEL}/${tmiInfo._id}` : `${env.PATH_MODEL}/${env.PATH_MODEL_USE_DEFAULT}`)
                                    }
                                    resolve(_set_data);
                                })
                        })
                    })()

                    imagesModel.find({ jobs_id: _res.jobs_id })
                        .exec(async function (err, imagesInfo) {
                            if (err) {
                                return res.json({
                                    status: false,
                                    data: {},
                                    msg: "ระบบเกิดข้อผิดพลาด"
                                })
                            }
                            if (imagesInfo.length) {

                                console.log(`Model Use : ${_get_tmiModel.version} , ${_get_tmiModel._id}`);

                                socketCtl.room(`admin:log`, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            msg: `กำลังประมวลผลภาพ JobsId : ${_res.jobs_id} , model version : ${_get_tmiModel.version} , Code name :  ${_get_tmiModel._id} , images [${imagesInfo.length}] files.`,
                                            jobsId: msg.jobsId
                                        }
                                    }
                                })


                                /**
                                 * Send socket to client for queue
                                 * ถ้ามี reload จะไปหน้านั้นทันที
                                 * - reload + alert จะถามว่าไปหรือไม่
                                 * - reload + toast จะไปหน้านั้นทันที และ แสดงผล
                                 */
                                const _room_name = `client:${imagesInfo[0].account_id}`;
                                socketCtl.room(_room_name, 'process', {
                                    reload: {
                                        url: `/dashboard/view/${_res.jobs_id}`
                                    },
                                    display: {
                                        alert: {
                                            title: `ถึงคิวประมวลผลคุณแล้ว`,
                                            msg: `กดไปยังหน้า เพื่อเข้าสู่ Job : ${_res.jobs_id} และ โปรดรอเตรียมข้อมูลสักครู่`,
                                            btnOk: `ไปยังหน้า`
                                        },
                                    },
                                })

                                let isSuccess = false;
                                const model = new TeachableMachine({
                                    modelUrl: path.resolve(`../storage/${_get_tmiModel.path}`)
                                });
                                return new Promise(async (resolve) => {

                                    socketCtl.room(_room_name, 'process', {
                                        display: {
                                            toast: {
                                                title: `เริ่มประมวลผลภาพทั้งหมด : ${imagesInfo.length} ภาพ`
                                            }
                                        }
                                    })

                                    for (let index = 0; index < imagesInfo.length; index++) {
                                        const _image = imagesInfo[index];
                                        const imgPath = `../storage/user/${_image.file_name}`;
                                        const imgData = fs.readFileSync(imgPath);
                                        const base64Img = imgData.toString('base64');
                                        await new Promise((resolve_classify) => {
                                            model.classify({
                                                imageUrl: `data:image/jpeg;base64,${base64Img}`,
                                            }).then(async (predictions) => {

                                                let _set_data = {
                                                    top_best: {
                                                        _id: _list_group[predictions[0].class]._id, /** id หมวดหมู่ */
                                                        name_th: _list_group[predictions[0].class].name_th, /** ชื่อภาษาไทย */
                                                        name_en: _list_group[predictions[0].class].name_en, /** ชื่อภาษาอังกฤษ */
                                                        score: predictions[0].score, /** คะแนน */
                                                    },
                                                    group_list: []
                                                }

                                                for (let i = 0; i < predictions.length; i++) {
                                                    const _predict = predictions[i];
                                                    _set_data.group_list.push({
                                                        _id: _list_group[_predict.class]._id, /** id หมวดหมู่ */
                                                        name_th: _list_group[_predict.class].name_th, /** ชื่อภาษาไทย */
                                                        name_en: _list_group[_predict.class].name_en, /** ชื่อภาษาอังกฤษ */
                                                        score: _predict.score, /** คะแนน */
                                                    })
                                                }
                                                await crud.update(imagesModel, {
                                                    group_rank: _set_data,
                                                    tmiModeUse: { _id: _get_tmiModel._id, version: _get_tmiModel.version },
                                                    'tmiMode.confirm_group': {
                                                        _id: _list_group[predictions[0].class]._id, /** id หมวดหมู่ */
                                                        name_th: _list_group[predictions[0].class].name_th, /** ชื่อภาษาไทย */
                                                        name_en: _list_group[predictions[0].class].name_en, /** ชื่อภาษาอังกฤษ */
                                                        score: predictions[0].score, /** คะแนน */
                                                    }, update_date: crud.getTimeZone()
                                                }, _image._id).then((res_update) => {
                                                    isSuccess = true;
                                                    console.log("Image Id : ", _image._id);
                                                    socketCtl.room(_room_name, 'process', {
                                                        display: {
                                                            logs: {
                                                                title: `ภาพที่ ${(index + 1)}/${imagesInfo.length} สำเร็จแล้ว , คืบหน้าไปแล้ว ${(((index + 1) / imagesInfo.length) * 100).toFixed(2)}%`
                                                            }
                                                        }
                                                    })
                                                    resolve_classify();
                                                })
                                                // console.log("_set_data", _set_data);
                                                // console.log("Predictions:", `${index + 1} ${predictions[0].class} :: ${(predictions[0].score * 100).toFixed(2)}% -> ${_image.file_name}`);
                                                // console.table(predictions)
                                            }).catch((e) => {
                                                console.log(`ERROR [${index + 1}]`, _image.file_name, e);
                                            });
                                        });

                                    }

                                    socketCtl.room(_room_name, 'process', {
                                        display: {
                                            logs: {
                                                title: `ดำเนินการเสร็จสิ้นแล้ว กำลังทำรายงานสรุปผลทั้งหมด`
                                            }
                                        }
                                    })

                                    resolve();

                                }).then(() => {
                                    if (isSuccess) {

                                        imagesModel.aggregate([
                                            {
                                                '$match': {
                                                    'jobs_id': _res.jobs_id
                                                }
                                            }, {
                                                '$group': {
                                                    '_id': '$group_rank.top_best._id',
                                                    'name_th': {
                                                        '$first': '$group_rank.top_best.name_th'
                                                    },
                                                    'name_en': {
                                                        '$first': '$group_rank.top_best.name_en'
                                                    },
                                                    'sum': {
                                                        '$sum': 1
                                                    }
                                                }
                                            }, {
                                                '$sort': {
                                                    'sum': -1
                                                }
                                            }, {
                                                '$facet': {
                                                    'data': [],
                                                    'total': [
                                                        {
                                                            '$group': {
                                                                '_id': null,
                                                                'total': {
                                                                    '$sum': '$sum'
                                                                }
                                                            }
                                                        }
                                                    ]
                                                }
                                            }
                                        ])
                                            .exec(async function (err, imagesSummary) {
                                                // console.log("imagesSummary", imagesSummary);
                                                if (imagesSummary.length) {

                                                    const _data_group = imagesSummary[0]["data"];
                                                    const _data_total = imagesSummary[0]["total"].length ? imagesSummary[0]["total"][0] : { _id: null, total: 5 };
                                                    // console.log("_data_group", _data_group);
                                                    // console.log("_data_total", _data_total);

                                                    let _set_data_jobs = {
                                                        top_best: {
                                                            _id: "", /** id หมวดหมู่ */
                                                            name_th: "", /** ชื่อภาษาไทย */
                                                            name_en: "", /** ชื่อภาษาอังกฤษ */
                                                            score: 0, /** คะแนน */
                                                            weight: 0, /** น้ำหนัก */
                                                            price: 0, /** ราคา */
                                                        },
                                                        group_list: []
                                                    }

                                                    for (let index = 0; index < _data_group.length; index++) {
                                                        const _group = _data_group[index];
                                                        const _percen = (_group.sum / _data_total.total) * 100;

                                                        if (_set_data_jobs.top_best.score < _percen) {
                                                            _set_data_jobs.top_best.score = _percen
                                                            _set_data_jobs.top_best = {
                                                                _id: _group._id, /** id หมวดหมู่ */
                                                                name_th: _group.name_th, /** ชื่อภาษาไทย */
                                                                name_en: _group.name_en, /** ชื่อภาษาอังกฤษ */
                                                                score: _percen, /** คะแนน */
                                                                weight: 0, /** น้ำหนัก */
                                                                price: 0, /** ราคา */
                                                            }
                                                        }
                                                        _set_data_jobs.group_list.push({
                                                            _id: _group._id, /** id หมวดหมู่ */
                                                            name_th: _group.name_th, /** ชื่อภาษาไทย */
                                                            name_en: _group.name_en, /** ชื่อภาษาอังกฤษ */
                                                            score: _percen, /** คะแนน */
                                                        })
                                                        // console.log(`${_group.name_th} :: ${(_group.sum / _data_total.total) * 100}`);
                                                        // console.table(_set_data_jobs.group_list);
                                                    }

                                                    crud.update(jobsModel, { tmiModeUse: { _id: _get_tmiModel._id, version: _get_tmiModel.version }, progress: "03", group_rank: _set_data_jobs, success_date: crud.getTimeZone(), update_date: crud.getTimeZone() }, _res.jobs_id).then((res_jobs) => {
                                                        console.log("Update Jobs Id : ", res_jobs.status);
                                                        socketCtl.room(_room_name, 'process', {
                                                            reload: {
                                                                url: `/dashboard/view/${_res.jobs_id}`
                                                            },
                                                            display: {
                                                                logs: {
                                                                    title: `ดำเนินการทั้งหมดเสร็จสิ้นแล้ว Job : ${_res.jobs_id} , Model : ${_get_tmiModel.version}`
                                                                }
                                                            }
                                                        })
                                                        // send socket
                                                    })

                                                } else {
                                                    // socket error report
                                                }
                                            })

                                    } else {
                                        // socket send error
                                    }
                                })



                            }

                        })

                }, {
                    noAck: true
                });

            } catch (error) {
                console.error('Error connecting to RabbitMQ:', error);
            }
        },
        main: async function (jobs_id = undefined) {
            if (!jobs_id) {
                console.log("Jobs Id null");
                return
            }
            try {
                // console.log("channel", channel);
                // Use the channel to perform operations with RabbitMQ
                connection = await amqplib.connect('amqp://admin:Patch24_r@159.65.131.143/');
                channel = await connection.createChannel();
                const _data = { jobs_id: jobs_id };
                const queue = await channel.assertQueue(queue_name, {
                    durable: false
                });
                channel.sendToQueue(queue_name, Buffer.from(JSON.stringify(_data)), {
                    replyTo: queue.queue,
                });

                crud.update(jobsModel, { progress_date: crud.getTimeZone(), update: crud.getTimeZone() }, jobs_id, { jobs_id: jobs_id });

                console.log(" [x] Sent %s", _data);

            } catch (error) {
                console.error('Error connecting to RabbitMQ:', error);
            }

        },
        main_train: async function (model_id = undefined) {
            if (!model_id) {
                console.log("Model Id [TMIModel] null");
                return
            }
            try {
                // console.log("channel", channel);
                // Use the channel to perform operations with RabbitMQ
                connection = await amqplib.connect('amqp://admin:Patch24_r@159.65.131.143/');
                channel = await connection.createChannel();
                const _data = { model_id: model_id };
                const queue = await channel.assertQueue(queue_name_train, {
                    durable: false
                });
                channel.sendToQueue(queue_name_train, Buffer.from(JSON.stringify(_data)), {
                    replyTo: queue.queue,
                });
                console.log(" [x] Sent %s", _data);

            } catch (error) {
                console.error('Error connecting to RabbitMQ:', error);
            }

        },
        receive_train: function () {
            try {
                channel.assertQueue(queue_name_train, {
                    durable: false
                });

                console.log("Rabbitmq Jobs Model Train : ", _console.color().FgGreen(queue_name_train));
                channel.consume(queue_name_train, async function (msg) {
                    // console.log(" [x] Received %s", msg.content.toString());
                    const _res = JSON.parse(msg.content.toString());
                    console.log("Rabbitmq New Job Model Train :: ", _console.color().FgGreen(_res.model_id));

                    TMIModel.aggregate([
                        {
                            '$match': {
                                '_id': _res.model_id
                            }
                        }, {
                            '$project': {
                                '_id': 1,
                                'path': {
                                    'download': '$path.zip_file_train',
                                    'save_file_name': '$path.zip_file_build'
                                },
                                'chart': 1
                            }
                        }, {
                            '$lookup': {
                                'from': 'images',
                                'localField': '_id',
                                'foreignField': 'tmiMode._id',
                                'as': 'data'
                            }
                        }, {
                            '$unwind': {
                                'path': '$data',
                                'preserveNullAndEmptyArrays': true
                            }
                        }, {
                            '$group': {
                                '_id': '$data.tmiMode.confirm_group._id',
                                'name': {
                                    '$first': '$data.tmiMode.confirm_group.name_en'
                                },
                                'dir': {
                                    '$first': {
                                        '$replaceAll': {
                                            'input': {
                                                '$toLower': '$data.tmiMode.confirm_group.name_en'
                                            },
                                            'find': ' ',
                                            'replacement': '-'
                                        }
                                    }
                                },
                                'files': {
                                    '$push': '$data.file_name'
                                },
                                'jobsId': {
                                    '$first': '$_id'
                                },
                                'path': {
                                    '$first': '$path'
                                },
                                'chart': {
                                    '$first': '$chart'
                                },
                                'score': {
                                    '$push': '$data.tmiMode.confirm_group.score'
                                },
                                'actual': {
                                    '$push': 1
                                }
                            }
                        }, {
                            '$sort': {
                                '_id': 1
                            }
                        }, {
                            '$facet': {
                                'data': [
                                    {
                                        '$group': {
                                            '_id': '$jobsId',
                                            'path': {
                                                '$first': '$path'
                                            },
                                            'chart': {
                                                '$first': '$chart'
                                            },
                                            'data': {
                                                '$push': {
                                                    '_id': '$_id',
                                                    'name': '$name',
                                                    'dir': '$dir',
                                                    'files': '$files'
                                                }
                                            }
                                        }
                                    }
                                ],
                                'score': [
                                    {
                                        '$unwind': {
                                            'path': '$score',
                                            'preserveNullAndEmptyArrays': true
                                        }
                                    }, {
                                        '$group': {
                                            '_id': null,
                                            'score_point': {
                                                '$push': '$score'
                                            }
                                        }
                                    }
                                ]
                            }
                        }, {
                            '$project': {
                                '_id': {
                                    '$arrayElemAt': [
                                        '$data._id', 0
                                    ]
                                },
                                'path': {
                                    '$arrayElemAt': [
                                        '$data.path', 0
                                    ]
                                },
                                'chart': {
                                    '$arrayElemAt': [
                                        '$data.chart', 0
                                    ]
                                },
                                'data': {
                                    '$arrayElemAt': [
                                        '$data.data', 0
                                    ]
                                },
                                'score': {
                                    '$arrayElemAt': [
                                        '$score.score_point', 0
                                    ]
                                }
                            }
                        }
                    ])
                        .exec(async function (err, configZipInfo) {

                            const _room_name = `admin:log`;

                            if (!configZipInfo.length) {

                                socketCtl.room(_room_name, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            msg: `Error -> Train model Id : ${_res.model_id} Data null !!`
                                        }
                                    }
                                })

                                return
                            }


                            const _file_zip_path_main = path.resolve(`../storage/files-zip`);
                            const _file_zip_path = (`${_file_zip_path_main}/${_res.model_id}`);
                            const _main_config = configZipInfo[0];

                            const _predictions = _main_config.score;
                            let _rmse = 0;
                            let _mae = 0;

                            for (let index = 0; index < _predictions.length; index++) {
                                const _predic = _predictions[index];
                                const _actual = 1;
                                let __rmse = (_predic - _actual);
                                let __rmse_up2 = Math.pow(__rmse, 2);
                                _rmse += __rmse_up2;
                                _mae += (0 - __rmse);
                            }

                            crud.update(TMIModel, { "class.rmse": (_rmse / _predictions.length), "class.mae": (_mae / _predictions.length), update_date: crud.getTimeZone() }, _res.model_id, { _rmse: _rmse, _mae: _mae })
                            // console.log("_main_config.score", _main_config.score, _rmse, _rmse / _predictions.length, _mae, _mae / _predictions.length); 
                            /**
                             * เช็คที่อยู่หลักก่อนแล้วถ้าหากเจอให้ลบและสร้างใหม่ กรณีต้องการรีการสร้างใหม่
                             */
                            await crud.re_create_dir(_file_zip_path);
                            socketCtl.room(_room_name, 'process', {
                                admin: {
                                    log: {
                                        time: crud.getTimeZone(),
                                        msg: `เริ่มเตรียมไฟล์สำหรับเทรน , ${_res.model_id}`
                                    }
                                }
                            })
                            console.log(`เริ่มเตรียมไฟล์สำหรับเทรน , ${_res.model_id}`);

                            /**
                             * สร้างไฟล์ json สำหรับบอทอ่านข้อมูลจากไฟล์
                             */
                            const json_data_create = JSON.stringify(_main_config, null, 2);
                            fs.writeFile(`${_file_zip_path}/config.json`, json_data_create, (error) => {
                                if (error) {
                                    socketCtl.room(_room_name, 'process', {
                                        admin: {
                                            log: {
                                                time: crud.getTimeZone(),
                                                msg: `Create File : config.json error -> ${JSON.stringify(error)}!`
                                            }
                                        }
                                    })
                                    console.log(`Create File : config.json error -> ${JSON.stringify(error)}!`);
                                } else {
                                    socketCtl.room(_room_name, 'process', {
                                        admin: {
                                            log: {
                                                time: crud.getTimeZone(),
                                                msg: `Create File : config.json ok!`
                                            }
                                        }
                                    })
                                    console.log(`Create File : config.json ok!`);
                                }
                            });

                            for (let index = 0; index < _main_config.data.length; index++) {
                                const _config = _main_config.data[index];
                                const _file_img_path = path.resolve(`../storage/user`);

                                /**
                                 * สร้าง dir สำหรับแต่ละหมวดหมู่
                                 */
                                await crud.re_create_dir(`${_file_zip_path}/${_config.dir}`);
                                socketCtl.room(_room_name, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            msg: `Create Group Dir : ${_config.dir} ok!`
                                        }
                                    }
                                })
                                console.log(`Create Group Dir : ${_config.dir} ok!`);
                                /**
                                 * เพิ่มไฟล์เข้าหมวดหมู่
                                 */
                                await crud.copyFiles(_config.files, _file_img_path, `${_file_zip_path}/${_config.dir}`);
                                socketCtl.room(_room_name, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            msg: `Copy File Group : ${_config.name} , images ${_config.files.length} ok!`
                                        }
                                    }
                                })
                                console.log(`Copy File Group : ${_config.name} , images ${_config.files.length} ok!`);
                            }

                            /**
                             * Zip ไฟล์แล้วบันทึกลงที่ root ของ model file zip
                             */
                            const _path_file_zip = `${_file_zip_path_main}/${_main_config.path.download}`;

                            /**
                             * Bug zip files!!
                             */
                            await crud.zip_file(_file_zip_path, _path_file_zip).then(async (z_res) => {
                                socketCtl.room(_room_name, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            msg: z_res
                                        }
                                    }
                                })
                                console.log(z_res);

                                /**
                                 * ลบทั้งหมดและเก็บไฟล์ zip ไว้
                                 */
                                await crud.remove_dir(_file_zip_path);
                            }).catch((err) => {
                                socketCtl.room(_room_name, 'process', {
                                    admin: {
                                        log: {
                                            time: crud.getTimeZone(),
                                            msg: err
                                        }
                                    }
                                })
                                console.log(err);
                            })

                            socketCtl.room(_room_name, 'process', {
                                admin: {
                                    log: {
                                        time: crud.getTimeZone(),
                                        msg: `เตรียมไฟล์สำหรับเทรนเสร็จสิ้น , กำลังจัดส่งข้อมูลให้บอททำการเทรนโมเดลใหม่`
                                    }
                                }
                            })

                            /**
                             * ส่งข้อมูลหาบอทให้โหลดไฟล์
                             */
                            socketCtl.room('bot:train', 'process', {
                                bot: {
                                    msg: 'เริ่มทำการโหลดข้อมูล',
                                    jobsId: _res.model_id,
                                    file_download: `/admin/files/train/${_main_config.path.download}`
                                }
                            })

                        })


                }, {
                    noAck: true
                });

            } catch (error) {
                console.error('Error connecting to RabbitMQ:', error);
            }
        },
    }
};