/**
 * Model
 */
const accountModel = require('../models/accountModel');
const apiKeyModel = require('../models/apiKeyModel');
const palmPriceModel = require('../models/palmPriceModel');

/**
 * System
 */
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { nanoid, customAlphabet } = require('nanoid');
const os = require('os');
const fs = require('fs');
const path = require('path');
const request = require('request');
const archiver = require('archiver');
const unzipper = require('unzipper');
const df = require('node-df');

require('dotenv').config()
const env = process.env;

module.exports = {
    get_index: function (model, data = {}) {
        return new Promise((resolve) => {
            model.findOne(data)
                .sort({ index: -1 })
                .exec(function (err, resInfo) {
                    if (err) {
                        return resolve({
                            status: false,
                            msg: 'Error getting.',
                            index: undefined
                        });
                    }
                    return resolve({
                        status: true,
                        msg: (resInfo ? '' : 'ไม่พบข้อมูล'),
                        index: (resInfo ? Number(resInfo.index) + 1 : 1)
                    });
                })
        })
    },
    uuid: function (generateMaxNum = 21, mode = 1) {
        if (mode === 1) {
            return nanoid(generateMaxNum)
        }

        return customAlphabet('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 10)(generateMaxNum)
    },
    comparePassword: function (candidatePassword, hash) {
        return new Promise((resolve) => {
            bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
                if (err) return resolve(false);
                return resolve(isMatch);
            });
        })
    },
    hashPassword: function (password) {
        return new Promise((resolve) => {
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(password, salt, function (err, hash) {
                    if (err) return resolve(undefined);
                    return resolve(hash);
                });
            });
        })
    },
    setToken: function (data, secretType = "accessToken", typeToken = true) {
        var token = jwt.sign({
            _id: data._id,
            role: data.role
        }, (secretType === "refreshToken" ? env.REFRESH_TOKEN_SECRET : env.ACCESS_TOKEN_SECRET), {
            expiresIn: data.expiresIn //7d = week , 300 = 5m
        });
        // return "JWT " + token; 
        return `${typeToken ? 'JWT ' : ''}${token}`;
    },
    create: function (model, data, parameter = []) {
        return new Promise(function (resolve, reject) {
            try {
                // console.log("create data",data)
                model.create(data, function (err, detail) {
                    // console.log("[create]err, detail", err, detail);
                    if (err) {
                        reject({
                            status: false,
                            msg: 'พบข้อผิดพลาดโปรดลองใหม่.' + JSON.stringify(err),
                            data: undefined
                        });
                    }
                    resolve({
                        status: (detail ? true : false),
                        msg: (detail ? `สร้างสำเร็จ` : `สร้างไม่สำเร็จโปรดลองใหม่ภายหลัง`),
                        data: detail
                    });
                })
            } catch (error) {
                console.log("crud[create]", error)
                resolve({
                    status: false,
                    msg: error,
                    data: undefined
                })
            }
        })
    },
    update: function (model, data, id, parameter = []) {
        return new Promise((resolve) => {
            try {
                model.update({ _id: id }, { $set: data }, function (err, detail) {
                    if (err) {
                        resolve({
                            status: false,
                            msg: 'Error getting.',
                            data: undefined
                        });
                    }
                    if (!detail) {
                        resolve({
                            status: false,
                            msg: 'No such.',
                            data: undefined
                        });
                    } else {
                        resolve({
                            status: true,
                            msg: 'update success !!',
                            data: detail
                        });
                    }
                });
            } catch (error) {
                this.debug(error.stack, { data: data, id: id, parameter: parameter });
                console.log("crud[update]", error)
                resolve({
                    status: false,
                    msg: 'Error catch.'
                });
            }
        });
    },
    getTimeZone: function () {
        try {
            var setTimeZone = new Date(new Date().toUTCString("en-US", { timeZone: "asia/bangkok" }))
            // var time = new Date(new Date(setTimeZone).setHours(setTimeZone.getHours() + (setTimeZone.getTimezoneOffset() === 0 ? 7 : 0))); 
        } catch (error) {
            console.log("getTimeZone", error)
        }
        return setTimeZone;
    },
    account_update_size_bytes: function (account_id = undefined, total_size_bytes = 0) {
        if (!account_id || !total_size_bytes) {
            console.log("account_update_size_bytes :: not run !");
            return
        }

        accountModel.findOne({ _id: account_id })
            .exec(async function (err, userInfo) {
                if (err || !userInfo) {
                    return done(null, false);
                }

                let total_used_storage = userInfo.total_used_storage;

                accountModel.update({ _id: account_id }, { $set: { total_used_storage: (total_used_storage + total_size_bytes) } }, function (err, detail) {
                    // console.log("detail", detail);
                })

            })

    },
    permission_role: function (role = [], token) {
        try {
            var _allowed = false
            role.forEach((_role) => {
                if (_role === token.role) {
                    console.log("Permission_role->[role]", (token.name ? "[" + _consoles.FgYellow(token._id) + "] " + token.name : ""), _role)
                    _allowed = true
                }
            })
            return {
                status: (_allowed ? true : false),
                msg: (_allowed ? undefined : 'You do not permission to access.')
            }
        } catch (error) {
            return {
                status: false,
                msg: 'You do not permission to access. [1]'
            };
        }
    },
    convert_file_size: function (sizeInBytes) {
        const units = ["B", "KB", "MB", "GB", "TB", "PB", "EB"];
        let convertedSize = sizeInBytes;
        let unitIndex;

        for (unitIndex = 0; convertedSize >= 1024 && unitIndex < units.length - 1; unitIndex++) {
            convertedSize /= 1024;
        }

        return [convertedSize.toFixed(2), units[unitIndex]];
    },
    status_server: async function () {

        const _ram = (() => {
            const _used = this.convert_file_size(os.freemem());
            const _total = this.convert_file_size(os.totalmem())
            return {
                percen: (os.freemem() / os.totalmem()) * 100,
                used: {
                    display: `${_used[0]}${_used[1]}`,
                    unit: os.freemem()
                },
                total: {
                    display: `${_total[0]}${_total[1]}`,
                    unit: os.totalmem()
                }
            }
        })()

        const _get_cpu = (() => {
            const cpuInfo = os.cpus();
            const numCPUs = os.cpus().length;
            const cpuUsage = getCpuUsage(cpuInfo);

            function getCpuUsage(cpuInfo) {
                const totalUsage = cpuInfo.reduce((total, core) => {
                    return total + core.times.user + core.times.sys + core.times.irq;
                }, 0);
                const totalIdle = cpuInfo.reduce((total, core) => {
                    return total + core.times.idle;
                }, 0);
                const total = totalUsage + totalIdle;
                const totalUsagePercent = (totalUsage / total) * 100;
                const activeCores = cpuInfo.filter(core => {
                    const coreUsage = (core.times.user + core.times.sys + core.times.irq) / total * 100;
                    return coreUsage > 0;
                }).length;
                return {
                    percen: totalUsagePercent,
                    activeCores
                };
            }

            function getUptimeString(uptime) {
                const secondsPerMinute = 60;
                const secondsPerHour = secondsPerMinute * 60;
                const secondsPerDay = secondsPerHour * 24;
                const secondsPerMonth = secondsPerDay * 30; // Assumes a 30-day month

                if (uptime < secondsPerMinute) {
                    return `${uptime.toFixed(0)} seconds`;
                } else if (uptime < secondsPerHour) {
                    const minutes = Math.floor(uptime / secondsPerMinute);
                    const seconds = Math.floor(uptime % secondsPerMinute);
                    return `${minutes} minute${minutes === 1 ? '' : 's'}, ${seconds} second${seconds === 1 ? '' : 's'}`;
                } else if (uptime < secondsPerDay) {
                    const hours = Math.floor(uptime / secondsPerHour);
                    const minutes = Math.floor((uptime % secondsPerHour) / secondsPerMinute);
                    return `${hours} hour${hours === 1 ? '' : 's'}, ${minutes} minute${minutes === 1 ? '' : 's'}`;
                } else if (uptime < secondsPerMonth) {
                    const days = Math.floor(uptime / secondsPerDay);
                    const hours = Math.floor((uptime % secondsPerDay) / secondsPerHour);
                    return `${days} day${days === 1 ? '' : 's'}, ${hours} hour${hours === 1 ? '' : 's'}`;
                } else {
                    const months = Math.floor(uptime / secondsPerMonth);
                    const days = Math.floor((uptime % secondsPerMonth) / secondsPerDay);
                    return `${months} month${months === 1 ? '' : 's'}, ${days} day${days === 1 ? '' : 's'}`;
                }
            }
            return {
                name: cpuInfo[0].model,
                speed: `${(cpuInfo[0].speed / 1000).toFixed(2)} GHz`,
                uptime: getUptimeString(os.uptime()),
                percen: cpuUsage.percen,
                core: `${cpuUsage.activeCores}/${numCPUs}`
            }
        })()

        const _storage = (() => {

            const getStorageInformation = (async () => {
                return new Promise((resolve) => {

                    let available = 0;
                    let used = 0;
                    let free = 0;

                    df(function (error, response) {
                        if (error) {
                            console.error('Error retrieving disk space:', error);
                            return resolve({ total: available, used: used });
                        }

                        const _index = response.findIndex(v => v.mount === '/');
                        available = response[_index].size;
                        response.forEach((disk) => {
                            used += disk.used;
                            free += disk.available;
                        });

                        resolve({ total: (available * 1024), used: (used * 1024) })
                    })
                });


                // return { total: _get_diskInfo[_index].blocks, used: _get_diskInfo[_index].used }

            })

            return new Promise(async (resolve) => {
                // Usage example
                const _get_storage_ = await getStorageInformation();
                let _set_data = {
                    percen: 0,
                    total: {
                        display: ``,
                        unit: 0
                    },
                    used: {
                        display: ``,
                        unit: 0
                    }
                }

                const _total = this.convert_file_size(_get_storage_.total);
                const _used = this.convert_file_size(_get_storage_.used);
                _set_data.used.display = `${_used[0]}${_used[1]}`;
                _set_data.used.unit = _get_storage_.used;
                _set_data.total.display = `${_total[0]}${_total[1]}`;
                _set_data.total.unit = _get_storage_.total;

                resolve(_set_data)
            })
        })();

        const _ram_rabbitmq = (() => {
            return new Promise((resolve) => {
                const _total = this.convert_file_size(os.totalmem())
                const _convert_file_size = ((sizeInBytes) => {
                    return this.convert_file_size(sizeInBytes);
                })
                var options = {
                    'method': 'GET',
                    'url': 'http://159.65.131.143:15672/api/nodes',
                    'headers': {
                        'Authorization': 'Basic YWRtaW46UGF0Y2gyNF9y',
                        'Cookie': 'connect.sid=s%3As0r_LeXvVXGhcUHduTWmfqEnaZonk0To.X52Km%2BwVQ0WUUu5sPhVKq5BR%2BmKJi4Ve6At3niCT3fM'
                    }
                };
                request(options, function (error, response) {

                    let _set_data = {
                        percen: 0,
                        used: {
                            display: ``,
                            unit: 0
                        },
                        total: {
                            display: `${_total[0]}${_total[1]}`,
                            unit: os.totalmem()
                        }
                    }

                    try {

                        const _res_mq = JSON.parse(response.body)[0];
                        const _unit = (_res_mq.mem_used ? Number(_res_mq.mem_used) : 0);
                        const _used = _convert_file_size(_unit);

                        _set_data.percen = (_unit / _set_data.total.unit) * 100;
                        _set_data.used.display = `${_used[0]}${_used[1]}`;
                        _set_data.used.unit = _unit;


                    } catch (err) {
                        console.log("err", err);
                    }

                    resolve(_set_data);
                });
            })
        })();

        return {
            ram: _ram,
            storage: await _storage,
            cpu: _get_cpu,
            rabbitmq_ram: await _ram_rabbitmq
        }
    },
    copyFiles: function (files = [], sourceDir, destinationDir) {
        return new Promise((resolve) => {
            for (let index = 0; index < files.length; index++) {
                const fileName = files[index];
                const sourceFile = path.join(sourceDir, fileName);
                const destinationFile = path.join(destinationDir, fileName);

                fs.copyFile(sourceFile, destinationFile, (error) => {
                    if (error) {
                        console.error(`Error copying file ${fileName}:`, error);
                    } else {
                        console.log(`File ${fileName} copied successfully`);
                    }
                });
            }
            resolve();
        })
    },
    remove_dir: function (path_dri) {
        if (fs.existsSync(path_dri)) {
            fs.rmSync(path_dri, { recursive: true, force: true });
        }
    },
    re_create_dir: function (path_dri) {
        return new Promise((resolve) => {
            this.remove_dir(path_dri);
            fs.mkdirSync(path_dri);
            setTimeout(() => {
                resolve();
            }, 300);
        })
    },
    create_dir: function (path_dri) {
        return new Promise((resolve) => {
            if (!fs.existsSync(path_dri)) {
                fs.mkdirSync(path_dri);
            }
            setTimeout(() => {
                resolve();
            }, 300);
        })
    },
    zip_file: function (path_dir, path_dir_output) {
        return new Promise((resolve, reject) => {
            const output = fs.createWriteStream(path_dir_output);
            const archive = archiver('zip', { zlib: { level: 9 } });
            const _file_name = String(path_dir_output).split("/")[(String(path_dir_output).split("/").length - 1)];

            output.on('close', (() => {
                console.log(archive.pointer() + ' total bytes');
                console.log('archiver has been finalized and the output file descriptor has closed.');
                const _file_size = this.convert_file_size(archive.pointer());
                resolve(`File name : ${_file_name} ${_file_size[0]}${_file_size[1]}`);
            }));

            archive.on('error', (error) => {
                console.error('Error zipping files:', error);
                reject(`[Error] File name : ${_file_name} , ${JSON.stringify(error)} `);
            });

            archive.pipe(output);
            archive.directory(path_dir, false);
            archive.finalize();
        });
    },
    upzip_file: function (path_file_zip, path_dir_output) {
        return new Promise((resolve) => {
            fs.createReadStream(path_file_zip)
                .pipe(unzipper.Extract({ path: path_dir_output }))
                .on('finish', () => {
                    console.log('Extraction complete.');
                    resolve(`unzip : cumplete`)
                })
                .on('error', (err) => {
                    console.error('Error occurred while extracting:', err);
                    resolve(`unzip : error -> ${err}`)
                });
        })
    },
    _get_palm_price: function () {
        return new Promise((resolve) => {
            palmPriceModel.findOne({
                isActive: true
            }, ["title", "price_avg"])
                .sort({
                    create_date: -1
                })
                .exec(async function (err, palmPriceInfo) {
                    resolve({
                        status: (palmPriceInfo ? true : false),
                        data: (palmPriceInfo ? { title: palmPriceInfo.title, price: palmPriceInfo.price_avg.price } : { title: "", price })
                    })
                })
        })
    },
    account_status_online: function (accountId = "", type = "user", status = false, getList = false) {

        const _get_account = (() => {
            return new Promise((resolve) => {
                accountModel.aggregate([
                    {
                        '$project': {
                            '_id': 1,
                            'full_name': 1,
                            'profile_url': 1,
                            'isLogOn': 1
                        }
                    }, {
                        '$addFields': {
                            'isLogOn': {
                                '$ifNull': [
                                    '$isLogOn', false
                                ]
                            }
                        }
                    }, {
                        '$sort': {
                            'isLogOn': -1
                        }
                    }
                ])
                    .exec(async function (err, accountInfo) {

                        if (err || !accountInfo.length) {
                            return resolve([]);
                        }

                        resolve(accountInfo);

                    })
            })
        })

        const _get_service = (() => {
            return new Promise((resolve) => {
                apiKeyModel.aggregate([
                    {
                        '$project': {
                            '_id': 1,
                            'full_name': '$name',
                            'profile_url': 'default.png',
                            'isLogOn': 1
                        }
                    }, {
                        '$addFields': {
                            'isLogOn': {
                                '$ifNull': [
                                    '$isLogOn', false
                                ]
                            }
                        }
                    }
                ])
                    .exec(async function (err, accountInfo) {

                        if (err || !accountInfo.length) {
                            return resolve([]);
                        }

                        resolve(accountInfo);

                    })
            })
        })

        // console.log(
        //     accountId, type, status, getList
        // );

        return new Promise(async (resolve) => {

            /**
             * กรณีต้องการผู้ใช้งานที่ออนไลน์
             */
            if (getList) {

                await Promise.all([_get_account(), _get_service()]).then((res_promise) => {
                    return resolve({
                        account: res_promise[0],
                        bot: res_promise[1]
                    })
                })
            }
            if (type === "bot") {
                await this.update(apiKeyModel, { isLogOn: status }, accountId, { accountId: accountId, status: status, type: type }).then((res_update) => {
                    return resolve(res_update);
                })
            }

            this.update(accountModel, { isLogOn: status }, accountId, { accountId: accountId, status: status, type: type }).then((res_update) => {
                resolve(res_update);
            })
        })
    }

}