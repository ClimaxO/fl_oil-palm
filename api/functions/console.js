
module.exports = {
	color: function () {
		return {
			FgGreen: function (txt) {
				return ("\x1b[32m" + txt + "\x1b[0m")
			},
			FgRed: function (txt) {
				return ("\x1b[31m" + txt + "\x1b[0m")
			},
			FgYellow: function (txt) {
				return ("\x1b[33m" + txt + "\x1b[0m")
			},
			FgBlue: function (txt) {
				return ("\x1b[34m" + txt + "\x1b[0m")
			},
			FgMagenta: function (txt) {
				return ("\x1b[35m" + txt + "\x1b[0m")
			},
			FgCyan: function (txt) {
				return ("\x1b[36m" + txt + "\x1b[0m")
			},
		}
	}
};
