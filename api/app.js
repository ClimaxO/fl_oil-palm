const express = require('express');
const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
const path = require('path');
const _console = require('./functions/console');

require('dotenv').config()

var morgan = require('morgan');
var socket_io = require('socket.io');
const crud = require('./functions/crud');
const env = process.env;
/**
 * create "middleware"
 * Logget Mode : combined , dev
 */
var logger = morgan((env.API_PRODUCTION === "DEV" ? 'combined' : 'dev'))

console.log(`Server Mode : ${_console.color().FgRed(env.API_PRODUCTION)}`);

Sentry.init({
  dsn: "https://5c7b02252f3a4c0a99f3229ddbf1565a@o4505192382595072.ingest.sentry.io/4505192385282048",
  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new Tracing.Integrations.Express({ app }),
    // Automatically instrument Node.js libraries and frameworks
    ...Sentry.autoDiscoverNodePerformanceMonitoringIntegrations(),
  ],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: (env.API_PRODUCTION === "DEV" ? 0 : 1.0),
});

var app = express();
var io = socket_io();
let userConnect = [];
app.io = io;
app._console = _console;

const rabbitmq = require('./functions/rabbitmq')(io);
const socketCtl = require('./functions/socketCtl')(io);


mongoose.Promise = global.Promise;
mongoose.set("strictQuery", true);
mongoose.connect(env.DATABASE_MAIN);
var db = mongoose.connection;
db.on('error', (err) => {
  console.log('MongoDB Error : ' + err);
});
db.on('connected', () => {
  console.log('MongoDB connected to database :' + _console.color().FgGreen(' Main'));
});

// RequestHandler creates a separate execution context, so that all
// transactions/spans/breadcrumbs are isolated across requests
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(logger);
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: false }));
// app.use(bodyParser());
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')))

app.use(require('express-session')({ secret: env.API_SECRET_HTTP, resave: true, saveUninitialized: true }));

app.use(passport.initialize());
app.use(passport.session());


require('./functions/passport')(passport);
app.disable('etag');

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development 
  res.locals.message = err.message;
  res.locals.error = (env.API_PRODUCTION === "DEV" ? err : {});

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/**
 * Import Route : API
 */
app.use('/', require('./routes/indexRoutes')(io));
app.use('/admin', require('./routes/adminRoutes')(io));
app.use('/user', require('./routes/userRoutes')(io));

rabbitmq.connect_to_server().then(() => {
  rabbitmq.receive();
  rabbitmq.receive_train();
});

const _socketCtl = require('./functions/socketCtl')(io);
const _send_status_admin = (async () => {
  const _status = await crud.status_server();
  const _account_online = await crud.account_status_online("", "", false, true);
  socketCtl.room('admin:log', 'process', {
    admin: {
      status_server: _status,
      account_online: _account_online
    }
  })
})

io.on('connection', (socket) => {

  // console.log("socket", socket);
  console.log("incoming socket")


  socket.on('socket:login', async (msg) => {
    console.log("socket:login");
    const _login_check = await _socketCtl.login(msg, socket);
    if (_login_check) {
      console.log(_console.color().FgGreen('Socket Login : '), _login_check);

      /**
       * Join Room Socket
       */
      socket.join(`client:${_login_check._id}`);
      userConnect.push(_login_check);

      if (_login_check.role === "ADMIN") {
        socket.join('admin:log');
        await _send_status_admin();
        socketCtl.room('admin:log', 'process', {
          admin: {
            log: {
              time: crud.getTimeZone(),
              msg: `[User] ${_login_check["full_name"]}, ${_login_check["platform"]}, เข้าสู่ระบบแล้ว`
            }
          }
        })
      }

      console.log(`Online :: ${userConnect.length}`);

    }
  })

  socket.on('socket:login:bot', async (msg) => {
    console.log("socket:login:bot", msg);

    const _login_check = await _socketCtl.login_bot(msg, socket);
    if (_login_check) {
      io.emit('socket:login:bot:status', { status: true, msg: 'login success !!' });
      console.log(_console.color().FgGreen('Socket Login : '), _login_check);
      await _send_status_admin();
      socket.join('bot:train');
      userConnect.push(_login_check);
      /**
       * ทดสอบส่ง ผ่าน room event name : process !
       */
      socketCtl.room('admin:log', 'process', {
        admin: {
          log: {
            time: crud.getTimeZone(),
            msg: '[Bot] เข้าสู่ระบบสำเร็จแล้ว'
          }
        }
      })
    } else {
      io.emit('socket:login:bot:status', { status: false, msg: 'login failed !!' });
      socketCtl.room('admin:log', 'process', {
        admin: {
          log: {
            time: crud.getTimeZone(),
            msg: '[Bot] เข้าสู่ระบบไม่สำเร็จ'
          }
        }
      })
    }


  })

  socket.on('socket:bot:update', async (msg) => {
    /**
     * บอทส่งข้อมูลอัพเดทให้แอดมิน
     * ส่งมาแบบ json { msg : "ต้องการบอก" , jobsId : "ไอดีที่กำลังเทรนที่ส่งให้ไปพร้อมลิ้งดาวน์โหลด" , progress : "01" | undefined }
     */


    if (msg.jobsId && msg.progress) {
      const TMIModel = require('./models/TMIModel');

      crud.update(TMIModel, { progress: msg.progress }, msg.jobsId, { socket: msg }).then((res_update) => {

        socketCtl.room(`admin:log`, 'process', {
          admin: {
            log: {
              time: crud.getTimeZone(),
              msg: `อัพเดท JobsId : ${msg.jobsId} , status : ${res_update.status} !!`,
              jobsId: msg.jobsId
            }
          }
        })

      })
      return
    }

    // console.log("log:bot", msg);
    socketCtl.room(`admin:log`, 'process', {
      admin: {
        log: {
          time: crud.getTimeZone(),
          msg: msg.msg,
          jobsId: msg.jobsId
        }
      }
    })

  })

  socket.on('send:msg', (msg) => {
    console.log('message: ' + msg);
    io.emit('res:msg', msg);
  });

  socket.on('disconnect', async () => {
    const _index = userConnect.findIndex(v => v.socket_id === socket.id);

    if (_index !== -1) {
      const _accountInfo = userConnect[_index];
      console.log(_console.color().FgRed('user disconnect :'), userConnect.length, `index : ${_index}`, _accountInfo["full_name"], _accountInfo["platform"]);
      userConnect.splice(_index, 1);

      /**
       * เช็คว่าต้องส่งข้อมูลให้แอดมินหรือไม่ กรณียังมีแอดมินเข้าสู่ระบบอยู่
       */
      const _index_admin = userConnect.findIndex(v => v.role === "ADMIN");
      if (_index_admin !== -1) {
        await _send_status_admin();
        socketCtl.room('admin:log', 'process', {
          admin: {
            log: {
              time: crud.getTimeZone(),
              msg: `${_accountInfo["role"] === "SERVICE" ? "[Bot]" : "[User]"} ${_accountInfo["full_name"]}, ${_accountInfo["platform"]}, ออกจากระบบแล้ว`
            }
          }
        })
      }

      /**
       * เช็คว่าผู้ใช้งานล็อกอินซ้ำไหมเพื่อไม่ให้อัพเดทออนไลน์เป็นออฟไลน์
       */
      const _index_account = userConnect.findIndex(v => v._id === _accountInfo["_id"]);
      if (_index_account === -1) {
        crud.account_status_online(_accountInfo["_id"], _accountInfo["role"] === "SERVICE" ? "bot" : "user");
        await _send_status_admin();
      }

    }

  });

})

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

module.exports = app;
