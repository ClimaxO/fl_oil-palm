module.exports = function (io) {
    const express = require('express');
    const router = express.Router();
    const indexController = require('../controllers/indexController')(io);

    router.get('/', indexController.main);
    router.get('/detail', indexController.detail);
    router.post('/auth', indexController.auth_login);
    router.get('/package', indexController.package_info);

    return router;
}