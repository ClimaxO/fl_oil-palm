module.exports = function (io) {
    const express = require('express');
    const passport = require('passport');
    const router = express.Router();
    const userController = require('../controllers/userController')(io);

    router.get('/', passport.authenticate('jwt', { session: false }), userController.me_info);
    router.put('/update', passport.authenticate('jwt', { session: false }), userController.me_update);
    router.post('/update/profile', passport.authenticate('jwt', { session: false }), userController.me_update_profile);
    router.post('/create', passport.authenticate('jwt', { session: false }), userController.create);
    router.post('/reset-password', passport.authenticate('jwt', { session: false }), userController.reset_password);
    router.get('/image/:fileName', userController.imageView);
    router.get('/profile/:fileName', userController.imageProfileView);
    router.get('/palm-info', passport.authenticate('jwt', { session: false }), userController.get_palm_info);
    router.get('/history-today', passport.authenticate('jwt', { session: false }), userController.history_now);
    router.get('/historys', passport.authenticate('jwt', { session: false }), userController.history_group);
    router.get('/history/:jobsId', passport.authenticate('jwt', { session: false }), userController.historyByJobsId);
    router.put('/history/:jobsId/weigth', passport.authenticate('jwt', { session: false }), userController.history_jobsid_update_weigth);

    return router;
}