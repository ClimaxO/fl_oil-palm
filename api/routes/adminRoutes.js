module.exports = function (io) {
    const express = require('express');
    const router = express.Router();
    const passport = require('passport');
    const adminController = require('../controllers/adminController')(io);

    // router.post('/create-apikey', adminController.create_apikey);
    // router.post('/create-account', adminController.create);
    // router.post('/create-package', adminController.create_package);
    router.post('/test-queue', adminController.queue_mq);
    router.post('/test-queue-train', adminController.queue_train_mq);
    router.post('/test-queue-train/empty', adminController.empty_train);
    // router.post('/create-tmi-progress', adminController.create_progress_tmi);


    router.get('/chart/train/:modelId/:file_name', adminController.view_image_model_chart);
    router.get('/files/train/:file_name', adminController.dl_file_train_model);
    router.get('/files/train-update', adminController.update_model_tranin);

    router.get('/palm/crud', passport.authenticate('jwt', { session: false }), adminController.crud_palm_list);
    router.get('/palm/crud/update', passport.authenticate('jwt', { session: false }), adminController.crud_palm_update);
    router.post('/palm/crud/create', passport.authenticate('jwt', { session: false }), adminController.crud_palm_create);
    router.post('/palm/crud/active', passport.authenticate('jwt', { session: false }), adminController.crud_palm_is_active);

    router.get('/account', passport.authenticate('jwt', { session: false }), adminController.view_account);
    router.put('/account/create', passport.authenticate('jwt', { session: false }), adminController.create_account);
    router.put('/account/update', passport.authenticate('jwt', { session: false }), adminController.update_account);
    router.put('/account/reset-password', passport.authenticate('jwt', { session: false }), adminController.reset_password_account);

    router.get('/tranin/model-select', passport.authenticate('jwt', { session: false }), adminController.list_model_select);
    router.get('/tranin/view-history', passport.authenticate('jwt', { session: false }), adminController.view_history);
    router.get('/tranin/view-history/:modelId', passport.authenticate('jwt', { session: false }), adminController.view_history_by_id);
    router.get('/tranin/image-training', passport.authenticate('jwt', { session: false }), adminController.view_training_await);
    router.post('/tranin/move/group', passport.authenticate('jwt', { session: false }), adminController.move_group_by_id);
    router.post('/tranin/create', passport.authenticate('jwt', { session: false }), adminController.create_tranin);
    router.post('/tranin/used', passport.authenticate('jwt', { session: false }), adminController.update_used_model);
    // router.get('/create-progress', adminController.create_progress);
    // router.get('/create-key-group', adminController.create_key_group);

    return router;
}