export const environment = {
  production: true,
  api: {
    server: 'http://159.65.131.143:6060',
    storage_img: '/user/image/',
    storage_profile: '/user/profile/',
    storage_chart: '/admin/chart/train/',
  }
};
