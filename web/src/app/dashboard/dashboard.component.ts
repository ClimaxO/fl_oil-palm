import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GlobalService } from '../_service/global.service';
import { AuthService } from '../auth/auth.service';
import { RES_JOBS_TO_DAY } from '../class/res_jobs_today.class';
import { Chart, registerables } from 'chart.js';
import { optionChart } from '../view-history/view-history.component';
Chart.register(...registerables);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  isLoading: boolean = true;
  page: { jobs: Array<RES_JOBS_TO_DAY>, chart: Array<SUMMARY_GROUP_CHART>, title: string } = {
    jobs: [],
    chart: [],
    title: ""
  }
  _chart_view!: Chart;
  @ViewChild('canvasViewsRank') canvasRef: ElementRef | undefined;

  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) {
    this.global.setTitlePage("หน้าหลัก");
  }

  ngOnInit(): void {
    this._load();
  }

  _load() {


    this.auth.getData(`user/history-today`)
      .subscribe((event: any) => {
        // console.log(event); 

        this.isLoading = false;

        if (event.status) {
          this.page.jobs = event.data
          this.page.chart = event.chart;
          setTimeout(() => {
            this._load_chart()
          }, 1000);
        }

      })


  }

  _load_chart() {

    if (this._chart_view) { this._chart_view.destroy(); }

    const ctx = this.canvasRef?.nativeElement.getContext('2d');
    let _labels: Array<string> = [];
    let _score: Array<number> = [];

    for (let index = 0; index < this.page.chart.length; index++) {
      const _group = this.page.chart[index];
      _labels.push(_group.name_th);
      _score.push(Number(_group.percen.toFixed(2)));
    }

    this._chart_view = new Chart(ctx, {
      type: 'line',
      data: {
        labels: _labels,
        datasets: [
          {
            label: "ระดับผล",
            data: _score,
            fill: 'start',
            backgroundColor: '#a1cfff',
            borderColor: 'rgba(30, 150, 252, 1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(30, 150, 252, 1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointHoverBackgroundColor: 'rgba(30, 150, 252, 1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 5,
            pointRadius: 5,
            pointHitRadius: 35,
            barThickness: 20
          }
        ]
      },
      options: new optionChart().optionChart()
    })
  }

  /**
   * END
   */

}

export interface SUMMARY_GROUP_CHART {
  _id: string;
  name_th: string;
  name_en: string;
  percen: number;
}

