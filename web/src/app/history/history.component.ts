import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../_service/global.service';
import { AuthService } from '../auth/auth.service';
import { RES_JOBS_TO_DAY } from '../class/res_jobs_today.class';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  isLoading: boolean = true;
  page: Array<{
    _id: string,
    jobs: Array<RES_JOBS_TO_DAY>
  }> = []

  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) {
    this.global.setTitlePage("ประวัติ");
  }

  ngOnInit(): void {
    this._load();
  }

  _load() {

    this.isLoading = true;
    this.auth.getData(`user/historys`)
      .subscribe((event: any) => {
        // console.log(event);

        this.isLoading = false;

        if (event.status) {
          this.page = event.data
        }

      })


  }


}
