import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSidedarUserComponent } from './menu-sidedar-user.component';

describe('MenuSidedarUserComponent', () => {
  let component: MenuSidedarUserComponent;
  let fixture: ComponentFixture<MenuSidedarUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuSidedarUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSidedarUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
