import { Component, OnInit } from '@angular/core';
import { MENU_SIDEBAR_CLASS } from '../class/menu_sidebar.class';

@Component({
  selector: 'app-menu-sidedar-user',
  templateUrl: './menu-sidedar-user.component.html',
  styleUrls: ['./menu-sidedar-user.component.scss']
})
export class MenuSidedarUserComponent implements OnInit {

  menu_sidebar: Array<MENU_SIDEBAR_CLASS> = [
    {
      title: "Dashboard",
      page_url: '/dashboard',
      icon_name: "speedometer-outline",
      isOpen: false
    },
    {
      title: "History",
      page_url: '/history',
      icon_name: "time-outline",
      isOpen: false
    },
    {
      title: "Package",
      page_url: '/package',
      icon_name: "file-tray-stacked-outline",
      isOpen: false
    }
  ];

  isHideForMobile: boolean = false;

  constructor() {
  }

  ngOnInit(): void {

    this._menu_check();

  }

  _menu_active() {
    this.isHideForMobile = (this.isHideForMobile ? false : true);
  }

  _menu_check() {
    const _pathname = window.location.pathname;
    this.menu_sidebar.forEach((_m, i) => {
      let _c = (_m.page_url === _pathname ? true : false);
      this.menu_sidebar[i].isOpen = _c;
    })
  }

  /**
   * END
   */

}
