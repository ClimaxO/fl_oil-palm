import { Component, Input, OnInit } from '@angular/core';

import { AuthService } from "../auth/auth.service";
import { MemoryRamBrowserService } from '../_service/memory-ram-browser.service';
import { GlobalService } from '../_service/global.service';

@Component({
  selector: 'app-nav-bar-user',
  templateUrl: './nav-bar-user.component.html',
  styleUrls: ['./nav-bar-user.component.scss']
})
export class NavBarUserComponent implements OnInit {

  isOpenMenuAccount: boolean = false;

  constructor(
    private AuthService: AuthService,
    public global: GlobalService,
    public mrbs: MemoryRamBrowserService
  ) { }

  ngOnInit(): void {
  }

  _menu_account() {
    this.isOpenMenuAccount = (this.isOpenMenuAccount ? false : true);
  }

  _logout() {
    this.AuthService.logout();
  }

  /**
   * END
   */
}
