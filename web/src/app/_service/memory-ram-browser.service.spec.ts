import { TestBed } from '@angular/core/testing';

import { MemoryRamBrowserService } from './memory-ram-browser.service';

describe('MemoryRamBrowserService', () => {
  let service: MemoryRamBrowserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemoryRamBrowserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
