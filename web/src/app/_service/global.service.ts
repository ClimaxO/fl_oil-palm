import { Injectable } from '@angular/core';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { Title } from '@angular/platform-browser';

import { AlertButton, AlertInput } from '@ionic/core/dist/types/components/alert/alert-interface'
import { environment } from 'src/environments/environment';

import { EditProfileComponent } from '../shared/edit-profile/edit-profile.component';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(
    private _toastCtl: ToastController,
    private alertCtl: AlertController,
    private titleService: Title,
    public modalCtrl: ModalController
  ) { }

  setTitlePage(title: string) {
    this.titleService.setTitle(`FL-OPI : ${title}`);
  }

  async _toast(msg: string = "", duration: number = 1000, position: 'top' | 'middle' | 'bottom' = 'top') {
    const toast = await this._toastCtl.create({
      message: msg,
      duration: duration,
      position: position,
    });

    await toast.present();
  }

  async _alert_ui(header = "", messag = "", inputs: Array<AlertInput>, buttons: Array<AlertButton> = []) {
    const alert = await this.alertCtl.create({
      header: header,
      message: messag,
      inputs: inputs,
      buttons: buttons,
    });

    await alert.present();
  }

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    const allowedFiles = ['.jpg', '.jpeg', '.png', '.JPG', '.JPEG', '.PNG'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }

  base64ToFile(base64String: string): Blob {
    var byteString = atob(base64String.split(',')[1]);
    var mimeString = base64String.split(',')[0].split(':')[1].split(';')[0]

    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);

    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    var blob = new Blob([ab], { type: mimeString });
    return blob;
  }

  check_file_image(url: string) {
    return new Promise<{ status: boolean, width: number, height: number, url: string }>((resolve, reject) => {
      var img = new Image();
      img.src = url;
      try {
        img.onload = function () {
          // console.log("try", img);
          resolve({ status: true, width: img.width, height: img.height, url: url });
        }
      } catch (error) {
        resolve({ status: false, width: 0, height: 0, url: url });
        console.log("error", error);
      }
    })
  }

  date_to_th(datetime: string) {
    const date = new Date(datetime);

    const formattedDate = date.toLocaleString('en-TH', {
      timeZone: 'Asia/Bangkok',
      hour12: false,
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });

    return formattedDate;
  }

  async _open_edit_profile() {
    const _modal = await this.modalCtrl.create({
      component: EditProfileComponent,
    });

    _modal.present();
  }

  convert_file_size(sizeInBytes: number) {
    const units = ["B", "KB", "MB", "GB", "TB", "PB", "EB"];
    let convertedSize = sizeInBytes;
    let unitIndex;

    for (unitIndex = 0; convertedSize >= 1024 && unitIndex < units.length - 1; unitIndex++) {
      convertedSize /= 1024;
    }

    return [convertedSize.toFixed(2), units[unitIndex]];
  }

  secToHmsDisplay(strStart: string, strEnd: string) {

    if (strStart && strEnd) {
      var _sec = 1000;
      const d = Number((new Date(strEnd).getTime() - new Date(strStart).getTime()) / _sec);
      var h = Math.floor(d / 3600);
      var m = Math.floor(d % 3600 / 60);
      var s = Math.floor(d % 3600 % 60);

      var hDisplay = h > 0 ? h + " ชม. " : "";
      var mDisplay = m > 0 ? m + " นาที " + (s > 0 ? ", " : "") : "";
      var sDisplay = s > 0 ? s + " วินาที" : "";

      return (hDisplay + mDisplay + sDisplay || "-");
    } else {
      return "-";
    }
  }

  storage = (() => {
    return {
      image: ((file_name: string) => {
        return `${environment.api.server}${environment.api.storage_img}${file_name}`
        // return `http://159.65.131.143:6060${environment.api.storage_img}${file_name}`
      }),
      profile: ((file_name: string) => {
        return `${environment.api.server}${environment.api.storage_profile}${file_name}`
        // return `http://159.65.131.143:6060${environment.api.storage_profile}${file_name}`
      }),
      chart: ((model_id: string, file_name: string) => {
        return `${environment.api.server}${environment.api.storage_chart}${model_id}/${file_name}`
        // return `http://159.65.131.143:6060${environment.api.storage_chart}${model_id}/${file_name}`
      }),
    }
  })()

}
