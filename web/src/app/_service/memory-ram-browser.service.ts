import { Injectable } from '@angular/core';

import { ACCOUNT_TXT_INFO } from '../class/storage.class';

@Injectable({
  providedIn: 'root'
})
export class MemoryRamBrowserService {

  private account_shared: ACCOUNT_CLASS = {
    token: "",
    refreshToken: "",
    full_name: "",
    role: "",
    profile_url: ""
  }

  private socket_event: SOCKET_CALSS = {
    status: "",
    logs: {
      user: {
        log: []
      },
      admin: {
        log: [],
        status_server: {
          cpu: {
            name: "",
            speed: "",
            uptime: "",
            percen: 0,
            core: ""
          },
          ram: {
            percen: 0,
            used: { display: "", unit: 0 },
            total: { display: "", unit: 0 }
          },
          storage: {
            percen: 0,
            used: { display: "", unit: 0 },
            total: { display: "", unit: 0 }
          },
          rabbitmq_ram: {
            percen: 0,
            used: { display: "", unit: 0 },
            total: { display: "", unit: 0 }
          }
        },
        account_login: {
          account: [],
          bot: []
        }
      }
    }
  }

  // constructor() { }

  socket = (() => {
    return {
      status: (() => {
        return {
          get: (() => {
            return this.socket_event.status;
          }),
          set: ((status: "ONLINE" | "OFFLINE" | "") => {
            this.socket_event.status = status;
          })
        }
      })(),
      log: (() => {
        return {
          user: (() => {
            return {
              get: (() => {
                return this.socket_event.logs.user.log;
              }),
              set: ((val: { msg: string, time: string }) => {
                this.socket_event.logs.user.log.push(val);
              })
            }
          })(),
          admin: (() => {
            return {
              get: (() => {
                return this.socket_event.logs.admin.log;
              }),
              set: ((val: { msg: string, time: string }) => {
                this.socket_event.logs.admin.log.push(val);
              })
            }
          })()
        }
      })(),
      status_server: (() => {
        return {
          get: (() => {
            return this.socket_event.logs.admin.status_server;
          }),
          set: ((val: SOCKET_STATUS_SERVER | undefined) => {
            if (val) {
              this.socket_event.logs.admin.status_server = val;
            }
          })
        }
      })(),
      online: (() => {
        return {
          get: (() => {
            return this.socket_event.logs.admin.account_login;
          }),
          set: ((val: { bot: Array<SOCKET_ACCOUNT_LOGIN>, account: Array<SOCKET_ACCOUNT_LOGIN> }) => {
            if (val) {
              this.socket_event.logs.admin.account_login.account = val.account;
              this.socket_event.logs.admin.account_login.bot = val.bot;
            }
          })
        }
      })()
    }
  })();

  account = (() => {
    return {
      get: (() => {
        return this.account_shared;
      }),
      set: ((val: ACCOUNT_CLASS) => {
        this.account_shared = val;
        localStorage.setItem(ACCOUNT_TXT_INFO, JSON.stringify(this.account_shared));
      }),
      clear: (() => {
        this.account_shared = {
          token: "",
          refreshToken: "",
          full_name: "",
          role: "",
          profile_url: ""
        }
      })
    }
  })();

}


export interface ACCOUNT_CLASS {
  token: string;
  refreshToken: string;
  full_name: string;
  role: "ADMIN" | "USER" | "";
  profile_url: string;
}

export interface SOCKET_CALSS {
  status: "ONLINE" | "OFFLINE" | "",
  logs: {
    user: {
      log: Array<{
        msg: string,
        time: string
      }>
    },
    admin: {
      log: Array<{
        msg: string,
        time: string
      }>,
      status_server: SOCKET_STATUS_SERVER,
      account_login: {
        account: Array<SOCKET_ACCOUNT_LOGIN>,
        bot: Array<SOCKET_ACCOUNT_LOGIN>
      }
    }
  }
}

export interface SOCKET_STATUS_SERVER {
  cpu: {
    name: string,
    speed: string,
    uptime: string,
    percen: number,
    core: string
  },
  ram: {
    percen: number,
    used: { display: string, unit: number },
    total: { display: string, unit: number }
  },
  storage: {
    percen: number,
    used: { display: string, unit: number },
    total: { display: string, unit: number }
  },
  rabbitmq_ram: {
    percen: number,
    used: { display: string, unit: number },
    total: { display: string, unit: number }
  }
}

export interface SOCKET_ACCOUNT_LOGIN {
  _id: string;
  full_name: string;
  profile_url: string;
  isLogOn: boolean;
}