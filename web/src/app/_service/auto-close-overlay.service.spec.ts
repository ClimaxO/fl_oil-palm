import { TestBed } from '@angular/core/testing';

import { AutoCloseOverlayService } from './auto-close-overlay.service';

describe('AutoCloseOverlayService', () => {
  let service: AutoCloseOverlayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutoCloseOverlayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
