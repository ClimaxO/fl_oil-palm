import { Injectable, QueryList, ViewChildren } from '@angular/core';
import { HTMLIonOverlayElement } from '@ionic/core';
import { IonRouterOutlet } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AutoCloseOverlayService {

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet> | undefined;
  constructor(
  ) { }

  async trigger() {
    // console.log('backbutton triggered');
    try {
      const overlays = document.querySelectorAll('ion-alert, ion-action-sheet, ion-loading, ion-modal, ion-picker, ion-popover, ion-toast');
      const overlaysArr = Array.from(overlays) as HTMLIonOverlayElement[];
      overlaysArr.forEach(o => o.dismiss());
    } catch (error) {
      console.error(error);
    }

  }
}
