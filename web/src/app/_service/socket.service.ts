import { Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController, ToastController } from "@ionic/angular";
import { Socket } from "ngx-socket-io";
import { Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { MemoryRamBrowserService, SOCKET_ACCOUNT_LOGIN, SOCKET_STATUS_SERVER } from "./memory-ram-browser.service";

@Injectable()
export class SocketAPI extends Socket {

    event_listener_process: Subscription | undefined;

    constructor(
        private _toastCtl: ToastController,
        private alertCtl: AlertController,
        private router: Router,
        private route: ActivatedRoute,
        private mrbs: MemoryRamBrowserService,
    ) {

        super({
            url: environment.api.server,
            options: {
                transports: ['websocket', 'polling'],
                reconnection: true,
                reconnectionDelay: 1500,
                query: {
                    "Authorization": '',
                    "platform": "web",
                    "version": "0.0.2"
                }
            }
        });

    }

    _init() {
        this.connect();
        this._event_socket();
    }

    _event_socket() {
        this.on('disconnect', (_socket: any) => {
            console.log("disconnect", _socket);
            this.mrbs.socket.status.set('OFFLINE');
        });

        this.on('reconnect', () => {
            console.log("reconnect");
            this.mrbs.socket.status.set('');
        });

        this.on('connect', () => {
            console.log("connect");
            this.mrbs.socket.status.set('ONLINE');
            setTimeout(() => {
                this._login_auth();
            }, 1000);
        });

    }

    _login_auth() {
        this.emit("socket:login", {
            "Authorization": this.mrbs.account.get().token,
            "platform": "web",
            "version": "0.0.1"
        });

        if (!this.event_listener_process) {
            this.event_listener_process = this._event_process()
                .subscribe(async (data: SOCKET_RES) => {
                    // console.log("data", data);
                    if (data) {

                        if (data.reload && data.display) {

                            if (data.display.alert) {
                                const _url = data.reload.url;
                                const alert = await this.alertCtl.create({
                                    header: data.display.alert.title,
                                    message: data.display.alert.msg,
                                    buttons: [
                                        {
                                            text: 'ยกเลิก',
                                            role: 'close',
                                            handler: (() => {

                                            })
                                        },
                                        {
                                            text: data.display.alert.btnOk,
                                            role: 'confirm',
                                            handler: (() => {
                                                this.navigateTo(_url);
                                            })
                                        }
                                    ],
                                });

                                await alert.present();

                                return
                            }

                            if (data.display.logs) {
                                this.mrbs.socket.log.user.set({
                                    msg: data.display.logs.title,
                                    time: new Date().toLocaleTimeString()
                                })
                                this.navigateTo(data.reload.url);
                                return
                            }

                            return
                        }

                        if (!data.reload && data.display) {

                            if (data.display.toast) {
                                const toast = await this._toastCtl.create({
                                    message: data.display.toast.title,
                                    duration: 2000,
                                    position: 'top',
                                });

                                await toast.present();
                                return
                            }

                            if (data.display.logs) {
                                this.mrbs.socket.log.user.set({
                                    msg: data.display.logs.title,
                                    time: new Date().toLocaleTimeString()
                                })
                            }

                            return
                        }

                        if (data.reload) {
                            this.navigateTo(data.reload.url);
                            return
                        }

                        if (data.admin) {

                            if (data.admin.status_server) {
                                this.mrbs.socket.status_server.set(data.admin.status_server);
                            }

                            if (data.admin.log) {
                                this.mrbs.socket.log.admin.set(data.admin.log);
                            }

                            if (data.admin.account_online) {
                                this.mrbs.socket.online.set(data.admin.account_online);
                            }

                        }

                    }

                })
        }

    }

    private navigateTo(currentUrl: string) {

        if (currentUrl === this.router.url) {
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            const currentUrlReload = this.router.url;
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
                this.router.navigateByUrl(currentUrlReload);
            });
        } else {
            this.router.navigate([currentUrl], { relativeTo: this.route });
        }

    }

    private _event_process() {
        return this.fromEvent<any>(`process`).pipe(map((data: any) => data));
    }

}

export interface SOCKET_RES {
    admin: {
        status_server: SOCKET_STATUS_SERVER | undefined,
        log: {
            time: string,
            msg: string
        } | undefined,
        account_online: {
            bot: Array<SOCKET_ACCOUNT_LOGIN>,
            account: Array<SOCKET_ACCOUNT_LOGIN>
        } | undefined
    } | undefined,
    reload: {
        url: string
    } | undefined,
    display: {
        alert: {
            title: string,
            msg: string,
            btnOk: string
        } | undefined,
        toast: {
            title: string
        } | undefined,
        logs: {
            title: string
        } | undefined
    } | undefined
}