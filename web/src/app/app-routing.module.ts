import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * auth
 */
import { AuthGuard } from './auth/auth.guard';

/**
 * Page
 */
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardAdminComponent } from './admin_role/dashboard-admin/dashboard-admin.component';
import { HistoryComponent } from './history/history.component';
import { PackageComponent } from './package/package.component';
import { TrainCreateComponent } from './train-create/train-create.component';
import { ViewHistoryComponent } from './view-history/view-history.component';
import { TraninHistoryComponent } from './admin_role/tranin-history/tranin-history.component';
import { TraninCreateComponent } from './admin_role/tranin-create/tranin-create.component';
import { AccountPageComponent } from './admin_role/account-page/account-page.component';
import { PalmPriceComponent } from './admin_role/palm-price/palm-price.component';
import { PackageCtlComponent } from './admin_role/package-ctl/package-ctl.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], data: { role: "USER" } },
  { path: 'dashboard/create', component: TrainCreateComponent, canActivate: [AuthGuard], data: { role: "USER" } },
  { path: 'dashboard/view/:jobsId', component: ViewHistoryComponent, canActivate: [AuthGuard], data: { role: "USER" } },
  { path: 'history', component: HistoryComponent, canActivate: [AuthGuard], data: { role: "USER" } },
  { path: 'package', component: PackageComponent, canActivate: [AuthGuard], data: { role: "USER" } },
  { path: 'admin', component: DashboardAdminComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: 'tranin-model', component: TraninHistoryComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: 'tranin-model/create', component: TraninCreateComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: 'tranin-model/view/:modelId', component: TraninCreateComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: 'account', component: AccountPageComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: 'palm', component: PalmPriceComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: 'package-control', component: PackageCtlComponent, canActivate: [AuthGuard], data: { role: "ADMIN" } },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
