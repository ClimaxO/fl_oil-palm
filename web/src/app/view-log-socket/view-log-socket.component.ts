import { Component, OnInit } from '@angular/core';
import { SocketAPI } from '../_service/socket.service';
import { MemoryRamBrowserService } from '../_service/memory-ram-browser.service';

@Component({
  selector: 'view-log-socket',
  templateUrl: './view-log-socket.component.html',
  styleUrls: ['./view-log-socket.component.scss']
})
export class ViewLogSocketComponent implements OnInit {

  logs: Array<string> = [];
  isUserAllowed: boolean = false;

  constructor(
    public socketService: SocketAPI,
    public mrbs: MemoryRamBrowserService,
  ) {
  }

  ngOnInit(): void {
  }

  _open_log() {
    this.isUserAllowed = (this.isUserAllowed ? false : true);
  }

  /**
   * END
   */

}
