import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLogSocketComponent } from './view-log-socket.component';

describe('ViewLogSocketComponent', () => {
  let component: ViewLogSocketComponent;
  let fixture: ComponentFixture<ViewLogSocketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewLogSocketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLogSocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
