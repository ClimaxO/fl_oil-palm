import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgxFileDropModule } from 'ngx-file-drop';
import { NgxImageCompressService } from 'ngx-image-compress';
import { SocketIoModule } from 'ngx-socket-io';
import { DataTablesModule } from "angular-datatables";
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/**
 * Directive
 */
import { ImageModule } from './image/image.module';

/**
 * Service
 */
import { SocketAPI } from './_service/socket.service';
import { MemoryRamBrowserService } from './_service/memory-ram-browser.service';

/**
 * auth
 */
import { AuthInterceptor } from './auth/auth.interceptor';

/**
 * Page User
 */
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavBarUserComponent } from './nav-bar-user/nav-bar-user.component';
import { MenuSidedarUserComponent } from './menu-sidedar-user/menu-sidedar-user.component';
import { CardHistoryComponent } from './shared/card-history/card-history.component';
import { TrainCreateComponent } from './train-create/train-create.component';
import { ViewHistoryComponent } from './view-history/view-history.component';
import { ViewLogSocketComponent } from './view-log-socket/view-log-socket.component';

/**
 * Page Admin
 */
import { DashboardAdminComponent } from './admin_role/dashboard-admin/dashboard-admin.component';
import { MenuSidebarAdminComponent } from './admin_role/menu-sidebar-admin/menu-sidebar-admin.component';
import { NavBarAdminComponent } from './admin_role/nav-bar-admin/nav-bar-admin.component';
import { HistoryComponent } from './history/history.component';
import { PackageComponent } from './package/package.component';
import { MetersCanvasComponent } from './shared/meters-canvas/meters-canvas.component';
import { TraninHistoryComponent } from './admin_role/tranin-history/tranin-history.component';
import { TraninCreateComponent } from './admin_role/tranin-create/tranin-create.component';
import { ViewImageStorageComponent } from './shared/view-image-storage/view-image-storage.component';
import { ViewEmptyDataComponent } from './shared/view-empty-data/view-empty-data.component';
import { UiLoadingTextComponent } from './shared/ui-loading-text/ui-loading-text.component';
import { AccountPageComponent } from './admin_role/account-page/account-page.component';
import { AccountEditPageComponent } from './admin_role/account-edit-page/account-edit-page.component';
import { EditProfileComponent } from './shared/edit-profile/edit-profile.component';
import { PalmPriceComponent } from './admin_role/palm-price/palm-price.component';
import { PulseAnimationComponent } from './shared/pulse-animation/pulse-animation.component';
import { CardModelComponent } from './shared/card-model/card-model.component';
import { CardPackageComponent } from './shared/card-package/card-package.component';
import { PackageCtlComponent } from './admin_role/package-ctl/package-ctl.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    DashboardAdminComponent,
    MenuSidebarAdminComponent,
    NavBarAdminComponent,
    NavBarUserComponent,
    MenuSidedarUserComponent,
    HistoryComponent,
    PackageComponent,
    CardHistoryComponent,
    TrainCreateComponent,
    ViewHistoryComponent,
    ViewLogSocketComponent,
    MetersCanvasComponent,
    TraninHistoryComponent,
    TraninCreateComponent,
    ViewImageStorageComponent,
    ViewEmptyDataComponent,
    UiLoadingTextComponent,
    AccountPageComponent,
    AccountEditPageComponent,
    EditProfileComponent,
    PalmPriceComponent,
    PulseAnimationComponent,
    CardModelComponent,
    CardPackageComponent,
    PackageCtlComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot({
      mode: 'ios'
    }),
    NgxFileDropModule,
    SocketIoModule,
    ImageModule,
    DataTablesModule,
    ImageCropperModule,
  ],
  providers: [
    NgxImageCompressService,
    SocketAPI,
    MemoryRamBrowserService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
