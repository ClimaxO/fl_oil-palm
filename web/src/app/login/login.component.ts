import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { GlobalService } from '../_service/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: { username: string | undefined, password: string | undefined } = {
    username: undefined,
    password: undefined
  };
  passHide: boolean = true;
  isLoginFailed = false;
  errorMessage = '';

  constructor(
    private global: GlobalService,
    private authService: AuthService
  ) {
    this.global.setTitlePage("เข้าสู่ระบบ");
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {

      const _goTo = `${window.location.origin}/dashboard`;
      window.location.replace(_goTo);

    }
  }

  login(): void {
    this.authService.login(String(this.form.username), String(this.form.password));
  }

  _hide_pass() {
    this.passHide = (this.passHide ? false : true);
  }

}
