import { Component, Input, OnInit } from '@angular/core';
import { RES_MODEL_VIEW } from 'src/app/admin_role/tranin-history/tranin-history.component';

@Component({
  selector: 'card-model',
  templateUrl: './card-model.component.html',
  styleUrls: ['./card-model.component.scss']
})
export class CardModelComponent implements OnInit {

  @Input() modelInfo!: RES_MODEL_VIEW;

  constructor() { }

  ngOnInit(): void {
  }

} 