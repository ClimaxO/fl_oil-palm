import { Component, OnInit, Renderer2 } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { MemoryRamBrowserService } from 'src/app/_service/memory-ram-browser.service';
import { AuthService } from 'src/app/auth/auth.service';
import { ImageCroppedEvent, ImageTransform } from 'ngx-image-cropper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ACCOUNT_CREATE } from 'src/app/admin_role/account-edit-page/account-edit-page.component';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  account!: ACCOUNT_CREATE_CUSTOM;
  formCtl!: FormGroup;
  isLoading: boolean = true;
  isInputChange: boolean = false;

  init_crop: { event: any, res_image: any, res_image_name: string, res_image_file: File | undefined, show_ui: boolean, option: { scale: number, transform: ImageTransform } } = {
    event: "",
    res_image: "",
    res_image_name: "",
    res_image_file: undefined,
    show_ui: false,
    option: {
      scale: 1,
      transform: {}
    }
  }

  constructor(
    public global: GlobalService,
    private auth: AuthService,
    private mrbs: MemoryRamBrowserService,
    private formBuilder: FormBuilder,
    private renderer: Renderer2
  ) {


  }

  ngOnInit(): void {
    this._load();
  }

  _load() {
    this.auth.getData(`user`)
      .subscribe((event: any) => {
        // console.log(event); 

        this.isLoading = false;

        if (event.status) {
          this.account = event.data;
          this.buildForm();
        } else {
          this.global._toast(event.msg, 1500, 'middle');
          this._cancel()
        }

      })

  }

  private buildForm() {
    this.formCtl = this.formBuilder.group({
      'txt_full_name': [this.account.full_name, [Validators.required]],
    });
  }

  crop = (() => {
    return {
      fileChangeEvent: ((event: any): any => {
        // console.log("event", event);

        if (event.target.files && event.target.files[0]) {
          var reader = new FileReader();

          reader.readAsDataURL(event.target.files[0]); // read file as data url
          reader.onload = (events) => { // called once readAsDataURL is completed
            // console.log("events", events);

            if (events.target) {
              this.global.check_file_image(String(events.target["result"])).then((info) => {
                // console.log("info", info);

                if (info.width < 245 || info.height < 245) {
                  this.global._alert_ui('เกิดข้อผิดพลาด', `ขนาดไฟล์ภาพใหญ่กว่า 245*245 <p><b>ขนาดที่ได้ ${info.width}*${info.height}<b></p>`, [], [
                    {
                      role: 'close',
                      text: 'เข้าใจแล้ว',
                      handler: (() => {

                      })
                    }
                  ])
                  return
                }

                this.init_crop.res_image_name = event.target.files[0]["name"];
                this.init_crop.event = event;
                this.init_crop.show_ui = true;

              })
            }

          }/* END called once readAsDataURL is completed */
        }

      }),
      imageCropped: ((event: ImageCroppedEvent) => {
        // console.log("event", event);

        this.init_crop.res_image = event.base64;
        this.init_crop.res_image_file = <File>this.global.base64ToFile(event.base64!);
      }),
      tools: (() => {
        return {
          cropImage: ((): void => {
            this.init_crop.show_ui = false;
            this._upload_image();
          }),
          zoomIn: ((): void => {
            this.init_crop.option.scale += 0.1;
            this.init_crop.option.transform = {
              ...this.init_crop.option.transform,
              scale: this.init_crop.option.scale,
            };
          }),
          zoomOut: ((): void => {
            this.init_crop.option.scale -= 0.1;
            this.init_crop.option.transform = {
              ...this.init_crop.option.transform,
              scale: this.init_crop.option.scale,
            };
          }),
          resetZoom: ((): void => {
            this.init_crop.option.scale = 1;
            this.init_crop.option.transform = {};
          })
        }
      })()
    }
  })();

  _modalWindowFile() {
    const _element = document.querySelector("input[name=myImage]") as any;
    _element!.click();
  }

  _input_update() {
    this.isInputChange = true;
  }

  _upload_image() {

    const _set_upload: { fileName: string, file: File } = {
      fileName: this.init_crop.res_image_name,
      file: this.init_crop.res_image_file!
    }

    this.auth.uploadProfileImg(_set_upload)
      .subscribe((event: any) => {
        console.log(event);

        this.isLoading = false;

        if (event.status) {
          let _set_account = this.mrbs.account.get();
          _set_account.profile_url = event.filename;

          this.mrbs.account.set(_set_account); 

        } else {
          this.global._toast(event.msg, 1500, 'middle');
          //   this._cancel()
        }

      })

  }

  _update_profile() {
    this.isLoading = true;
    this.auth.putData(`user/update`, { ...this.formCtl.value })
      .subscribe((event: any) => {
        // console.log(event);

        this.isLoading = false;
        this.global._toast(event.msg, 2500, 'middle');
        this._cancel();

      })
  }

  _update_password() {
    this.global._alert_ui('เปลี่ยนรหัสผ่านใหม่', 'โปรดระบุรหัสผ่านเก่าและ กำหนดรหัสผ่านใหม่พร้อมทั้งยืนยันรหัสใหม่อีกครั้ง', [
      {
        type: 'password',
        label: 'Password เดิม',
        placeholder: 'Password เดิม',
        name: 'txt_pass'
      },
      {
        type: 'password',
        label: 'Password ใหม่',
        placeholder: 'Password ใหม่',
        name: 'txt_pass_new',
        min: 6
      },
      {
        type: 'password',
        label: 'Password ยืนยันอีกครั้ง',
        placeholder: 'Password ยืนยันอีกครั้ง',
        name: 'txt_pass_new_confirm'
      }
    ], [
      {
        role: 'cancel',
        text: 'ยกเลิก',
        handler: ((data) => {
        })
      },
      {
        role: 'confirm',
        text: 'ยืนยัน',
        handler: ((data) => {

          return this._call_update_password(data).then((status) => {
            return status;
          })

        })
      }
    ])
  }

  _call_update_password(data: { txt_pass: string, txt_pass_new_confirm: string, txt_pass_new: string }) {
    return new Promise((resolve) => {

      if (!data.txt_pass || !data.txt_pass_new || !data.txt_pass_new_confirm || String(data.txt_pass_new).length <= 5) {

        this.global._toast('รหัสผ่านไม่สามารถว่างได้ ต้องมีอย่างน้อย 6 ตัวอักษร', 2500, 'top');

        return resolve(false);
      }

      if (data.txt_pass_new !== data.txt_pass_new_confirm) {

        this.global._toast('รหัสผ่านยืนยันไม่ตรงกันโปรดลองใหม่', 2500, 'top');

        return resolve(false);
      }

      this.isLoading = true;
      this.auth.postData(`user/reset-password`, { ...data })
        .subscribe((event: any) => {
          // console.log(event);

          this.isLoading = false;
          this.global._toast(event.msg, 2500, 'middle');

          if (!event.status) {
            return resolve(false);
          }

          resolve(true);
        })
    })
  }

  _cancel_image() {
    this.init_crop.event = "";
    this.init_crop.show_ui = false;
  }

  _size_to_type(sizeInBytes: number) {
    const _size = this.global.convert_file_size(sizeInBytes);
    return `${_size[0]}${_size[1]}`
  }

  _cancel() {
    return this.global.modalCtrl.dismiss();
  }

}


export interface ACCOUNT_CREATE_CUSTOM extends ACCOUNT_CREATE {
  packageInfo: {
    name: string,
    detail: string,
    can_used_storage: number
  };
}