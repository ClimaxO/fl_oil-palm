import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewImageStorageComponent } from './view-image-storage.component';

describe('ViewImageStorageComponent', () => {
  let component: ViewImageStorageComponent;
  let fixture: ComponentFixture<ViewImageStorageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewImageStorageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewImageStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
