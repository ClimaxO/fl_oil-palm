import { Component, OnInit } from '@angular/core';
import { AlertInput } from '@ionic/angular';
import { GlobalService } from 'src/app/_service/global.service';
import { RES_CREATE_DATA_IMAGES_MODEL } from 'src/app/admin_role/tranin-create/tranin-create.component';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-view-image-storage',
  templateUrl: './view-image-storage.component.html',
  styleUrls: ['./view-image-storage.component.scss']
})
export class ViewImageStorageComponent implements OnInit {

  name_th: string = "";
  name_th_list: Array<{ _id: string, name: string }> = [];
  images: Array<RES_CREATE_DATA_IMAGES_MODEL> = [];
  isReload: boolean = false;
  isMode: "ADMIN" | "USER" = "USER";
  isEdit: boolean = false;
  view_image: { show: boolean, image: RES_CREATE_DATA_IMAGES_MODEL | any, index: number } = {
    show: false,
    image: {},
    index: 0
  }

  pagination = {
    currentPage: 0,
    end: 24,
    pageNum: 1,
    limit: 24,
    btn: 0
  }

  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) {

  }

  ngOnInit(): void {
    this.pagination.btn = Math.ceil(this.images.length / this.pagination.limit);
    this._load_page_view();
  }

  counter(i: number) {
    return new Array(i);
  }

  handleChange(ev: { detail: { value: number } } | any) {
    this.pagination.pageNum = ev.detail.value;
    this._load_page_view();
    // console.log("this.pagination", this.pagination);
  }

  _next_page() {
    const pageNum = this.pagination.pageNum + 1;
    if (pageNum <= this.pagination.btn) {
      this.pagination.pageNum++;
      this._load_page_view();
    }
  }

  _prev_page() {
    const pageNum = this.pagination.pageNum - 1;
    if (pageNum > 0) {
      this.pagination.pageNum--;
      this._load_page_view();
    }
  }

  _load_page_view() {
    this.pagination.currentPage = ((this.pagination.pageNum - 1) * this.pagination.limit);
    this.pagination.end = (this.pagination.currentPage + this.pagination.limit);
  }

  _move_group(index: number) {

    let _select: Array<AlertInput> = [];

    for (let index = 0; index < this.name_th_list.length; index++) {
      const _name = this.name_th_list[index];
      if (_name.name !== this.name_th) {
        _select.push({
          label: `${_name.name}`,
          type: 'radio',
          value: `${_name._id}`,
        })
      }
    }

    this.global._alert_ui("เปลี่ยนกลุ่ม", `ถ้าหากทำการเปลี่ยนกลุ่มผลลัพธ์ใหม่แล้วจะไม่สามารถเปลี่ยนกลับเป็นคะแนนเดิมได้เนื่องจากถูกเปลี่ยนเป็น 100% โดนผู้ใช้แล้ว`, _select, [
      {
        text: 'ยกเลิก',
        role: 'close',
        handler: (() => {

          return true
        })
      },
      {
        text: 'ยืนยัน',
        role: 'confirm',
        handler: ((data) => {
          console.log("data", data);
          console.log("images", this.images[index]);
          if (!data) {
            this.global._toast("โปรดเลือกกลุ่มที่ต้องการย้าย หรือกดยกเลิก", 1500, "top");
            return false;
          }

          this._update_move(this.images[index]._id, data, index);
          return true;
        })
      }
    ])
  }

  _update_move(imgId: string, groupId: string, index: number) {

    this.auth.postData(`admin/tranin/move/group`, { imgId: imgId, groupId: groupId })
      .subscribe((event: any) => {
        // console.log(event);
        if (event.status) {
          this.images.splice(index, 1);
          this.isReload = true;
        }

        this.global._toast(event.msg, 2000, 'middle');

      })

  }

  _view_image(offset: number, index: number) {
    this.view_image.index = offset + index;
    this.view_image.show = true;
    this.view_image.image = this.images[this.view_image.index];
  }

  _close_image() {
    this.view_image.show = false;
  }

  _view_image_page(select: "next" | "prev") {

    const images = ((index: number): RES_CREATE_DATA_IMAGES_MODEL | undefined => {
      return this.images[index];
    })

    switch (select) {
      case "next":

        if (images((this.view_image.index + 1))) {
          this.view_image.index = this.view_image.index + 1;
          this.view_image.image = images((this.view_image.index));
        }

        break;

      case "prev":

        if (images((this.view_image.index - 1))) {
          this.view_image.index = this.view_image.index - 1;
          this.view_image.image = images((this.view_image.index));
        }

        break;

      default:
        break;
    }
  }

  _cancel() {
    return this.global.modalCtrl.dismiss(this.isReload, 'reload');
  }

}
