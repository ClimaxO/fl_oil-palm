import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEmptyDataComponent } from './view-empty-data.component';

describe('ViewEmptyDataComponent', () => {
  let component: ViewEmptyDataComponent;
  let fixture: ComponentFixture<ViewEmptyDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewEmptyDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEmptyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
