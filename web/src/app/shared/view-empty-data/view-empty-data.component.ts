import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'view-empty-data',
  templateUrl: './view-empty-data.component.html',
  styleUrls: ['./view-empty-data.component.scss']
})
export class ViewEmptyDataComponent implements OnInit {

  @Input() title: string | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
