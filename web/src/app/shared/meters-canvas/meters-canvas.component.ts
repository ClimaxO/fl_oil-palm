import { Component, Input, AfterViewInit, ViewChild, ElementRef, HostListener } from '@angular/core';

@Component({
  selector: 'meters-canvas',
  templateUrl: './meters-canvas.component.html',
  styleUrls: ['./meters-canvas.component.scss']
})
export class MetersCanvasComponent implements AfterViewInit {

  @Input() progress!: number;
  @Input() title!: string;
  @Input() color!: string;
  @Input() bgColor!: string;
  @Input() lineWidth!: number;
  @ViewChild('meters_ui') canvas!: ElementRef<HTMLCanvasElement>;

  ctx!: CanvasRenderingContext2D


  constructor() { }

  ngAfterViewInit(): void {
    this._create();
  }

  ngOnInit(): void {
  }

  // @HostListener('window:resize', ['$event'])
  // onWindowResize(event: Event) { 
    
  //   if (this.ctx) {
  //     this.drawChart();
  //   }
  // }


  _create() {
    var ctx: CanvasRenderingContext2D = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.ctx = ctx;
    this.drawChart();
  }

  drawChart() {
    // console.log(`${this.title} :: ${this.progress}`);

    // Set the chart properties
    var ctx = this.ctx;
    var width = this.canvas.nativeElement.width;
    var height = this.canvas.nativeElement.height;
    var radius = Math.min(width, height) / 2;
    var lineWidth = this.lineWidth || 40;
    var color = this.color || "#ff6384";
    var bgColor = this.bgColor || "#e8e8e8";
    var font = "24px Arial";
    var progress = this.progress || 0;
    var text = (progress).toFixed(2) + "%";
    var title = this.title || "";
    var startAngle = Math.PI / 1.2;

    // Draw the chart function
    // function drawChart() {
    // Draw the chart
    ctx.lineWidth = lineWidth;
    ctx.lineCap = "butt";
    ctx.strokeStyle = bgColor;
    ctx.beginPath();
    ctx.lineCap = "round";
    ctx.arc(width / 2, height / 2, radius - lineWidth / 2, startAngle, startAngle + (Math.PI / 0.75));
    ctx.stroke();

    if (progress > 0) {
      ctx.strokeStyle = color;
      ctx.beginPath();
      let endAngle = startAngle + (Math.PI / 0.75) * progress / 100;
      if (progress < 100 && progress >= 95) {
        endAngle -= Math.PI * 2;
      } else if (progress < 95) {
        if (progress > 5) {
          ctx.lineCap = "round";
        }
        endAngle -= Math.PI * 10;
      }
      ctx.arc(width / 2, height / 2, radius - lineWidth / 2, startAngle, endAngle);
      ctx.stroke();
    }

    ctx.font = font;
    ctx.fillStyle = color;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(text, width / 2, height / 2);
    ctx.font = "20px Arial";
    ctx.fillText(title, width / 2, (height / 2) + 30);
    // }
  }

}
