import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetersCanvasComponent } from './meters-canvas.component';

describe('MetersCanvasComponent', () => {
  let component: MetersCanvasComponent;
  let fixture: ComponentFixture<MetersCanvasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetersCanvasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MetersCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
