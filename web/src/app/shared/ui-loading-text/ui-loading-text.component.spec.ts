import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiLoadingTextComponent } from './ui-loading-text.component';

describe('UiLoadingTextComponent', () => {
  let component: UiLoadingTextComponent;
  let fixture: ComponentFixture<UiLoadingTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiLoadingTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiLoadingTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
