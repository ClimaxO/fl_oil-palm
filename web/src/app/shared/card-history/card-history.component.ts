import { Component, Input, OnInit } from '@angular/core';
import { RES_JOBS_TO_DAY } from 'src/app/class/res_jobs_today.class';

@Component({
  selector: 'card-history',
  templateUrl: './card-history.component.html',
  styleUrls: ['./card-history.component.scss']
})
export class CardHistoryComponent implements OnInit {

  @Input() jobsInfo!: RES_JOBS_TO_DAY;

  constructor() { }

  ngOnInit(): void {
  }

}
