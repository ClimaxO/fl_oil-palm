import { Component, Input, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';
import { PACKAGE_CLASS } from 'src/app/package/package.component';

@Component({
  selector: 'card-package',
  templateUrl: './card-package.component.html',
  styleUrls: ['./card-package.component.scss']
})
export class CardPackageComponent implements OnInit {
  @Input() _packages!: Array<PACKAGE_CLASS>;
  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) { }

  ngOnInit(): void {
  }

  _size_to_type(sizeInBytes: number) {
    const _size = this.global.convert_file_size(sizeInBytes);
    return `${_size[0]}${_size[1]}`
  }

}
