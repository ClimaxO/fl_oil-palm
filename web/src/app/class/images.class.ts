export interface MAIN_IMAGES_CLASS {
    _id: string;
    account_id: string; /** id ผู้ใช้ */
    jobs_id: string; /** id jobs */
    file_name: string; /** ชื่อไฟล์ */
    file_size_bytes: 0;/** ขนาดไฟล์คำนวณเป็นไปต์ */
    create_date: string;
    update_date: string;
    group_rank: {
        top_best: MAIN_IMAGES_SUB_CLASS,
        group_list: Array<MAIN_IMAGES_SUB_CLASS>
    },
}

export interface MAIN_IMAGES_SUB_CLASS {
    _id: string, /** id หมวดหมู่ */
    name_th: string, /** ชื่อภาษาไทย */
    name_en: string, /** ชื่อภาษาอังกฤษ */
    score: number,/** คะแนน */
    file_name: string,/** ชื่อไฟล์ */
}