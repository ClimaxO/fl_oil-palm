export interface MENU_SIDEBAR_CLASS {
    title: string;
    page_url: string;
    icon_name: string;
    isOpen: boolean;
}