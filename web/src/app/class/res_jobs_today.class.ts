export interface RES_JOBS_TO_DAY {
    _id: string;
    group_top_bast: {
        _id: string,
        name_th: string,
        name_en: string,
        score: number,
    };
    form_data: JOBs_FORM_DATA | undefined;
    date: string;
    count_image: number;
    total_image: number;
}

export interface JOBs_FORM_DATA {
    seller_id: string, /** รหัสผู้ขาย */
    seller_name: string, /** ชื่อผู้ขาย */
    plot_id: string, /** แปลงที่ */
    driver_name: string, /** ชื่อผู้ขับ */
    car_no_id: string, /** ทะเบียนรถ */
    weigher_name: string, /** ชื่อชั่งชื่อ */
    evaluation_officer: string, /** เจ้าหน้าที่ประเมิน */
}