import { Component, Input, OnInit } from '@angular/core';

import { AuthService } from "../../auth/auth.service";
import { MemoryRamBrowserService } from 'src/app/_service/memory-ram-browser.service';
import { GlobalService } from 'src/app/_service/global.service';

@Component({
  selector: 'app-nav-bar-admin',
  templateUrl: './nav-bar-admin.component.html',
  styleUrls: ['./nav-bar-admin.component.scss']
})
export class NavBarAdminComponent implements OnInit {

  @Input() title!: string;
  isOpenMenuAccount: boolean = false;

  constructor(
    private AuthService: AuthService,
    public global: GlobalService,
    public mrbs: MemoryRamBrowserService
  ) { }

  ngOnInit(): void {
  }

  _menu_account() {
    this.isOpenMenuAccount = (this.isOpenMenuAccount ? false : true);
  }

  _logout() {
    this.AuthService.logout();
  }

  /**
   * END
   */
}
