import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-palm-price',
  templateUrl: './palm-price.component.html',
  styleUrls: ['./palm-price.component.scss']
})
export class PalmPriceComponent implements OnInit {
  page: {
    input: {
      title: string,
      prices: Array<{
        title: string,
        price: number,
        unit: string
      }>,
      price_avg: {
        title: string,
        price: number,
        unit: string
      }
    },
    isCreate: boolean,
    list: Array<{
      _id: string,
      title: string,
      prices: Array<{
        title: string,
        price: number,
        unit: string
      }>,
      price_avg: {
        title: string,
        price: number,
        unit: string
      },
      create_date: string,
      isActive: boolean
    }>
  } = {
      input: {
        title: "",
        prices: [],
        price_avg: {
          title: "",
          price: 0,
          unit: ""
        }
      },
      isCreate: false,
      list: []
    }

  isLoading: boolean = true;

  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) { }

  _view_create() {
    this.page.isCreate = true;
    this._load_price();
  }

  _load() {

    return new Promise((resolve) => {
      this.isLoading = true;
      this.auth.getData(`admin/palm/crud`)
        .subscribe((event: any) => {
          // console.log(event);

          if (event.status) {
            this.page.list = event.data;
          }

          if (event.msg) {
            this.global._toast(event.msg, 2000, 'middle');
          }

          this.isLoading = false;

          resolve(true);

        })
    })


  }

  _load_price() {

    return new Promise((resolve) => {
      this.isLoading = true;
      this.auth.getData(`admin/palm/crud/update`)
        .subscribe((event: any) => {
          // console.log(event);

          if (event.status) {
            this.page.input = event.data;
            if (event.msg) this.global._toast(event.msg, 4000, 'middle');
          }

          this.page.isCreate = event.status

          if (event.msg) {
            this.global._alert_ui("แจ้งเตือน", event.msg, [], [
              {
                role: 'cancel',
                text: 'เข้าใจแล้ว',
              }
            ])
          }

          this.isLoading = false;

          resolve(true);

        })
    })


  }

  _edit_price_palm(index: number) {

    const _target = this.page.input.prices[index];

    // console.log("_target", _target, index);


    this.global._alert_ui("แก้ไขราคาผลปาล์ม", _target.title, [
      {
        placeholder: 'ราคา',
        name: 'txtPrice',
        type: 'number',
        value: _target.price
      }
    ], [
      {
        role: 'cancel',
        text: 'ยกเลิก',
      },
      {
        role: 'confirm',
        text: 'ปรับราคา',
        handler: ((value) => {
          if (!value.txtPrice || value.txtPrice < 0) {
            this.global._toast("โปรดระบุราคาก่อนปรับราคาใหม่", 2000, 'middle')
            return false;
          }

          if (index === 0) {
            this.page.input.price_avg.price = value.txtPrice;
          }

          this.page.input.prices[index].price = value.txtPrice
          return true;
        }),
      }
    ])
  }

  _open_alert(index: number) {
    const _target = this.page.list[index];

    this.global._alert_ui(`ต้องการ ${_target.isActive ? '[ปิด]' : '[เปิด]'} ใช้งานหรือไม่`, _target.title, [], [
      {
        role: 'cancel',
        text: 'ยกเลิก'
      },
      {
        role: 'confirm',
        text: 'ยืนยัน',
        handler: (() => {

          this.isLoading = true;
          this.auth.postData(`admin/palm/crud/active`, { _id: _target._id })
            .subscribe((event: any) => {
              // console.log(event);

              if (event.status) {
                this._load().then(() => {
                });
              }

              this.isLoading = false;
              this.global._toast(event.msg, 2500, 'middle');

            })

        })
      }
    ])

  }

  _create() {
    this.isLoading = true;
    this.auth.postData(`admin/palm/crud/create`, { ...this.page.input })
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          this._load().then(() => {
          });
        }

        this.isLoading = false;
        this.global._toast(event.msg, 2500, 'middle');

      })
  }


  ngOnInit(): void {
    this._load().then(() => {
    });
  }

}
