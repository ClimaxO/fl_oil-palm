import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalmPriceComponent } from './palm-price.component';

describe('PalmPriceComponent', () => {
  let component: PalmPriceComponent;
  let fixture: ComponentFixture<PalmPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalmPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalmPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
