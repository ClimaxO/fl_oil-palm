import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-account-edit-page',
  templateUrl: './account-edit-page.component.html',
  styleUrls: ['./account-edit-page.component.scss']
})
export class AccountEditPageComponent implements OnInit {

  formCtl!: FormGroup;
  isLoading: boolean = false;
  isReload: boolean = false;
  isCreate: boolean = false;
  isMatchPass: boolean = false;
  account: ACCOUNT_CREATE = {
    _id: "",
    full_name: "",
    index: 0,
    email: "",
    profile_url: "",
    role: "",
    create_date: "",
    update_date: "",
    isActive: false,
    total_used_storage: 0
  }

  constructor(
    public global: GlobalService,
    private auth: AuthService,
    private formBuilder: FormBuilder
  ) {



  }

  _create() {
    console.log("_create", this.formCtl.value);
    this.isLoading = true;
    this.auth.putData(`admin/account/create`, { ...this.formCtl.value, _id: this.account._id })
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          this.isReload = true;
          setTimeout(() => {
            this._cancel();
          }, 1100);
        }

        this.isLoading = false;
        this.global._toast(event.msg, 1000, 'middle');
      })
  }

  _update() {
    this.isReload = true;
    this.isLoading = true;
    this.auth.putData(`admin/account/update`, { ...this.formCtl.value, _id: this.account._id })
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          setTimeout(() => {
            this._cancel();
          }, 1100);
        }

        this.isLoading = false;
        this.global._toast(event.msg, 1000, 'middle');
      })
  }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm() {

    if (this.isCreate) {
      this.formCtl = this.formBuilder.group({
        'txt_full_name': [this.account.full_name, [Validators.required]],
        'txt_password': ["", [Validators.required]],
        'txt_confirm_password': ["", [Validators.required]],
        'txt_email': [{ value: (this.isCreate ? "" : this.account.email), disabled: (this.isCreate ? false : true) }, [Validators.required]],
        'txt_role': [this.account.role, [Validators.required]],
        'isActive': [this.account.isActive, []],
      });

      this.formCtl.controls.txt_confirm_password.valueChanges.subscribe(() => {
        this.isMatchPass = (this.formCtl.controls.txt_confirm_password.value === this.formCtl.controls.txt_password.value && this.formCtl.controls.txt_password.value)
      })

    } else {
      this.formCtl = this.formBuilder.group({
        'txt_full_name': [this.account.full_name, [Validators.required]],
        'txt_email': [{ value: (this.isCreate ? "" : this.account.email), disabled: (this.isCreate ? false : true) }, [Validators.required]],
        'txt_role': [this.account.role, [Validators.required]],
        'isActive': [this.account.isActive, []],
      });
    }

  }

  _update_password() {
    this.global._alert_ui('เปลี่ยนรหัสผ่านใหม่', 'โปรดระบุรหัสผ่านเก่าและ กำหนดรหัสผ่านใหม่พร้อมทั้งยืนยันรหัสใหม่อีกครั้ง', [
      {
        type: 'password',
        label: 'Password ใหม่',
        placeholder: 'Password ใหม่',
        name: 'txt_pass_new',
        min: 6
      },
      {
        type: 'password',
        label: 'Password ยืนยันอีกครั้ง',
        placeholder: 'Password ยืนยันอีกครั้ง',
        name: 'txt_pass_new_confirm'
      }
    ], [
      {
        role: 'cancel',
        text: 'ยกเลิก',
        handler: ((data) => {
        })
      },
      {
        role: 'confirm',
        text: 'ยืนยัน',
        handler: ((data) => {

          return this._call_update_password(data).then((status) => {
            return status;
          })

        })
      }
    ])
  }

  _call_update_password(data: { txt_pass_new_confirm: string, txt_pass_new: string }) {
    return new Promise((resolve) => {

      if (!data.txt_pass_new || !data.txt_pass_new_confirm || String(data.txt_pass_new).length <= 5) {

        this.global._toast('รหัสผ่านไม่สามารถว่างได้ ต้องมีอย่างน้อย 6 ตัวอักษร', 2500, 'top');

        return resolve(false);
      }

      if (data.txt_pass_new !== data.txt_pass_new_confirm) {

        this.global._toast('รหัสผ่านยืนยันไม่ตรงกันโปรดลองใหม่', 2500, 'top');

        return resolve(false);
      }

      this.isLoading = true;
      this.auth.putData(`admin/account/reset-password`, { ...data, _id: this.account._id })
        .subscribe((event: any) => {
          // console.log(event);

          this.isLoading = false;
          this.global._toast(event.msg, 2500, 'middle');

          if (!event.status) {
            return resolve(false);
          }

          resolve(true);
        })
    })
  }


  _cancel() {
    return this.global.modalCtrl.dismiss(this.isReload, 'reload');
  }

}

export interface ACCOUNT_CREATE {
  _id: string;
  full_name: string;
  index: number;
  email: string;
  profile_url: string;
  role: string;
  create_date: string;
  update_date: string;
  isActive: boolean;
  total_used_storage: number;
}