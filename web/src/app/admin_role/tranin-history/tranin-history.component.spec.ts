import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TraninHistoryComponent } from './tranin-history.component';

describe('TraninHistoryComponent', () => {
  let component: TraninHistoryComponent;
  let fixture: ComponentFixture<TraninHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TraninHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TraninHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
