import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-tranin-history',
  templateUrl: './tranin-history.component.html',
  styleUrls: ['./tranin-history.component.scss']
})
export class TraninHistoryComponent implements OnInit {

  page: { create: { total: number }, model: Array<RES_MODEL>, select: string, select_old: string, history: Array<{ _id: string, data: Array<RES_MODEL_VIEW> }> } = {
    create: {
      total: 0
    },
    model: [],
    select: "",
    select_old: "",
    history: []
  }
  isLoading: boolean = true;

  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) {
    this._load();
  }

  ngOnInit(): void {
  }

  async _load() {
    this.isLoading = true;

    const _load_bg_images_training = (() => {
      return new Promise((resolve) => {
        this.auth.getData(`admin/tranin/image-training`)
          .subscribe((event: any) => {
            // console.log(event);

            if (event.status) {
              this.page.create.total = event.total;
            }

            resolve(true);

          })
      })
    })

    const _load_bg_model = (() => {
      return new Promise((resolve) => {
        this.auth.getData(`admin/tranin/model-select`)
          .subscribe((event: any) => {
            // console.log(event);

            if (event.status) {
              this.page.model = event.data;
              this.page.select = event.select;
              this.page.select_old = event.select;
            }

            resolve(true);

          })
      })
    })

    const _load_bg_history = (() => {
      return new Promise((resolve) => {
        this.auth.getData(`admin/tranin/view-history`)
          .subscribe((event: any) => {
            // console.log(event);

            if (event.status) {
              this.page.history = event.data;
            }

            resolve(true);

          })
      })
    })

    Promise.all([_load_bg_images_training(), _load_bg_model(), _load_bg_history()]).then(() => {

      this.isLoading = false;

    })
  }

  _update_select_model() {
    if (this.page.select === this.page.select_old) {
      this.global._toast("Model ที่เลือกกำลังถูกใช้งานอยู่แล้ว", 2500, 'middle');
      return
    }

    this.auth.postData(`admin/tranin/used`, { modelId: this.page.select })
      .subscribe((event: any) => {
        // console.log(event);
        if (event.status) {
          this._load();
        }

        this.global._toast(event.msg, 2500, 'middle')
      })

  }

}


export interface RES_MODEL {
  _id: string;
  version: string;
}

export interface RES_MODEL_VIEW {
  chart: {
    img_acc: string,
    img_lost: string
  };
  count_images: number;
  create_date: string;
  progressInfo: {
    _id: string,
    name_th: string,
    name_en: string
  }
  version: string;
  _id: string;
}
