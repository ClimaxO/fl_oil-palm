import { Component, OnInit } from '@angular/core';
import { MENU_SIDEBAR_CLASS } from 'src/app/class/menu_sidebar.class';

@Component({
  selector: 'app-menu-sidebar-admin',
  templateUrl: './menu-sidebar-admin.component.html',
  styleUrls: ['./menu-sidebar-admin.component.scss']
})
export class MenuSidebarAdminComponent implements OnInit {

  menu_sidebar: Array<MENU_SIDEBAR_CLASS> = [
    {
      title: "Dashboard",
      page_url: '/admin',
      icon_name: "speedometer-outline",
      isOpen: false
    },
    {
      title: "Tranin Model",
      page_url: '/tranin-model',
      icon_name: "extension-puzzle-outline",
      isOpen: false
    },
    {
      title: "Account",
      page_url: '/account',
      icon_name: "person-outline",
      isOpen: false
    },
    {
      title: "Palm Price",
      page_url: '/palm',
      icon_name: "bar-chart-outline",
      isOpen: false
    },
    {
      title: "Package",
      page_url: '/package-control',
      icon_name: "file-tray-stacked-outline",
      isOpen: false
    }
  ];

  isHideForMobile: boolean = false;

  constructor() {

  }

  ngOnInit(): void {

    this._menu_check();

  }

  _menu_active() {
    this.isHideForMobile = (this.isHideForMobile ? false : true);
  }

  _menu_check() {
    const _pathname = window.location.pathname;
    this.menu_sidebar.forEach((_m, i) => {
      let _c = (_m.page_url === _pathname ? true : false);
      this.menu_sidebar[i].isOpen = _c;
    })
  }

  /**
   * END
   */

} 