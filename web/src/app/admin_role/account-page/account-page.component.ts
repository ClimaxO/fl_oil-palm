import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';
import { AccountEditPageComponent } from '../account-edit-page/account-edit-page.component';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent implements OnInit {


  page: { account: Array<RES_ACCOUNT_VIEW> } = {
    account: []
  }

  isCreate: boolean = false;
  isLoading: boolean = true;
  dtOptions: DataTables.Settings = {};

  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) { }

  _load() {

    return new Promise((resolve) => {
      this.isLoading = true;
      this.auth.getData(`admin/account`)
        .subscribe((event: any) => {
          // console.log(event);

          if (event.status) {
            this.page.account = event.data;
          }

          this.isLoading = false;

          resolve(true);

        })
    })

  }

  ngOnInit(): void {
    this._load().then(() => {
      this._set_option_table();
    })
  }

  _set_option_table() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      autoWidth: false,
      responsive: true,
      scrollCollapse: true,
      scrollX: true
    };
  }

  _view_create_account() {
    this.isCreate = true;
    this._open_modal(-1);
  }

  async _open_modal(index: number) {

    this.isCreate = (index === -1 ? true : false);

    const _modal = await this.global.modalCtrl.create({
      component: AccountEditPageComponent,
      componentProps: {
        account: (index === -1 ? {
          _id: "",
          full_name: "",
          index: 0,
          email: "",
          profile_url: "",
          role: "",
          create_date: "",
          update_date: "",
          isActive: false,
          total_used_storage: 0
        } : this.page.account[index]),
        isCreate: this.isCreate,
      }
    });

    _modal.present();

    const { data, role } = await _modal.onWillDismiss();

    if (role === 'reload' && data) {
      this.global._toast("มีการแก้ไขข้อมูลกำลังโหลดใหม่รอสักครู่...", 2000, 'middle');
      this._load().then(() => {
        this._set_option_table();
      })
    }

  }

}


export interface RES_ACCOUNT_VIEW {
  _id: string;
  full_name: string;
  index: number;
  email: string;
  role: string;
  create_date: string;
}