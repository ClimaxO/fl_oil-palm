import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageCtlComponent } from './package-ctl.component';

describe('PackageCtlComponent', () => {
  let component: PackageCtlComponent;
  let fixture: ComponentFixture<PackageCtlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackageCtlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageCtlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
