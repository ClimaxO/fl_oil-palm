import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';
import { PACKAGE_CLASS } from 'src/app/package/package.component';

@Component({
  selector: 'app-package-ctl',
  templateUrl: './package-ctl.component.html',
  styleUrls: ['./package-ctl.component.scss']
})
export class PackageCtlComponent implements OnInit {

  isLoading: boolean = true;
  page: Array<PACKAGE_CLASS> = [];
  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) {
    this.global.setTitlePage("Package");
    this._load();
  }

  ngOnInit(): void {
  }

  _load() {

    this.isLoading = true;
    this.auth.getData(`package`)
      .subscribe((event: any) => {
        // console.log(event);

        this.isLoading = false;

        if (event.status) {
          this.page = event.data
        }

      })


  }


}
