import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/_service/global.service';
import { MemoryRamBrowserService } from 'src/app/_service/memory-ram-browser.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss']
})
export class DashboardAdminComponent implements OnInit {


  percen_process = {
    mq: 0,
    mq_c: -1
  }

  constructor(
    public mrbs: MemoryRamBrowserService,
    public global: GlobalService,
  ) {
    this.global.setTitlePage("หน้าหลัก");

    this.percen_process.mq_c = Number(setInterval(() => {
      this.percen_process.mq = ((this.mrbs.socket.status_server.get().storage.used.unit / this.mrbs.socket.status_server.get().storage.total.unit) * 100);
      if (this.percen_process.mq == 0) {
        clearInterval(this.percen_process.mq_c);
      }
    }, 1000))

  }

  ngOnInit(): void {
  }

}
