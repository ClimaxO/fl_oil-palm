import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';
import { GlobalService } from 'src/app/_service/global.service';
import { AuthService } from 'src/app/auth/auth.service';
import { ViewImageStorageComponent } from 'src/app/shared/view-image-storage/view-image-storage.component';

@Component({
  selector: 'app-tranin-create',
  templateUrl: './tranin-create.component.html',
  styleUrls: ['./tranin-create.component.scss']
})
export class TraninCreateComponent implements OnInit {

  page: { create: { total: number, data: Array<RES_CREATE_DATA_MODEL>, modelInfo: RES_MODEINFO }, isCreate: boolean, modelId: string } = {
    create: {
      total: 0,
      data: [],
      modelInfo: {
        _id: "",
        version: "",
        class: {
          mae: 0,
          rmse: 0
        },
        chart: {
          img_acc: "",
          img_lost: ""
        }
      }
    },
    isCreate: true,
    modelId: ""
  }

  isLoading: boolean = true;

  constructor(
    private route: ActivatedRoute,
    public global: GlobalService,
    private auth: AuthService,
    private router: Router
  ) {
    this.router.events.subscribe((event: Event) => {

      if (event instanceof NavigationEnd) {
        this.page.modelId = String(this.route.snapshot.paramMap.get('modelId'));



        if (this.page.modelId !== "null") {
          this.page.isCreate = false;
          this._load();
          this.global.setTitlePage(this.page.modelId);
        } else {
          this.global.setTitlePage("Training Model");
          this._load_create();
        }

      }

    });
  }

  ngOnInit(): void {
  }

  _load() {
    this.isLoading = true;
    this.auth.getData(`admin/tranin/view-history/${this.page.modelId}`)
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          this.page.create.modelInfo = event.modelInfo;
          this.page.create.data = event.data;
        }

        this.isLoading = false;
      })
  }

  _load_create() {
    this.isLoading = true;
    this.auth.getData(`admin/tranin/image-training`)
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          this.page.create.total = event.total;
          this.page.create.data = event.data;
        }

        this.isLoading = false;
      })
  }

  async _open_modal(index: number) {
    const _name_th = this.page.create.data[index].name_th;
    const _images = this.page.create.data[index].images;
    let _name_th_list = [];

    for (let i = 0; i < this.page.create.data.length; i++) {
      const _name = this.page.create.data[i];
      _name_th_list.push({
        _id: _name._id,
        name: _name.name_th
      });
    }

    const _modal = await this.global.modalCtrl.create({
      component: ViewImageStorageComponent,
      componentProps: {
        name_th: _name_th,
        name_th_list: _name_th_list,
        images: _images,
        isMode: "ADMIN",
        isEdit: this.page.isCreate
      }
    });

    _modal.present();

    const { data, role } = await _modal.onWillDismiss();

    if (role === 'reload' && data) {
      this.global._toast("มีการแก้ไขข้อมูลกำลังโหลดใหม่รอสักครู่...", 2000, 'middle');
      this._load_create();
    }

  }

  _send_data() {
    this.isLoading = true;
    let _arr = [];
    for (let index = 0; index < this.page.create.data.length; index++) {
      const _main = this.page.create.data[index];
      for (let i = 0; i < _main.images.length; i++) {
        const _img = _main.images[i];
        _arr.push(_img._id);
      }
    }

    this.auth.postData(`admin/tranin/create`, { images: _arr })
      .subscribe((event: any) => {
        // console.log(event);
        if (event.status) {
          this.router.navigate(['/tranin-model']);
        }

        this.global._toast(event.msg, 2500, 'middle')
      })

  }

  /**
   * END
   */

}


export interface RES_CREATE_DATA_MODEL {
  _id: string;
  name_th: string;
  name_en: string;
  images: Array<RES_CREATE_DATA_IMAGES_MODEL>
}

export interface RES_CREATE_DATA_IMAGES_MODEL {
  _id: string;
  tmiModeUse: {
    _id: string,
    version: string
  };
  tmiMode: {
    _id: string,
    version: string,
    confirm_group: {
      _id: string,
      name_th: string,
      name_en: string,
      score: number,
    },
  } | any;
  file_name: string;
  create_date: string;
  accountInfo: {
    _id: string,
    full_name: string,
  };
  name_th: string;
  name_en: string;
  score: number;
}

export interface RES_MODEINFO {
  _id: string;
  version: string;
  class: {
    mae: number,
    rmse: number
  },
  chart: {
    img_acc: string,
    img_lost: string
  }
}