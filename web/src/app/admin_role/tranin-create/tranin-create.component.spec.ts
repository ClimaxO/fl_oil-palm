import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TraninCreateComponent } from './tranin-create.component';

describe('TraninCreateComponent', () => {
  let component: TraninCreateComponent;
  let fixture: ComponentFixture<TraninCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TraninCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TraninCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
