import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';

import { GlobalService } from '../_service/global.service';
import { AuthService } from '../auth/auth.service';

import { MAIN_IMAGES_CLASS, MAIN_IMAGES_SUB_CLASS } from '../class/images.class';
import { JOBs_FORM_DATA } from '../class/res_jobs_today.class';
import { ChartOptions, Chart, registerables } from 'chart.js';
import { ViewImageStorageComponent } from '../shared/view-image-storage/view-image-storage.component';
Chart.register(...registerables);

@Component({
  selector: 'app-view-history',
  templateUrl: './view-history.component.html',
  styleUrls: ['./view-history.component.scss']
})
export class ViewHistoryComponent implements OnInit {

  page: {
    jobsId: string,
    detail: {
      _id: string,
      progress: string,
      progress_date: string,
      success_date: string,
      tmiModeUse: {
        _id: string,
        version: string
      }
      form_data: JOBs_FORM_DATA | undefined
      group_rank: {
        top_best: {
          name_en: string,
          name_th: string,
          price: number,
          score: number,
          weight: number,
          _id: string,
        },
        group_list: Array<{
          name_en: string,
          name_th: string,
          score: number,
          _id: string,
        }>
      },
      images: Array<MAIN_IMAGES_CLASS>,
      images_group: Array<{
        _id: string, /** id group */
        name_th: string, /** name group */
        name_en: string, /** name group */
        score: number, /** score group */
        images: Array<MAIN_IMAGES_SUB_CLASS>
      }>
    },
    time_process: string
  } = {
      jobsId: "",
      detail: {
        _id: "",
        progress: "01",
        success_date: "",
        progress_date: "",
        tmiModeUse: {
          _id: "",
          version: ""
        },
        form_data: undefined,
        group_rank: {
          top_best: {
            name_en: "",
            name_th: "",
            price: 0,
            score: 0,
            weight: 0,
            _id: "",
          },
          group_list: []
        },
        images: [],
        images_group: []
      },
      time_process: ""
    }

  _chart_view!: Chart;

  isLoading: boolean = true;

  @ViewChild('canvasViewsRank') canvasRef: ElementRef | undefined;

  constructor(
    private route: ActivatedRoute,
    public global: GlobalService,
    private auth: AuthService,
    private router: Router
  ) {

    this.router.events.subscribe((event: Event) => {

      if (event instanceof NavigationEnd) {
        this.page.jobsId = String(this.route.snapshot.paramMap.get('jobsId'));
        this.global.setTitlePage(this.page.jobsId);
        this._load();
      }

    });


  }

  ngOnInit(): void {

  }

  _load() {

    if (!this.page.jobsId) {
      this.global._alert_ui("เกิดข้อผิดพลาด", `ไม่พบรายงานที่คุณต้องการ : ${this.page.jobsId}`, [], [
        {
          text: 'เข้าใจแล้ว',
          role: 'close',
          handler: (() => {
            this.router.navigate(['/']);
            return true
          })
        }
      ])
    }


    this.auth.getData(`user/history/${this.page.jobsId}`)
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          this.isLoading = false;
          this.page.detail = event.data

          if (this.page.detail.progress === "03") {
            setTimeout(() => {
              this._load_chart();
              this.page.time_process = this.global.secToHmsDisplay(this.page.detail.progress_date, this.page.detail.success_date);
            }, 200);
          }
          // console.log(this.page.detail.images_group); 

          // this.global._toast(event.msg, 2000, 'middle')
        } else {
          this.global._toast(event.msg, 2000, 'middle')
          this.router.navigate(['/']);
        }
      })


  }

  _load_chart() {

    if (this._chart_view) { this._chart_view.destroy(); }

    const ctx = this.canvasRef?.nativeElement.getContext('2d');
    let _labels: Array<string> = [];
    let _score: Array<number> = [];

    for (let index = 0; index < this.page.detail.group_rank.group_list.length; index++) {
      const _group = this.page.detail.group_rank.group_list[index];
      _labels.push(_group.name_th);
      _score.push(_group.score);
    }

    this._chart_view = new Chart(ctx, {
      type: 'line',
      data: {
        labels: _labels,
        datasets: [
          {
            label: "ระดับผล",
            data: _score,
            fill: 'start',
            backgroundColor: '#a1cfff',
            borderColor: 'rgba(30, 150, 252, 1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(30, 150, 252, 1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointHoverBackgroundColor: 'rgba(30, 150, 252, 1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 5,
            pointRadius: 5,
            pointHitRadius: 35,
            barThickness: 20
          }
        ]
      },
      options: new optionChart().optionChart()
    })
  }

  async _add_weight() {

    const _get_palm_info = (() => {
      this.isLoading = true;
      return new Promise<{ title: string, price: number, status: boolean }>((resolve) => {
        this.auth.getData(`user/palm-info`)
          .subscribe((event: any) => {
            // console.log(event);

            let _set_data = {
              title: "",
              price: 0,
              status: false
            }

            if (event.status) {
              _set_data.title = event.data.title;
              _set_data.price = event.data.price;
              _set_data.status = true;
            }

            if (!event.status) {
              this.global._toast(event.msg, 2000, 'middle');
            }

            this.isLoading = false;
            resolve(_set_data);

          })
      })
    })

    const _palmInfo = await _get_palm_info();

    this.global._alert_ui('กำหนดน้ำหนัก', `ราคากลาง <b>${(_palmInfo.price).toFixed(2)} ฿</b> <p>${_palmInfo.title}</p> <p>ตัวอย่าง : 1.26 กิโลกรัม , 0.26 กิโลกรัม</p>`, [
      {
        placeholder: 'กำหนดน้ำหนักเป็นกิโลกรัม',
        min: 0,
        name: 'txtWeigth',
        type: 'number',
        handler: ((data) => {
          console.log("data", data);
        })
      }
    ], [
      {
        role: 'cancel',
        text: 'ยกเลิก'
      },
      {
        role: 'confirm',
        text: 'ยืนยัน',
        handler: ((data) => {

          if (!Number(data.txtWeigth) || Number(data.txtWeigth) < 0) {
            this.global._toast('น้ำหนักต้องมากกว่า 0 และไม่เป็นค่าว่าง', 2500, 'middle')
            return false;
          }

          this._call_update_weigth(data.txtWeigth);

          return true;

        })
      }
    ])
  }

  _call_update_weigth(_weigth: number) {
    this.isLoading = true;
    this.auth.putData(`user/history/${this.page.jobsId}/weigth`, { weigth: _weigth })
      .subscribe((event: any) => {
        // console.log(event); 
        this._load();
        this.isLoading = false;
        this.global._toast(event.msg, 2500, 'middle');

      })
  }

  async _open_modal(index: number) {
    const _name_th = this.page.detail.images_group[index].name_th;
    const _images = this.page.detail.images_group[index].images;


    // console.log("_images", _images);


    const _modal = await this.global.modalCtrl.create({
      component: ViewImageStorageComponent,
      componentProps: {
        name_th: _name_th,
        name_th_list: [],
        images: _images,
        isMode: "USER"
      }
    });

    _modal.present();

  }

  /**
   * END
   */

}


export class optionChart {
  optionChart(): ChartOptions {
    return {
      plugins: {
        legend: {
          display: false
        },
        tooltip: {
          callbacks: {
            label: ((context) => {
              let label = `ระดับ : ${context.label || ''} `;

              if (label) {
                label += `${Number(context.formattedValue).toFixed(2)}%`;
              }

              return label;
            })
          }
        }
      },
      elements: {
        line: {
          tension: 0.4
        }
      },
      responsive: false,
      maintainAspectRatio: false,
      scales: {
        x: {
          grid: {
            display: true
          },
          border: {
            display: false
          },
          ticks: {
            display: true
          }
        },
        y: {
          grid: {
            display: false
          },
          border: {
            display: false
          },
          ticks: {
            display: false,
          }
        }
      },
    }
  }
}