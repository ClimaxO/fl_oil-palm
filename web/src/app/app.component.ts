import { Component } from '@angular/core';
import { SocketAPI } from './_service/socket.service';
import { MemoryRamBrowserService } from './_service/memory-ram-browser.service';
import { AuthService } from './auth/auth.service';
import { NavigationStart, Router } from '@angular/router';
import { AutoCloseOverlayService } from './_service/auto-close-overlay.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project-fl-opi';

  constructor(
    public socketService: SocketAPI,
    public authService: AuthService,
    private router: Router,
    private _closeOverlay: AutoCloseOverlayService,
    public mrbs: MemoryRamBrowserService
  ) {

    if (mrbs.account.get().role) {
      this.socketService._init();
    }

    this.router.events.subscribe((event: any): void => {
      if (event instanceof NavigationStart) {
        if (event.navigationTrigger === 'popstate') {
          this._closeOverlay.trigger();
        }
      }
    });

  }

}
