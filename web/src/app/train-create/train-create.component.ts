import { Component, OnInit } from '@angular/core';
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { GlobalService } from '../_service/global.service';
import { AuthService } from '../auth/auth.service';
import { JOBs_FORM_DATA } from '../class/res_jobs_today.class';

@Component({
  selector: 'app-train-create',
  templateUrl: './train-create.component.html',
  styleUrls: ['./train-create.component.scss']
})
export class TrainCreateComponent implements OnInit {

  public files: NgxFileDropEntry[] = [];
  formCtl!: FormGroup;
  isUpload: boolean = false;

  image_config: { images: Array<{ url: string, fileName: string, mimetype: string, file: File }>, form_data: JOBs_FORM_DATA, isProcess: boolean, config: { width: number, height: number } } = {
    images: [],
    form_data: {
      seller_id: "", /** รหัสผู้ขาย */
      seller_name: "", /** ชื่อผู้ขาย */
      plot_id: "", /** แปลงที่ */
      driver_name: "", /** ชื่อผู้ขับ */
      car_no_id: "", /** ทะเบียนรถ */
      weigher_name: "", /** ผู้ชั่งชื่อ */
      evaluation_officer: "", /** เจ้าหน้าที่ประเมิน */
    },
    isProcess: false,
    config: {
      width: 224,
      height: 224
    }
  }


  constructor(
    private global: GlobalService,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.global.setTitlePage("สร้างรายการใหม่");
    this.buildForm();
  }

  private buildForm() {
    this.formCtl = this.formBuilder.group({
      'car_no_id': [this.image_config.form_data.car_no_id, [Validators.required]],
      'driver_name': [this.image_config.form_data.driver_name, [Validators.required]],
      'evaluation_officer': [this.image_config.form_data.evaluation_officer, [Validators.required]],
      'plot_id': [this.image_config.form_data.plot_id, [Validators.required]],
      'seller_id': [this.image_config.form_data.seller_id, [Validators.required]],
      'seller_name': [this.image_config.form_data.seller_name, [Validators.required]],
      'weigher_name': [this.image_config.form_data.weigher_name, [Validators.required]],
    });
  }

  _next_upload() {
    this.isUpload = true;
  }

  ngOnInit(): void {
  }

  dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      if (droppedFile.fileEntry.isFile && !this.global.isFileAllowed(droppedFile.fileEntry.name)) {
        this.global._toast(`Only files in '.jpg', '.jpeg', '.png' format are accepted and directories are not allowed.`, 2000, 'middle');
        return
      }

      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // console.log(droppedFile.relativePath, file, file.type);
          const reader = new FileReader();

          reader.onload = (event) => {
            const base64String = event.target?.result as string;

            this.resizeAndRenderImage(base64String, file.name)
          };

          reader.readAsDataURL(file);

        });
      }
    }
  }

  resizeAndRenderImage(dataUrl: string, fileName: string) {
    const canvas: HTMLCanvasElement = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const img = new Image();

    img.onload = () => {
      canvas.width = this.image_config.config.width;
      canvas.height = this.image_config.config.height;
      ctx?.drawImage(img, 0, 0, this.image_config.config.width, this.image_config.config.height);

      // Get the resized image as data URL
      let dataBase64 = canvas.toDataURL('image/jpeg');
      this.image_config.images.push({
        fileName: fileName,
        url: dataBase64,
        file: <File>this.global.base64ToFile(dataBase64),
        mimetype: 'image/jpeg'
      })
    };

    img.src = dataUrl;
  }

  _remove_image(index: number) {
    this.image_config.images.splice(index, 1);
  }

  _upload_file() {
    this.global.setTitlePage("รอสักครู่...");
    if (this.image_config.images.length === 0) {
      this.global._toast("ไม่พบภาพสำหรับการประเมินผล โปรดลองใหม่", 2000, 'middle');
      return
    }
    this.image_config.isProcess = true;

    const _form_data: Array<{ key: string, val: string }> = [];

    for (let index = 0; index < Object.keys(this.formCtl.value).length; index++) {
      const _key: string = Object.keys(this.formCtl.value)[index];
      const _val: string = String(Object.values(this.formCtl.value)[index]);
      _form_data.push({
        key: _key,
        val: _val
      })
    }

    // console.log("_form_data", _form_data);

    this.auth.uploadFile(this.image_config.images, _form_data)
      .subscribe((event: any) => {
        // console.log(event);

        if (event.status) {
          this.router.navigate([`/dashboard/view/${event.jobs}`]);
        } else {
          this.global._toast(event.msg, 2000, 'middle')
        }
      })
  }

  /**
   * End
   */

} 