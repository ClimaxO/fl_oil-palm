import { ComponentFactoryResolver, Directive, ElementRef, ViewContainerRef } from '@angular/core';
import { fromEvent, race } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';

@Directive({
  selector: '[imgShowLoading]'
})
export class ShowLoadingDirective {

  get imageElement() {
    return this.el.nativeElement;
  }

  constructor(
    private el: ElementRef<HTMLImageElement>,
    private vc: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {

    this.vc.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(LoadingSpinnerComponent);
    const componentRef = this.vc.createComponent(componentFactory);

    // this.imageElement.parentElement?.appendChild(div);
    const _load = fromEvent(this.imageElement, 'load').pipe(map(() => true))
    const _err = fromEvent(this.imageElement, 'error').pipe(map(() => false))

    const _loaded = race(_load, _err);

    _loaded
      .pipe(
        first(),
        tap((_success) => {
          if (_success) {
            componentRef.destroy()
          } else {
            componentRef.destroy()
          }
        })
      ).subscribe()

  }

}
