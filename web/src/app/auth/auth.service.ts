import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { GlobalService } from '../_service/global.service';
import { MemoryRamBrowserService } from '../_service/memory-ram-browser.service';
import { SocketAPI } from '../_service/socket.service';
import { ACCOUNT_TXT_INFO as ACCOUNT_CLASS_INFO, ROLE_TXT_ADMIN as ROLE_CLASS_ADMIN, ROLE_TXT_USER as ROLE_CLASS_USER } from '../class/storage.class';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    ACCOUNT_TXT_INFO: string = ACCOUNT_CLASS_INFO;
    ROLE_TXT_ADMIN: string = ROLE_CLASS_ADMIN;
    ROLE_TXT_USER: string = ROLE_CLASS_USER;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private http: HttpClient,
        private global: GlobalService,
        private socketService: SocketAPI,
        private mrbs: MemoryRamBrowserService
    ) {
        const _account_info = localStorage.getItem(this.ACCOUNT_TXT_INFO);

        if (_account_info) {
            this.mrbs.account.set(JSON.parse(_account_info));
        }

    }

    login(email: string, password: string) {
        const _goTo = `${window.location.origin}/dashboard`;
        if (this.isLoggedIn()) {
            window.location.replace(_goTo);
            return
        }
        this.http
            .post(`${environment.api.server}/auth`, { email: email, password: password })
            .pipe(catchError(this.httpError))
            .subscribe((event: any) => {
                if (event.status) {
                    localStorage.setItem(this.ACCOUNT_TXT_INFO, JSON.stringify(event.data));
                    window.location.replace(_goTo);
                } else {
                    this.global._toast(event.msg, 2000, 'middle');
                }
            })
    }

    logout(): void {
        localStorage.clear();
        window.location.reload();
    }

    isLoggedIn(): boolean {
        const loggedIn = (localStorage.getItem(this.ACCOUNT_TXT_INFO) ? true : false);
        return loggedIn;
    }

    uploadFile(images: Array<{ url: string, fileName: string, mimetype: string, file: File }>, data: Array<{ key: string, val: string }> = []) {
        var formData = new FormData();

        images.forEach((_file: { url: string, fileName: string, mimetype: string, file: File }, i: any) => {
            formData.append('images', _file.file, _file.fileName);
        });

        data.forEach((_post_data: { key: string, val: string }, i: any) => {
            formData.append(_post_data.key, _post_data.val);
        });

        return this.http
            .post(`${environment.api.server}/user/create`, formData)
            .pipe(catchError(this.httpError));
    }

    uploadProfileImg(images: { fileName: string, file: File }) {
        var formData = new FormData();
        formData.append('profile', images.file, images.fileName);

        return this.http
            .post(`${environment.api.server}/user/update/profile`, formData)
            .pipe(catchError(this.httpError));
    }

    postData(url: string, body: any) {
        return this.http
            .post(`${environment.api.server}/${url}`, body)
            .pipe(catchError(this.httpError));
    }

    putData(url: string, body: any) {
        return this.http
            .put(`${environment.api.server}/${url}`, body)
            .pipe(catchError(this.httpError));
    }

    getData(url: string) {
        return this.http
            .get(`${environment.api.server}/${url}`)
            .pipe(catchError(this.httpError));
    }

    httpError(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(() => {
            return errorMessage;
        });
    }

    /**
     * END
     */
}
