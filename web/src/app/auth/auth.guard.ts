import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './auth.service';
import { MemoryRamBrowserService } from '../_service/memory-ram-browser.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthService,
    private mrbs: MemoryRamBrowserService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    // Check if the user is logged in by validating the access token
    const accessToken = this.mrbs.account.get().token;
    if (!accessToken) {
      this.router.navigate(['/login']);
      return false;
    }

    const userRole = this.mrbs.account.get().role;

    if (next.data.role) {

      switch (next.data.role) {
        case this.authService.ROLE_TXT_USER:

          if (userRole !== this.authService.ROLE_TXT_USER) {
            this.router.navigate(['/admin']);
          }


          break;

        case this.authService.ROLE_TXT_ADMIN:

          if (userRole !== this.authService.ROLE_TXT_ADMIN) {
            this.router.navigate(['/dashboard']);
          }

          break;

        default:
          break;
      }

    }


    return true;

  }
}
