import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { MemoryRamBrowserService } from '../_service/memory-ram-browser.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private mrbs: MemoryRamBrowserService
    ) {

    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        // Get the access token from localStorage or any other storage mechanism
        const accessToken = this.mrbs.account.get().token;

        // Clone the request and add the access token as an Authorization header
        if (accessToken) {
            const authReq = req.clone({
                headers: req.headers.set('Authorization', `${accessToken}`),
            });
            return next.handle(authReq);
        }

        return next.handle(req);
    }
}
