import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../_service/global.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent implements OnInit {

  isLoading: boolean = true;
  page: Array<PACKAGE_CLASS> = [];
  constructor(
    public global: GlobalService,
    private auth: AuthService,
  ) {
    this.global.setTitlePage("Package");
    this._load();
  }

  ngOnInit(): void {
  }

  _load() {

    this.isLoading = true;
    this.auth.getData(`package`)
      .subscribe((event: any) => {
        // console.log(event);

        this.isLoading = false;

        if (event.status) {
          this.page = event.data
        }

      })


  }


}


export interface PACKAGE_CLASS {
  _id: string;
  name: string;
  detail: string;
  price: number;
  can_used_storage: number;
}