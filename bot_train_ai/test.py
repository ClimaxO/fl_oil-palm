import time
import selenium
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

import math

import string
import time
import os
import datetime
import base64

def make_browser_option():
    try:
        err = None 
        
        options = Options()
        
        # prefs = {"download.default_directory": download_path} 
        # options.add_experimental_option("prefs", prefs)
        
        profile_path = os.path.abspath("bot-chrome-profile")
        print("profile_path",profile_path)
        options.add_argument('--user-data-dir='+profile_path)
        options.add_argument('--disable-extensions')
        options.add_argument('--profile-directory=Default')
        options.add_argument("--disable-plugins-discovery")
        options.add_argument("--disable-plugins")
        
        return options , err
    except Exception as e:
        # code to handle any other type of exception
        print("make_browser_option Exception occurred:", e)        
        return None,e


browser_options , browser_options_err = make_browser_option()
if(browser_options_err is not None):
    raise Exception("make_browser_option fail with error "+str(browser_options_err))

browser = selenium.webdriver.Chrome(options=browser_options)        

browser.get('https://teachablemachine.withgoogle.com/train/image/1wskNJZRxs-C2f18l9uO9LAJWmHzfRl3a') #main

time.sleep(10000)