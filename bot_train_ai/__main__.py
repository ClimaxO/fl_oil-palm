# import o
import socketio
import json
# from functions import browser
# from functions import files
import keyboard
import sys
import os
import time
import yaml
import mimetypes

from functions import files
from functions import state
from functions import browser
from functions import request_service

with open('config.yaml', 'r') as f:
    config = yaml.safe_load(f)
    
sio = socketio.Client()
# working = False
socket_host = config['socket_host']

#TODO - add try catch for check_pending_job
def check_pending_job():
    files.clear_download_directory()
    print("clear downloads folder")
    #TODO change it into joblist,joblist_err
    joblist = files.read_pending_jobs(sio)
    if(len(joblist) <= 0):
        print("no pending job ready for new job")
    else:
        print("some pending job start doing leftover job")
        selected_job = joblist[0]
        print("selected_job",selected_job)
        #TODO change it into job_data,job_data_err
        job_data = files.read_job_single(selected_job)
        print("job_data",job_data)
        # url = "http://159.65.131.143:6060" + job_data['file_download']
        url = socket_host + job_data['file_download']
        print("download_file url",url)
        jobs_id = job_data['jobsId']
        print("jobs_id",jobs_id)
        try:
            state.working = True
            sio.emit('socket:bot:update',files.make_socket_bot_progress_json(message="start",progress="02",jobs_id=jobs_id))
            # TODO change it into job_data,job_data_err 
            # TODO also add ability for bot to wait for download 
            # TODO also add check for download result 
            # TODO if timeout and still don't see file return err
            # TODO add timeout for waiting for server to zip the file
            # time.sleep(10)
            download_result = files.download_file(url)
            print("download_result",download_result)
            sio.emit('socket:bot:update',files.make_socket_bot_progress_json(message="python bot : "+download_result['result'],progress="01",jobs_id=jobs_id))
            sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : start doing job for jobsId "+jobs_id,jobs_id=jobs_id))
            #unzip files
            print("download_result['file_path']",download_result['file_path'])
            #might download too fast wait a bit
            time.sleep(10)
            #might want to check if file is actually downloaded
            #TODO add check for unzip result
            files.unzip_Job_file(download_result['file_path'])
            sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : unzip file success for jobsId "+jobs_id,jobs_id=jobs_id))
            #turn on browser
            sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : start opening browser",jobs_id=jobs_id))
            full_export_path , full_export_path_err = browser.start_browser(sio)
            if full_export_path_err != None:
                print("full_export_path_err "+full_export_path_err)
                raise(full_export_path_err)
            sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : closing browser",jobs_id=jobs_id))
            #creaet and move all export files into a zip file
            config_json_data , config_json_data_err = files.read_config_json()
            if(config_json_data_err is not None):
                print("config_json_data_err "+config_json_data_err)
                raise Exception("create_export_folder fail with error "+str(config_json_data_err))
            upload_file_name = config_json_data['path']['save_file_name']
            create_zip_result,create_zip_result_err = files.create_export_zip_file(full_export_path,upload_file_name)
            if create_zip_result_err != None:
                print("create_zip_result_err "+create_zip_result_err)
                raise(create_zip_result_err)
            #upload zip file
            upload_file_path = full_export_path + "\\" + upload_file_name
            upload_form_data = {'model_zip': (upload_file_name, open(upload_file_path, 'rb'), mimetypes.guess_type(upload_file_path)[0])}
            # upload_url = "http://159.65.131.143:6060/admin/files/train-update"
            upload_url = socket_host+"/admin/files/train-update"
            upload_result,upload_err = request_service.make_get_request(url=upload_url,form_data=upload_form_data)
            print("result",upload_result)
            print("err",upload_err)
            if(upload_err != None):
                print("upload_err "+upload_err)
                raise(upload_err)
            files.move_job(selected_job,True)
            # sio.emit('socket:bot:update',files.make_socket_bot_progress_json(message="python bot : done with job",progress="04",jobs_id=jobs_id))
            state.working = False
            check_pending_job()
        except Exception as e:
            print("check_pending_job Exception occurred:", e)
            sio.emit('socket:bot:update',files.make_socket_bot_progress_json(message="python bot : check pending job job fail",progress="03",jobs_id=jobs_id))
            files.save_exception_to_file(e,"main 1")
            files.move_job(selected_job,False)
            state.working = False
            check_pending_job()

@sio.event
def connect():
    print('connection established')
    sio.emit('socket:login:bot',
        {
            "Authorization":"6V9Ot6fc7cGY",
            "platform":"bot",
            "version":"6.6.6"
        }
    )
    # check_pending_job()
    # print("emit socket:login:bot")
    # check_pending_job()
    
# @sio.event
# def disconnect():
#     print("disconnected from server")
#     print("try to reconnect in 3 seconds")    
#     time.sleep(3)
#     main()

# @sio.event
# def my_message(data):
#     print('message received with ', data)
    
@sio.on('process')
def on_message(socket):
    print("process socket",socket)
    # print("process socket bot",socket['bot'])
    if files.is_job_json(socket):
        print("valid job json recieve")
        #TODO add check for write job success or fail also add try catch
        files.write_new_jobs(sio,socket)
        if(state.working == False):
            check_pending_job()
    if files.is_login_json(socket):
       print("valid login json recieve") 
       if(state.working == False):
            check_pending_job()
            
@sio.on('socket:login:bot:status')            
def on_socket_login_status(socket):
    print("socket:login:bot:status ",socket)
    if(files.is_login_status_json(socket)):
        print("login success")
        # time.sleep(3)
        check_pending_job()
    
def handle_exit():
    print("exit...") 
    os._exit(0)
    
def main():
    try:
        # query_params = {
        #     "Authorization":"6V9Ot6fc7cGY",
        #     "platform":"Optimus",
        #     "version":"6.6.6"
        # }
        
        # query_string = "&".join([f"{key}={value}" for key, value in query_params.items()])
        # socket_url = socket_host+"/socket.io?"+query_string
        
        sio.connect(socket_host)
        # sio.connect(socket_url)
        #api key : 6V9Ot6fc7cGY
        sio.wait()
    except Exception as e:
        # code to handle any other type of exception
        print("main Exception occurred:", e)
        print("try to connect again in 3 second")
        time.sleep(3)
        main()
    

if __name__ == '__main__':
    keyboard.add_hotkey('ctrl+c+.', lambda:handle_exit() )
    print("bot start press ctrl + c + . to exit")
    main()
    

        