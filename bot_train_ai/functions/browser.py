import time
import selenium
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

import math

import string
import time
import os
import datetime
import base64

from functions import files

def make_browser_option(download_path):
    try:
        err = None 
        
        options = Options()
        
        prefs = {"download.default_directory": download_path} 
        options.add_experimental_option("prefs", prefs)
        
        profile_path = os.path.abspath("bot-chrome-profile")
        print("profile_path",profile_path)
        options.add_argument('--user-data-dir='+profile_path)
        options.add_argument('--disable-extensions')
        options.add_argument('--profile-directory=Default')
        options.add_argument("--disable-plugins-discovery")
        options.add_argument("--disable-plugins")
        
        return options , err
    except Exception as e:
        # code to handle any other type of exception
        print("make_browser_option Exception occurred:", e)        
        return None,e

def start_browser(sio):
    try:
        download_path , download_path_err = files.create_export_folder()
        if(download_path_err is not None):
            raise Exception("create_export_folder fail with error "+str(download_path_err))
        
        config_json_data , config_json_data_err = files.read_config_json()
        if(config_json_data_err is not None):
            raise Exception("create_export_folder fail with error "+str(config_json_data_err))
        
        browser_options , browser_options_err = make_browser_option(download_path)
        if(browser_options_err is not None):
            raise Exception("make_browser_option fail with error "+str(browser_options_err))

        browser = selenium.webdriver.Chrome(options=browser_options)
        
        # browser.get('https://teachablemachine.withgoogle.com/train/image/1W9n5iXqNvwe86S9BC2iRDBhs7P0sQ8gg') #test
        browser.get('https://teachablemachine.withgoogle.com/train/image/1wskNJZRxs-C2f18l9uO9LAJWmHzfRl3a') #main
        # https://teachablemachine.withgoogle.com/train/image/1wskNJZRxs-C2f18l9uO9LAJWmHzfRl3a
        
        jobs_id = config_json_data['_id']
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : done go to page",jobs_id=jobs_id))
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : done go to page',config_json_data['_id']))
        
        check_page_load(browser=browser)
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : page load done",jobs_id=jobs_id))
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : page load done',config_json_data['_id']))
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : start upload",jobs_id=jobs_id))
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : start upload',config_json_data['_id']))
        
        for job in config_json_data['data']:
            trainClass = job['name']
            print("trainClass",trainClass)
            directory = job['dir']
            print("directory",directory)
            folder_path = os.path.abspath("downloads/"+job['dir'])
            print("folder_path",folder_path)
            upload(browser=browser,className=trainClass,folder_path=folder_path)
            sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : upload done for "+trainClass,jobs_id=jobs_id))
            # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : upload done for '+trainClass,config_json_data['_id']))  
            time.sleep(3)
            
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : upload sample done",jobs_id=jobs_id))            
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : upload sample done',config_json_data['_id']))
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : start training",jobs_id=jobs_id))            
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : start training',config_json_data['_id']))              
            
        train(sio=sio,browser=browser)    

        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : training done",jobs_id=jobs_id))      
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : training done',config_json_data['_id']))      
                            
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : start download",jobs_id=jobs_id))  
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : start download')
        
        download(sio=sio,browser=browser,downloadPath=download_path)
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : download done",jobs_id=jobs_id))  
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : download done')
        
        #wait for download
        time.sleep(10)
        
        unzip_result,unzip_result_err = unzip_model(download_path)                    
        if(unzip_result_err is not None):
            raise Exception("unzip_result_err "+str(unzip_result_err))
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : start saving",jobs_id=jobs_id))  
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : start saving')
        
        save_project(browser=browser)
        
        sio.emit('socket:bot:update',files.make_socket_bot_update_json(message="python bot : save project done",jobs_id=jobs_id))  
        # sio.emit('socket:bot:update',files.make_socket_bot_update_json('python bot : save project done')
        
        time.sleep(10)
        
        return download_path,None
        
    except Exception as e:
        # code to handle any other type of exception
        print("start_browser Exception occurred:", e)
        files.save_exception_to_file(e,"start_browser")
        # raise Exception(e)
        return None,e
    
def check_page_load(browser):
    try:
        tmApp_find_limit = 300
        tmApp_find_retry = 0
        while tmApp_find_retry < tmApp_find_limit:
            try:
                tmApp = browser.find_element(By.CSS_SELECTOR, '#tmApp')
                print(tmApp)
                if(tmApp is not None):
                    print("done find tmApp")
                    break
            except:
                tmApp_find_retry += 1
                print("still load project from google drive at tmApp  ... " + str(tmApp_find_retry) + " of " + str(tmApp_find_limit))
                time.sleep(5)
                
        if(tmApp_find_retry == tmApp_find_limit):
            raise Exception("Fail to find tmApp")                

        shadowRootMain_find_limit = 500
        shadowRootMain_find_retry = 0
        while shadowRootMain_find_retry < shadowRootMain_find_limit:
            try:
                shadowRootMain = browser.execute_script('return arguments[0].shadowRoot;', tmApp)
                print(shadowRootMain)
                if(shadowRootMain is not None):
                    print("done find shadowRootMain")
                    break  # break out of the loop if shadowRootMain is found
            except:
                shadowRootMain_find_retry += 1
                print("still load project from google drive at shadowRootMain  ... " + str(shadowRootMain_find_retry) + " of " + str(tmApp_find_limit))
                time.sleep(5)
                
        if(shadowRootMain_find_retry == shadowRootMain_find_limit):
            raise Exception("Fail to find shadowRootMain")                   
                
        tmModelShow_find_limit = 500
        tmModelShow_find_retry = 0
        while tmModelShow_find_retry < tmModelShow_find_limit:
            try:
                tmModelShow = shadowRootMain.find_element(By.CSS_SELECTOR, 'tm-modal[aria-hidden="true"]')
                print(tmModelShow)
                if(tmModelShow is not None):
                    print("done find tmModelShow")
                    break  # break out of the loop if tmModelShow is found
            except:
                tmModelShow_find_retry += 1
                print("still load project from google drive at tmModelShow  ... " + str(tmModelShow_find_retry) + " of " + str(tmApp_find_limit))
                time.sleep(5)    
                
        if(tmModelShow_find_retry == tmModelShow_find_limit):
            raise Exception("Fail to find tmModelShow")                              
        
    except Exception as e:
        # code to handle any other type of exception
        print("start_browser Exception occurred:", e)

def upload(browser,className: string,folder_path: string,):
    try:
        print("start upload for "+className)
        # sio.emit('send:msg','python bot : start upload for '+className)
        
        tmApp = loop_to_find_element(
            browser.find_element(By.CSS_SELECTOR, '#tmApp'),
            "tmApp"
        )
        # print(tmApp)
        
        shadowRootMain = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmApp)
            ,"shadowRootMain"
        )
        # print(shadowRootMain)
        
        tmClassifierDrawerClass = loop_to_find_element(
            shadowRootMain.find_element(By.CSS_SELECTOR, 'tm-classifier-drawer[label="'+className+'"]'),
            "tmClassifierDrawerClass "+className
        )
        # print(tmClassifierDrawerClass)
        
        shadowRootPreClick = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmClassifierDrawerClass)
            ,"shadowRootPreClick"
        )
        # print(shadowRootPreClick)
        
        upload_sample(
            browser=browser,
            shadowRootMain=shadowRootMain,
            tmClassifierDrawerClass=tmClassifierDrawerClass,
            shadowRootPreClick=shadowRootPreClick,
            className=className,
            folder_path=folder_path
        )
        
        print("upload sample done")
        # sio.emit('send:msg','python bot : upload sample done for '+className)
        
        time.sleep(3)
        
    except Exception as e:
        # code to handle any other type of exception
        print("upload Exception occurred:", e)
        # sio.emit('send:msg','python bot : upload for '+className+' Exception occurred: '+e)
    
def loop_to_find_element(search_function,name:string):
    try:
        find_limit = 100
        find_retry = 0
        while find_retry < find_limit:
            try:
                element = search_function
                # print(element)
                if(element is not None):
                    print("done find "+name)
                    time.sleep(1)
                    break
            except:
                find_retry += 1
                print("trying to find "+name+" ... " + str(find_retry) + " of " + str(find_limit))
                time.sleep(5)    
                
        if(find_retry == find_limit):
            raise Exception("Fail to find "+name)   
        
        return element
    
    except Exception as e:
        # code to handle any other type of exception
        print("upload.loop_to_find_element Exception occurred:", e)
    
def upload_sample(browser,shadowRootMain,tmClassifierDrawerClass,shadowRootPreClick,className: string,folder_path: string):
    try:
        #this loop will aim to check if upload sample button had been click to continue with next step
        #if it verify has been click then proceed to upload sample
        #if not try to click again until verify it has been click or reach max try limit
        upload_button_click_limit = 20
        upload_button_click_attemp = 0
        while upload_button_click_attemp < upload_button_click_limit:
            try:
                
                shadowRootTmClassifierDrawerClass = loop_to_find_element(
                    browser.execute_script('return arguments[0].shadowRoot;', tmClassifierDrawerClass)
                    ,"shadowRootTmClassifierDrawerClass"
                )
                
                exitButton = loop_to_find_element(
                    shadowRootTmClassifierDrawerClass.find_element(By.CSS_SELECTOR, '#exit-button'),
                    "exitButton "+className
                )
                                
                uploadButtonPreClick = loop_to_find_element(
                    shadowRootPreClick.find_element(By.CSS_SELECTOR, 'button.sample-source-btn[title="Add sample: upload"]'),
                    "uploadButtonPreClick "+className
                )
                # print(uploadButtonPreClick)
                
                uploadButtonPreClick.click()
                print("uploadButtonPreClick "+className+" clicked")
                
                tmFileSampleInput = loop_to_find_element(
                    tmClassifierDrawerClass.find_element(By.CSS_SELECTOR, 'tm-file-sample-input'),
                    "tmFileSampleInput "+className
                )
                # print(tmFileSampleInput)
                
                shadowRootTmFileSampleInput = loop_to_find_element(
                    browser.execute_script('return arguments[0].shadowRoot;', tmFileSampleInput)
                    ,"shadowRootTmFileSampleInput"
                )
                # print(shadowRootPostClick)
                
                inputElement = loop_to_find_element(
                    shadowRootTmFileSampleInput.find_element(By.CSS_SELECTOR, '#file-input'),
                    "inputElement "+className
                )
                
                if(inputElement is not None):
                    print("inputElement "+className+" found")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("upload_button_click_attemp exception ",e)
                upload_button_click_attemp += 1
                print("try to click again ... "+str(upload_button_click_attemp)+" out of "+str(upload_button_click_limit))
                time.sleep(3)
            
        if(upload_button_click_attemp == upload_button_click_limit):
            raise Exception("Fail to find inputElement "+className)            
        
        file_names_list = os.listdir(folder_path)
        num_files = len(file_names_list)
        # part_size = math.ceil(num_files / 10)  # Divide the files into 10 parts
        part_size = math.ceil(num_files / 4)  # Divide the files into 10 parts
        
        print("part_size",part_size)

        start_time = time.time()  # Get the current time at the start

        for i in range(0, num_files, part_size):
            files_part = file_names_list[i:i+part_size]  # Get the current part of files
            
            upload_file_path = ""
            
            for file_name in files_part:
                if(upload_file_path == ""):
                    upload_file_path += os.path.abspath(folder_path + "/" + file_name)
                    # print(upload_file_path)
                else:
                    upload_file_path += "\n" + os.path.abspath(folder_path + "/" + file_name)
                    # print(upload_file_path)
                
            # print("upload upload_file_path :",upload_file_path)                
                
            inputElement.send_keys(upload_file_path)
            check_sample_upload_progress(shadowRootMain=shadowRootMain, className=className)  
            
            browser.execute_script("arguments[0].value = '';", inputElement)  # Clear the input after each part

        end_time = time.time()  # Get the current time at the end

        elapsed_time = end_time - start_time  # Calculate the elapsed time

        print("Elapsed time: {:.2f} seconds".format(elapsed_time))
        
    except Exception as e:
        # code to handle any other type of exception
        print("upload.upload_sample Exception occurred:", e)
    
def check_sample_upload_progress(shadowRootMain,className: string):
    try:
        #this function is to check if upload progress modal is gone or not
        #I will not be able to check for hidden = false version since the whole code will froze at the moment
        #But I can check the hidden = true version to make sure that upload is truly done and I can progress
        file_upload_progress_modal_find_limit = 3000
        file_upload_progress_modal_find_attemp = 0
        while file_upload_progress_modal_find_attemp < file_upload_progress_modal_find_limit:
            try:
                fileUploadProgressModalHiding = loop_to_find_element(
                    shadowRootMain.find_element(By.CSS_SELECTOR, '#file-upload-progress-modal[aria-hidden="true"]'),
                    "tmClassifierDrawerClass "+className
                )
                
                if(fileUploadProgressModalHiding is not None):
                    print("check_sample_upload_progress complete")
                    time.sleep(1)
                    break
            except Exception as e:
                # print("file_upload_progress_modal_find_attemp exception ",e)
                file_upload_progress_modal_find_attemp += 1
                print("still uploading ... "+str(file_upload_progress_modal_find_attemp)+" out of "+str(file_upload_progress_modal_find_limit))
                time.sleep(1)      
        
        if(file_upload_progress_modal_find_attemp == file_upload_progress_modal_find_limit):
            raise Exception("check_sample_upload_progress seem to be stuck at "+className)         
                                  
    except Exception as e:
        # code to handle any other type of exception
        print("upload.upload_sample Exception occurred:", e)    
    
def train(sio,browser):    
    try:
        print("start training")
        # sio.emit('send:msg','python bot : start training ...')
        
        tmApp = loop_to_find_element(
            browser.find_element(By.CSS_SELECTOR, '#tmApp'),
            "tmApp"
        )
        # print(tmApp)
        
        shadowRootMain = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmApp)
            ,"shadowRootMain"
        )
        # print(shadowRootMain)
        
        tmTrain = loop_to_find_element(
            shadowRootMain.find_element(By.CSS_SELECTOR, '#train'),
            "tmTrain"
        )
        # print(tmApp)
        
        shadowRootTmTrain = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmTrain)
            ,"shadowRootTmTrain"
        )
        # print(shadowRootMain)
        
        tmProgressBtn = loop_to_find_element(
            shadowRootTmTrain.find_element(By.CSS_SELECTOR, '#train-btn'),
            "tmProgressBtn"
        )
        # print(tmApp)
        
        shadowRootTmProgressBtn = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmProgressBtn)
            ,"shadowRootTmProgressBtn"
        )
        # print(shadowRootMain)
        
        tmBtn = loop_to_find_element(
            shadowRootTmProgressBtn.find_element(By.CSS_SELECTOR, 'tm-button'),
            "tmBtn"
        )
        # print(tmApp)
        
        shadowRootTmBtn = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmBtn)
            ,"shadowRootTmBtn"
        )
        # print(shadowRootMain)
    
        trainBtn = loop_to_find_element(
            shadowRootTmBtn.find_element(By.CSS_SELECTOR, 'button'),
            "tmBtn"
        )
        # print(tmApp)
    
        try_to_click_train_btn(trainBtn=trainBtn,shadowRootTmTrain=shadowRootTmTrain)
        
        wait_for_trainging(shadowRootTmTrain=shadowRootTmTrain)
        
        print("done training")
        # sio.emit('send:msg','python bot : done training')
        
        time.sleep(3)
    
    except Exception as e:
        # code to handle any other type of exception
        print("train Exception occurred:", e)
        # sio.emit('send:msg','python bot : training Exception occurred: '+e)
        raise Exception(e)
    
def try_to_click_train_btn(trainBtn,shadowRootTmTrain):
    try:
        #this loop will try to click train button and make sure the train button has been click
        #if train button is verify to be click then proceed 
        #if not click again until verify to be click
        train_btn_click_limit = 50
        train_btn_click_attemp = 0
        while train_btn_click_attemp < train_btn_click_limit:
            try:
                trainBtn.click()
                tmProgressBtnTraining = loop_to_find_element(
                    shadowRootTmTrain.find_element(By.CSS_SELECTOR, '#train-btn[label="Training..."]'),
                    "tmProgressBtnTraining"
                )
                # print(tmProgressBtnTraining)
                
                if(tmProgressBtnTraining is not None):
                    print("training button has been click")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("train_btn_click_attemp exception ",e)
                train_btn_click_attemp += 1
                print("try to click again ... "+str(train_btn_click_attemp)+" out of "+str(train_btn_click_limit))
                time.sleep(3)   
                             
        if(train_btn_click_attemp == train_btn_click_limit):
            raise Exception("fail to click train button") 
                             
    except Exception as e:
        # code to handle any other type of exception
        print("try_to_click_train_btn Exception occurred:", e)
    
def wait_for_trainging(shadowRootTmTrain):
    #this loop is waiting until code can detect that model has finish traning by finding 'Model Trained' text
    try:
        training_time_limit = 10000
        training_time_attemp = 0
        while training_time_attemp < training_time_limit:
            try:
                tmProgressBtnTraining = loop_to_find_element(
                    shadowRootTmTrain.find_element(By.CSS_SELECTOR, '#train-btn[label="Model Trained"]'),
                    "tmProgressBtnTraining"
                )
                # print(tmProgressBtnTraining)
                
                if(tmProgressBtnTraining is not None):
                    print("training button has been click")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("training_time_attemp exception ",e)
                training_time_attemp += 1
                print("try to click again ... "+str(training_time_attemp)+" out of "+str(training_time_limit))
                time.sleep(3)   
                             
        if(training_time_attemp == training_time_limit):
            raise Exception("fail to click train button") 
                
    except Exception as e:
        # code to handle any other type of exception
        print("wait_for_trainging Exception occurred:", e)        

def download(sio,browser,downloadPath):
    try:
        print("start downlading")
        # sio.emit('send:msg','python bot : start downlading ...')
        
        # downloadPath = create_export_folder()
        print("downloadPath",downloadPath)
        
        tmApp = loop_to_find_element(
            browser.find_element(By.CSS_SELECTOR, '#tmApp'),
            "tmApp"
        )
        # print(tmApp)
        
        shadowRootMain = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmApp)
            ,"shadowRootMain"
        )
        # print(shadowRootMain)
        
        tmRun = loop_to_find_element(
            shadowRootMain.find_element(By.CSS_SELECTOR, '#run'),
            "tmRun"
        )
        # print(tmApp)
        
        shadowRootTmRun = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmRun)
            ,"shadowRootTmRun"
        )
        # print(shadowRootMain)
        
        tmBtn = loop_to_find_element(
            shadowRootTmRun.find_element(By.CSS_SELECTOR, 'tm-button[label="Export Model"]'),
            "tmBtn"
        )
        # print(tmApp)
        
        shadowRootTmBtn = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmBtn)
            ,"shadowRootTmBtn"
        )
        # print(shadowRootMain)
        
        exportBtn = loop_to_find_element(
            shadowRootTmBtn.find_element(By.CSS_SELECTOR, 'button'),
            "tmBtn"
        )
        # print(tmApp)
        
        download_under_the_hood(browser=browser,shadowRootMain=shadowRootMain,downloadPath=downloadPath)

        print("done download graph")
        # sio.emit('send:msg','python bot : done download graph ...')
        
        click_export_button(exportBtn=exportBtn,shadowRootMain=shadowRootMain)
        
        download_model(browser=browser,shadowRootMain=shadowRootMain)
        
        # # time.sleep(300)
        # print("wait 3 min for download")
        # time.sleep(200)

        print("done downlading")
        # sio.emit('send:msg','python bot : done downlading ...')
        
    except Exception as e:
        # code to handle any other type of exception
        print("download Exception occurred:", e)
        # sio.emit('send:msg','python bot : download Exception occurred: '+e)   

def create_export_folder():
    try:
        # Get the current date and time
        now = datetime.datetime.now()

        # Format the date and time as a string that can be used in a folder name
        folder_name = now.strftime("%Y-%m-%d_%H-%M-%S")

        # Replace any invalid characters with an underscore
        invalid_chars = ['\\', '/', ':', '*', '?', '"', '<', '>', '|']
        for char in invalid_chars:
            folder_name = folder_name.replace(char, '_')

        # Create the export folder path
        export_path = os.path.abspath("export/"+folder_name)
        # export_path.replace("\"","/")

        # Create the export folder if it doesn't exist
        if not os.path.exists(export_path):
            print("folder created")
            os.makedirs(export_path)

        return export_path
    
    except Exception as e:
        # code to handle any other type of exception
        print("create_export_folder Exception occurred:", e)

def download_under_the_hood(browser,shadowRootMain,downloadPath):
    try:
        #this function is for try to naviate to under the hood tab and download both graph
        tmTrain = loop_to_find_element(
            shadowRootMain.find_element(By.CSS_SELECTOR, '#train'),
            "tmTrain"
        )
        # print(tmApp)
        
        shadowRootTmTrain = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmTrain)
            ,"shadowRootTmTrain"
        )
        # print(shadowRootMain)
        
        divEditModelBtn = loop_to_find_element(
            shadowRootTmTrain.find_element(By.CSS_SELECTOR, 'div#edit-model-btn'),
            "tmTrain"
        )
        # print(tmApp)
        
        #open Advanced tab in training block
        open_advance_tab_limit = 10
        open_advance_tab_attemp = 0
        while open_advance_tab_attemp < open_advance_tab_limit:
            try:
                divEditModelBtn.click()
                time.sleep(1)
                divCollapsibleContent = loop_to_find_element(
                    shadowRootTmTrain.find_element(By.CSS_SELECTOR, 'div.collapsible-content[aria-hidden="false"]'),
                    "tmProgressBtnTraining"
                )
                # print(tmProgressBtnTraining)
                
                if(divCollapsibleContent is not None):
                    print("advance button has been click")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("open_advance_tab_attemp exception ",e)
                open_advance_tab_attemp += 1
                print("try to click again ... "+str(open_advance_tab_attemp)+" out of "+str(open_advance_tab_limit))
                time.sleep(3)   
                             
        if(open_advance_tab_attemp == open_advance_tab_limit):
            raise Exception("fail to open_advance_tab") 
        
        divUnderTheHoodBtn = loop_to_find_element(
            shadowRootTmTrain.find_element(By.CSS_SELECTOR, 'div[aria-label="Under the hood"]'),
            "divUnderTheHoodBtn"
        )
        # print(tmApp)
        
        #open under the hood tab/ viz panel
        open_under_the_hood_tab_limit = 10
        open_under_the_hood_tab_attemp = 0
        while open_under_the_hood_tab_attemp < open_under_the_hood_tab_limit:
            try:
                divUnderTheHoodBtn.click()
                time.sleep(1)
                tmViz = loop_to_find_element(
                    shadowRootMain.find_element(By.CSS_SELECTOR, '#viz'),
                    "tmViz"
                )
                # print(tmViz)
                
                shadowRootTmViz = loop_to_find_element(
                    browser.execute_script('return arguments[0].shadowRoot;', tmViz)
                    ,"shadowRootTmViz"
                )
                # print(shadowRootTmViz)
                
                vizPanel = loop_to_find_element(
                    shadowRootTmViz.find_element(By.CSS_SELECTOR, '#viz-panel[aria-hidden="false"]'),
                    "tmViz"
                )
                # print(tmViz)
                
                if(vizPanel is not None):
                    print("open_under_the_hood_tab is open")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("open_under_the_hood_tab_attemp exception ",e)
                open_under_the_hood_tab_attemp += 1
                print("try to click again ... "+str(open_under_the_hood_tab_attemp)+" out of "+str(open_under_the_hood_tab_limit))
                time.sleep(3)   
                             
        if(open_under_the_hood_tab_attemp == open_under_the_hood_tab_limit):
            raise Exception("fail to open_under_the_hood_tab") 
        
        accuracyPerEpoch = loop_to_find_element(
            shadowRootTmViz.find_element(By.CSS_SELECTOR, '#accuracy-per-epoch'),
            "accuracyPerEpoch"
        )
        # print(accuracyPerEpoch)
        
        accuracyPerEpochCanvas = loop_to_find_element(
            accuracyPerEpoch.find_element(By.CSS_SELECTOR, '.marks'),
            "accuracyPerEpochCanvas"
        )
        # print(accuracyPerEpochCanvas)
        
        accuracyPerEpochDataUrl = browser.execute_script('return arguments[0].toDataURL()', accuracyPerEpochCanvas)
        
        with open(downloadPath+'/accuracyPerEpoch.png', 'wb') as f:
            f.write(base64.b64decode(accuracyPerEpochDataUrl.split(',')[1]))
            
        lossPerEpoch = loop_to_find_element(
            shadowRootTmViz.find_element(By.CSS_SELECTOR, '#loss-per-epoch'),
            "lossPerEpoch"
        )
        # print(lossPerEpoch)
        
        lossPerEpochCanvas = loop_to_find_element(
            lossPerEpoch.find_element(By.CSS_SELECTOR, '.marks'),
            "lossPerEpochCanvas"
        )
        # print(lossPerEpochCanvas)
        
        lossPerEpochDataUrl = browser.execute_script('return arguments[0].toDataURL()', lossPerEpochCanvas)
        
        with open(downloadPath+'/lossPerEpoch.png', 'wb') as f:
            f.write(base64.b64decode(lossPerEpochDataUrl.split(',')[1]))   
            
        exitVizPanel = loop_to_find_element(
            shadowRootTmViz.find_element(By.CSS_SELECTOR, '#exit-icon'),
            "exitVizPanel"
        )        
        
        #exit under the hood tab/ viz panel
        exit_under_the_hood_tab_limit = 10
        exit_under_the_hood_tab_attemp = 0
        while exit_under_the_hood_tab_attemp < exit_under_the_hood_tab_limit:
            try:
                exitVizPanel.click()
                time.sleep(1)
                tmExportModelModal = loop_to_find_element(
                    shadowRootMain.find_element(By.CSS_SELECTOR, 'tm-export-model-modal'),
                    "tmExportModelModal"
                )
                
                if(tmExportModelModal is not None):
                    print("open_under_the_hood_tab is exit")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("exit_under_the_hood_tab_attemp exception ",e)
                exit_under_the_hood_tab_attemp += 1
                print("try to click again ... "+str(exit_under_the_hood_tab_attemp)+" out of "+str(exit_under_the_hood_tab_limit))
                time.sleep(3)   
                             
        if(exit_under_the_hood_tab_attemp == exit_under_the_hood_tab_limit):
            raise Exception("fail to open_under_the_hood_tab") 
                           
    
    except Exception as e:
        # code to handle any other type of exception
        print("download_under_the_hood Exception occurred:", e)        

def click_export_button(exportBtn,shadowRootMain):
    try:
        #try to click export button to view export modal
        click_export_button_limit = 10
        click_export_button_attemp = 0
        while click_export_button_attemp < click_export_button_limit:
            try:
                exportBtn.click()
                time.sleep(1)
                tmExportModelModal = loop_to_find_element(
                    shadowRootMain.find_element(By.CSS_SELECTOR, 'tm-export-model-modal[aria-hidden="false"]'),
                    "tmExportModelModal"
                )
                # print(tmViz)                
                
                if(tmExportModelModal is not None):
                    print("open_under_the_hood_tab is open")
                    time.sleep(1)
                    break
                
            except Exception as e:
                print("click_export_button_attemp exception ",e)
                click_export_button_attemp += 1
                print("try to click again ... "+str(click_export_button_attemp)+" out of "+str(click_export_button_limit))
                time.sleep(3)   
                             
        if(click_export_button_attemp == click_export_button_limit):
            raise Exception("fail to click_export_button") 
        
        
    except Exception as e:
        # code to handle any other type of exception
        print("click_export_button Exception occurred:", e)       
        
def download_model(browser,shadowRootMain):
    try:
        #download the model from export page
        tmExportModelModal = loop_to_find_element(
            shadowRootMain.find_element(By.CSS_SELECTOR, 'tm-export-model-modal[aria-hidden="false"]'),
            "tmExportModelModal"
        )
        
        shadowRootTmExportModelModal = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmExportModelModal)
            ,"shadowRootTmExportModelModal"
        )
        
        hostOptionHolder = loop_to_find_element(
            shadowRootTmExportModelModal.find_element(By.CSS_SELECTOR, '#host-option-holder'),
            "tmExportModelModal"
        )
        
        input_element = loop_to_find_element(
            hostOptionHolder.find_element(By.CSS_SELECTOR, 'input[name="host"][value="local"]'),
            "input_element"
        )        
        input_element.click()
        
        tmDownloadBtn = loop_to_find_element(
            shadowRootTmExportModelModal.find_element(By.CSS_SELECTOR, 'tm-button#download-btn'),
            "tmDownloadBtn"
        ) 
        
        shadowRootTmDownloadBtn = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmDownloadBtn)
            ,"shadowRootTmDownloadBtn"
        )
        
        downloadBtn = loop_to_find_element(
            shadowRootTmDownloadBtn.find_element(By.CSS_SELECTOR, 'button'),
            "radioContainer"
        ) 
        
        downloadBtn.click()
        
        modalCloseBtn = loop_to_find_element(
            shadowRootTmExportModelModal.find_element(By.CSS_SELECTOR, '.modal-close-btn'),
            "modalCloseBtn"
        )
        
        modalCloseBtn.click()
        print("exit export modal")
        
        return
        
    except Exception as e:
        # code to handle any other type of exception
        print("download_model Exception occurred:", e)
   
def save_project(browser):
    try:
        tmBurgerMenu = loop_to_find_element(
            browser.find_element(By.CSS_SELECTOR, 'tm-hamburger-menu'),
            "tmBurgerMenu"
        )
        
        shadowRootHamburgerMenu = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmBurgerMenu)
            ,"shadowRootHamburgerMenu"
        )
        
        menuHolder = loop_to_find_element(
            shadowRootHamburgerMenu.find_element(By.CSS_SELECTOR, '#menu-holder'),
            "menuHolder"
        )
        
        menuHolder.click()
        print("menuHolder clicked")
        
        mainMenu = loop_to_find_element(
            shadowRootHamburgerMenu.find_element(By.CSS_SELECTOR, '#main-menu[aria-hidden="false"]'),
            "mainMenu"
        )  
        
        driveSave = loop_to_find_element(
            shadowRootHamburgerMenu.find_element(By.CSS_SELECTOR, '#drive-save'),
            "driveSave"
        ) 
        
        driveSave.click()
        print("driveSave clicked")
        
        #wait for browser to response 
        #add check modal not hidden if still err
        # time.sleep(120)
        time.sleep(3)
        
        
        tmModalDrive = loop_to_find_element(
            browser.find_element(By.CSS_SELECTOR, 'tm-modal-drive'),
            "tmModalDrive"
        )

        # print("tmModalDrive",tmModalDrive)

        shadowRootTmModalDrive = loop_to_find_element(
            browser.execute_script('return arguments[0].shadowRoot;', tmModalDrive),
            "shadowRootTmModalDrive"
        )

        # print("shadowRootTmModalDrive",shadowRootTmModalDrive)

        # try:
        #     tmModalDriveModal = shadowRootTmModalDrive.find_element(By.CSS_SELECTOR, 'tm-modal'),
        #     print("tmModalDriveModal found")
        #     print("tmModalDriveModal",tmModalDriveModal)
        # except:
        #     print("tmModalDriveModal not found")
        
        # tmApp = loop_to_find_element(
        #     browser.find_element(By.CSS_SELECTOR, '#tmApp'),
        #     "tmApp"
        # )
        # # print(tmApp)
        
        # shadowRootMain = loop_to_find_element(
        #     browser.execute_script('return arguments[0].shadowRoot;', tmApp)
        #     ,"shadowRootMain"
        # )
        # # print(shadowRootMain)
        
        #wait for saving to complete
        wait_for_saving_limit = 10000
        wait_for_saving_attemp = 0
        while wait_for_saving_attemp < wait_for_saving_limit:
            try:
                
                # tmModalHidden = loop_to_find_element(
                #     shadowRootMain.find_element(By.CSS_SELECTOR, 'tm-modal[aria-hidden="true"]'),
                #     "tmModalHidden"
                # ) 
                
                # print("tmModalHidden",tmModalHidden)
                
                # tmModalDriveModal = loop_to_find_element(
                    # shadowRootTmModalDrive.find_element(By.CSS_SELECTOR, 'tm-modal'),
                # )
                
                tmModalDriveModal = shadowRootTmModalDrive.find_element(By.CSS_SELECTOR, 'tm-modal')
                
                print("tmModalDriveModal",tmModalDriveModal)
                print("tmModalDriveModal != None",tmModalDriveModal != None)
                
                # if(tmModalDriveModal is not None):
                #     print("saving is complete")
                #     time.sleep(1)
                #     break
                
                if(tmModalDriveModal != None):
                    # wait_for_saving_attemp += 1
                    print("still saving... " + str(wait_for_saving_attemp) + "/" + str(wait_for_saving_limit))
                    time.sleep(1)
                    
                wait_for_saving_attemp += 1    
                
            except Exception as e:
                # print("tmModalDriveModal e")
                # print(str(e))
                print("saving is complete") 
                time.sleep(3)   
                break
            
            # except Exception as e:
            #     print("wait_for_saving_attemp exception ",e)
            #     wait_for_saving_attemp += 1
            #     print("waiting ... "+str(wait_for_saving_attemp)+" out of "+str(wait_for_saving_limit))
            #     time.sleep(1)   
                             
        if(wait_for_saving_attemp == wait_for_saving_limit):
            raise Exception("fail to save") 
        
    except Exception as e:
        # code to handle any other type of exception
        print("save_project Exception occurred:", e)
        # sio.emit('send:msg','python bot : save_project Exception occurred: '+e)   
        
def unzip_model(folder_path):
    try:
        print("folder_path",folder_path)
        
        zip_file_name , zip_file_name_err = files.find_first_zip_file(folder_path)
        if(zip_file_name_err is not None):
            print("zip_file_name_err",zip_file_name_err)
            raise Exception("zip_file_name_err"+str(zip_file_name_err))
        print("zip_file_name",zip_file_name)
        
        zip_file_path = os.path.abspath(folder_path + "\\" + zip_file_name)
        print("zip_file_path",zip_file_path)
        
        unzip_and_delete , unzip_and_delete_err = files.unzip_and_delete(zip_file_path,folder_path)  
        if(unzip_and_delete_err is not None):
            print("unzip_and_delete_err",unzip_and_delete_err)
            raise Exception("unzip_and_delete_err"+str(unzip_and_delete_err))
        
        return True,None
    except Exception as e:
        # code to handle any other type of exception
        print("unzip_model Exception occurred:", e)
        files.save_exception_to_file(e,"unzip_model")
        # raise Exception(e)
        return None,e
  
  
  