import requests
import os
from functions  import files

def download_file(url):
    # job_data = read_job_single(job_file_name)
    # folder_path = "jobs/pending/"
    # url = "http://159.65.131.143:6060" + job_data['file_download']
    # print("download_file url",url)
    #http://159.65.131.143:6060/admin/files/train/VGugxGq-0002.zip
    try:
        folder_path = os.path.abspath("downloads")
        response = requests.get(url)
    
        if response.status_code == 200:
            file_name = url.split('/')[-1]
            file_path = os.path.join(folder_path, file_name)
            
            with open(file_path, 'wb') as file:
                file.write(response.content)
            
            print(f"File downloaded successfully and saved at: {file_path}")
            # return "download success url :"+url
            return {
                "result":"download success url :"+url,
                "file_path":file_path
            }
        else:
            print("Failed to download the file.")   
            files.save_exception_to_file("fail to download with status "+str(response.status_code),"download_file 1")
            # return "download fail url :"+url
            return {
                "result":"download fail url :"+url,
                "file_path":""
            }
    except Exception as e:
        print("create job json Exception occurred:", e)
        files.save_exception_to_file(e,"download_file 2")
        # return "exception occurred url :"+url
        return {
                "result":"download fail url :"+url,
                "file_path":""
        }
        
def make_get_request(url, headers=None, params=None, form_data=None):
    try:
        if form_data:
            response = requests.get(url, headers=headers, params=params, files=form_data)
        else:
            response = requests.get(url, headers=headers, params=params)
    
        response.raise_for_status()  # Raise an exception for 4xx/5xx status codes
        
        json_response = response.json()

        if(json_response['status']):
            return response.json(),None  # Assumes response content is JSON
        else:
            return None,response.json()  # Assumes response content is JSON
    except Exception as err:
        print(f"An error occurred: {err}")
        return None,err        
        
        
        
        
        