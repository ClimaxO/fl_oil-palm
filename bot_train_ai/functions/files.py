import json
import string
import os
import requests
import datetime
import shutil
import zipfile
import time
from tqdm import tqdm

def make_socket_bot_update_json(message,jobs_id):
    try:
        socket_bot_update_message_json = {
            "msg":message,
            "jobsId":jobs_id
        }
        return socket_bot_update_message_json
    except Exception as e:
        print("make_socket_bot_update_json Exception occurred:", e)
        save_exception_to_file(e,"make_socket_bot_update_json")
        
def make_socket_bot_progress_json(message,progress,jobs_id):
    try:
        socket_bot_progress_message_json = {
            "msg":message,
            "jobsId":jobs_id,
            "progress":progress
        }
        return socket_bot_progress_message_json
    except Exception as e:
        print("make_socket_bot_update_json Exception occurred:", e)
        save_exception_to_file(e,"make_socket_bot_update_json")        

def save_exception_to_file(e,function_name):
    try:
        error_message = str(e) + " from : " + function_name

        # Create the "err" folder if it doesn't exist
        folder_name = "err"
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

        # Generate a timestamp with the current date and time
        timestamp = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

        # Generate the filename with the timestamp
        filename = os.path.join(folder_name, f"{timestamp}_exception.txt")

        # Save exception to the text file
        with open(filename, 'w') as file:
            file.write(error_message)
            
        print("error log created")
        
        return
    
    except Exception as e:
        
        print("save_exception_to_file Exception occurred:", e)

def is_job_json(json_data):
    expected_keys = ['bot']
    expected_nested_keys = ['msg', 'jobsId', 'file_download']

    if not isinstance(json_data, dict):
        return False

    if not all(key in json_data for key in expected_keys):
        return False

    bot_data = json_data['bot']

    if not isinstance(bot_data, dict):
        return False

    if not all(key in bot_data for key in expected_nested_keys):
        return False

    return True

def is_login_json(json_data):
    expected_keys = ['bot']
    expected_nested_keys = ['msg']

    if not isinstance(json_data, dict):
        return False

    if not all(key in json_data for key in expected_keys):
        return False

    bot_data = json_data['bot']

    if not isinstance(bot_data, dict):
        return False

    if not all(key in bot_data for key in expected_nested_keys):
        return False

    return True

def is_login_status_json(json_data):
    expected_keys = ['status','msg']

    if not isinstance(json_data, dict):
        return False

    if not all(key in json_data for key in expected_keys):
        return False

    status = json_data['status']
    
    return status

def write_new_jobs(sio,json_data):
    
    # print("json_data",json_data)
    
    json_data = json_data['bot']
    
    folder_path = 'jobs'
    
    # try:
    #     json_data = json.loads(json_data)
    # except Exception as e:
    #     print("load json Exception occurred:", e)
    #     print("json_data",json_data)
    #     sio.emit('send:msg','python bot : write_jobs load json fail')
    #     save_exception_to_file(e)
    #     return
    
    try:
        json_path = os.path.abspath(folder_path+"/pending/"+datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")+"_jobId_"+json_data['jobsId']+".json")
        print(json_path)
    except Exception as e:
        print("make json path Exception occurred:", e)
        sio.emit('send:msg','python bot : write_jobs make json path fail')
        save_exception_to_file(e,"write_new_jobs 1")
        return
    
    try:
        with open(json_path, 'w') as file:
            json.dump(json_data, file)    
    except Exception as e:
        print("create job json Exception occurred:", e)
        sio.emit('send:msg','python bot : write_jobs create job json fail')
        save_exception_to_file(e,"write_new_jobs2")
        return
    
    sio.emit('send:msg','python bot : write_jobs create job success for '+json_data['jobsId'])
    return

def read_pending_jobs(sio):

    folder_path = "jobs/pending"

    try:
        # Get a list of JSON files in the "pending" folder
        json_files = [file for file in os.listdir(folder_path) if file.endswith(".json")]
        # print("json_files",json_files)
        
        # Sort the JSON files by date
        sorted_json_files = sorted(json_files)
        # print("sorted_json_files",sorted_json_files)
        
        return sorted_json_files
    
    except Exception as e:
        print("create job json Exception occurred:", e)
        save_exception_to_file(e,"write_new_jobs 3")
        return

def read_job_single(job_file_name):
    try:
        folder_path = "jobs/pending/"
        file_path = os.path.abspath(folder_path + job_file_name)
        with open(file_path, 'r') as file:
            data = json.load(file)
        return data
    except Exception as e:
        print("create job json Exception occurred:", e)
        save_exception_to_file(e,"read_job_single 1")
        return
    
def download_file(url):
    # job_data = read_job_single(job_file_name)
    # folder_path = "jobs/pending/"
    # url = "http://159.65.131.143:6060" + job_data['file_download']
    # print("download_file url",url)
    #http://159.65.131.143:6060/admin/files/train/VGugxGq-0002.zip
    try:
        print("download_file function url "+url)
        folder_path = os.path.abspath("downloads")
        response = requests.get(url, stream=True)
        
        total_size = int(response.headers.get('content-length', 0))
        block_size = 1024  # Adjust the block size as per your preference
        progress_bar = tqdm(total=total_size, unit='B', unit_scale=True)
    
        if response.status_code == 200:
            file_name = url.split('/')[-1]
            file_path = os.path.join(folder_path, file_name)
            
            with open(file_path, 'wb') as file:
                for data in response.iter_content(block_size):
                    progress_bar.update(len(data))
                    # sio.emit('socket:bot:update',make_socket_bot_progress_json(message="python bot : download progress",progress="01",jobs_id=jobs_id))
                    file.write(data)
            
            # with open(folder_path, 'wb') as file:
            #     # for data in response.iter_content(block_size):
            #     #     progress_bar.update(len(data))
            #     #     file.write(data)
            #     print("ok")
            
            print(f"File downloaded successfully and saved at: {file_path}")
            # return "download success url :"+url
            return {
                "result":"download success url :"+url,
                "file_path":file_path
            }
        else:
            print("Failed to download the file.")   
            save_exception_to_file("fail to download with status "+str(response.status_code),"download_file 1")
            # return "download fail url :"+url
            return {
                "result":"download fail url :"+url,
                "file_path":""
            }
    except Exception as e:
        print("create job json Exception occurred:", e)
        save_exception_to_file(str(e),"download_file 2")
        # return "exception occurred url :"+url
        return {
                "result":"download fail url :"+url,
                "file_path":""
        }

def unzip_Job_file(zip_file_path):
    tries_count = 0
    max_count = 500
    while(tries_count < max_count):
        time.sleep(3)
        try:
            folder_path = os.path.abspath("downloads")
            # print("folder_path",folder_path)
            # zip_file_path = os.path.abspath("downloads/"+zip_file_name)
            # print("zip_file_path",zip_file_path)
            
            with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
                zip_ref.extractall(folder_path)

            # Check if all files were extracted
            extracted_files = zip_ref.namelist()
            expected_files = ['config.json']  # Adjust this list based on the expected files in the ZIP
            
            if all(file in extracted_files for file in expected_files):
                print("All files were successfully extracted.")
                return
            else:
                print("Some files were not extracted successfully.")
                raise Exception("Some files were not extracted successfully.")
        except Exception as e:
            print("unzip_Job_file error "+e)
            # print("retry "+str(tries_count)+"/"+str(max_count))
            print("retry unzip")
            tries_count += 1
            # print(tries_count+" count unzip_Job_file Exception occurred:", e)
            # save_exception_to_file(e,"unzip_Job_file 1 count "+tries_count)
            # raise Exception(e)
            # return "exception occurred url :"+url
            
    if(tries_count >= max_count):
        save_exception_to_file("unzip job file retry out of range file not found or not download","unzip_Job_file")
        raise Exception("unzip job file retry out of range file not found or not download")
    
    return True

# """"""        
# create export folder and return full path        
# """"""
def create_export_folder():
    try:
        err = None
        # Get the current date and time
        now = datetime.datetime.now()

        # Format the date and time as a string that can be used in a folder name
        folder_name = now.strftime("%Y-%m-%d_%H-%M-%S")
        
        # Replace any invalid characters with an underscore
        invalid_chars = ['\\', '/', ':', '*', '?', '"', '<', '>', '|']
        for char in invalid_chars:
            folder_name = folder_name.replace(char, '_')

        # Create the export folder path
        export_path = os.path.abspath("export/"+folder_name)
        # export_path.replace("\"","/")

        # Create the export folder if it doesn't exist
        if not os.path.exists(export_path):
            print("folder created")
            os.makedirs(export_path)

        return export_path,err
    
    except Exception as e:
        # code to handle any other type of exception
        print("create_export_folder Exception occurred:", e)        
        return None,e

def read_config_json():
    try:
        err = None
        
        config_json_path = os.path.abspath("downloads/config.json")
        
        with open(config_json_path, 'r') as file:
            data = json.load(file)
        
        # Check if the loaded JSON data has the correct structure
        if validate_config_json(data):
            return data, err
        else:
            err = "Invalid config.json format"
            return None, err
    
    except Exception as e:
        # code to handle any other type of exception
        print("create_export_folder Exception occurred:", e)        
        return None,e        
    
    
def validate_config_json(data):
    # Perform the necessary validation checks on the JSON structure
    # Return True if the JSON is in the correct format, otherwise return False
    
    # Example validation: Check if the required keys are present
    required_keys = ['_id', 'path', 'chart', 'data']
    if all(key in data for key in required_keys):
        return True
    else:
        return False    
    
def move_job(job_name, success):
    source_file = os.path.abspath('jobs/pending/' + job_name)
    
    if success:
        destination_file = os.path.abspath('jobs/done/' + job_name)
    else:
        destination_file = os.path.abspath('jobs/fail/' + job_name)

    # Check if the source file exists
    if not os.path.exists(source_file):
        print("Source file not found:", source_file)
        save_exception_to_file("move_job cannot move Source file not found "+job_name)
        return

    # Create the destination directory if it doesn't exist
    destination_directory = os.path.dirname(destination_file)
    os.makedirs(destination_directory, exist_ok=True)

    try:
        shutil.move(source_file, destination_file)
        print("File moved successfully.")
    except Exception as e:
        print("Error occurred while moving the file:", str(e))
        save_exception_to_file("move_job Error occurred while moving the file:", str(e) , job_name)    
    
def create_export_zip_file(full_export_directory_path, zip_filename):
    try:
        # print("full_export_directory_path",full_export_directory_path)
        full_zip_filename_path = full_export_directory_path + "\\" + zip_filename
        # print("full_zip_filename_path",full_zip_filename_path)
        # Create a new zip file
        with zipfile.ZipFile(full_zip_filename_path, 'w') as zipf:
            # Iterate over all files and directories in the given directory
            for root, _, files in os.walk(full_export_directory_path):
                for file in files:
                    if file != zip_filename:
                        # print("file",file)
                        # print("file",zip_filename)
                        # print("os.path.abspath(file)",os.path.abspath(file))
                        # print("full_zip_filename_path",full_zip_filename_path)
                        # Get the full path of the file
                        file_path = os.path.join(root, file)
                        # Add the file to the zip file
                        zipf.write(file_path, os.path.relpath(file_path, full_export_directory_path))
                        
        return zip_filename,None                        
                        
    except Exception as e:
        # code to handle any other type of exception
        print("create_export_zip_file Exception occurred:", e)
        files.save_exception_to_file(e,"create_export_zip_file")
        # raise Exception(e)
        return None,e
    
# def rename_first_zip_file(folder_path, new_name):
#     # Iterate over all files in the given folder
#     for file in os.listdir(folder_path):
#         # Check if the file is a zip file
#         if file.endswith('.zip'):
#             # Create the new file path with the desired new name
#             new_file_path = os.path.join(folder_path, new_name + '.zip')
#             # Rename the zip file
#             os.rename(os.path.join(folder_path, file), new_file_path)
#             print(f'Renamed the first detected zip file to: {new_name}.zip')
#             return    
    
# def unzip_and_delete(zip_file_path, extract_folder):
#     try:
#         # Extract the contents of the zip file
#         with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
#             zip_ref.extractall(extract_folder)
        
#         # Delete the zip file
#         os.remove(zip_file_path)
#         print(f'Unzipped "{zip_file_path}" and deleted it.')    
#     except Exception as e:
#         # code to handle any other type of exception
#         print("create_export_zip_file Exception occurred:", e)
#         files.save_exception_to_file(e,"create_export_zip_file")
#         # raise Exception(e)
#         return None,e
    
def unzip_and_delete(zip_file_path, extract_folder):
    try:
        # Extract the contents of the zip file
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extractall(extract_folder)
        
        # Delete the zip file
        os.remove(zip_file_path)
        print(f'Unzipped "{zip_file_path}" and deleted it.')    
        return True,None
    except Exception as e:
        # code to handle any other type of exception
        print("unzip_and_delete Exception occurred:", e)
        save_exception_to_file(e,"unzip_and_delete")
        # raise Exception(e)
        return None,e
    
def find_first_zip_file(folder_path):
    try:
        # Iterate over all files in the given folder
        for file in os.listdir(folder_path):
            # Check if the file is a zip file
            if file.endswith('.zip'):
                return file,None
        
        return None,"not found"    
    except Exception as e:
        # code to handle any other type of exception
        print("find_first_zip_file Exception occurred:", e)
        save_exception_to_file(e,"find_first_zip_file")
        # raise Exception(e)
        return None,e
    
def clear_download_directory():
    directory_path = os.path.abspath("downloads")
    for filename in os.listdir(directory_path):
        file_path = os.path.join(directory_path, filename)
        if os.path.isfile(file_path):
            os.remove(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    
    
    